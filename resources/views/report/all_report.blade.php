<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<?php include 'resources/views/include/head.php'; ?>
<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <?php include 'resources/views/include/navbar.php'; ?>

    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        <?php echo env('APP_PROJECTNAME'); ?> </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            @lang('public.all_report.modul') </a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            @lang('public.all_report.judul') </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            @lang('public.all_report.judul')
                        </h3>
                    </div>
                </div>


                <div class="kt-portlet__body">

                    <!--begin: Search Form -->
                    <form class="kt-form kt-form--fit kt-margin-b-20 form_generate" method="POST">
                        <input type="hidden" id="id_user" value="{{ $user->id_user }}">
                        <input type="hidden" id="APP_URLAPP" value="{{ env('APP_URLAPP') }}">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="locate" value="{{ $locate }}">
                        <div class="row kt-margin-b-20">
                            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                                <label for="exampleSelect1">@lang('public.rewallet.report_year') </label>
                                <select class="form-control class_select2" id="search_report_year"
                                    name="search_report_year" style="width: 100%">
                                    <option value="">Please Choose</option>
                                    <?php
                $firstYear = (int) date('Y') - 20;
                $lastYear = $firstYear + 30;
                for ($i = $firstYear; $i <= $lastYear; $i++) { ?> @if ($max_year == $i)
                                        <option value="<?= $i ?>" selected><?= $i ?></option>
                                    @else
                                        <option value="<?= $i ?>"><?= $i ?></option>
                                    @endif
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                                <label for="exampleSelect1">@lang('public.rewallet.report_month') </label>
                                <select class="form-control class_select2" id="search_report_month"
                                    name="search_report_month" style="width: 100%">
                                    <option value="">@lang('public.public.pilih')</option>
                                    <?php foreach ($bulan as $key_bulan => $val_bulan): ?>
                                    @if ($max_month == $val_bulan->param_value)
                                        <option value="<?= $val_bulan->param_value ?>" selected>
                                            <?= $val_bulan->param_nama ?></option>
                                    @else
                                        <option value="<?= $val_bulan->param_value ?>"><?= $val_bulan->param_nama ?>
                                        </option>
                                    @endif
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-lg-0.7 kt-margin-b-10-tablet-and-mobile">
                                <label for="exampleSelect1">&nbsp;</label>
                                <button class="form-control btn btn-primary btn-brand--icon" id="btn_generate"
                                    type="button">
                                    <span>
                                        <i class="la la-download"></i>
                                        <span>Generate</span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </form>
                    <!--end: Search Form -->

                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>

    <!-- begin:: Footer -->
    <?php include 'resources/views/include/footer.php'; ?>
    <!-- end:: Footer -->
    </div>
    </div>
    </div>
    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <?php include 'resources/views/include/loadjs.php'; ?>
    <!--end::Page Scripts -->
</body>
<script src="<?php echo env('APP_URLAPP'); ?>/assets/javascript/report/all_report.js"></script>
<!-- end::Body -->

</html>
