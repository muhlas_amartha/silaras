<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<?php include 'resources/views/include/head.php'; ?>
<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <?php include 'resources/views/include/navbar.php'; ?>

    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        <?php echo env('APP_PROJECTNAME'); ?> </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            @lang('public.profil.modul') </a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            @lang('public.profil.judul') </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--mobile" id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            @lang('public.profil.judul')
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="la la-download"></i> Export
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__section kt-nav__section--first">
                                                <span class="kt-nav__section-text">Choose an option</span>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="javascript:void(0);" class="kt-nav__link"
                                                    id="btn_export_xlsx">
                                                    <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                    <span class="kt-nav__link-text">XLSX</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="javascript:void(0);" class="kt-nav__link"
                                                    id="btn_export_csv">
                                                    <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                    <span class="kt-nav__link-text">CSV</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                &nbsp;
                                <input type="hidden" id="APP_URLAPP" value="{{ env('APP_URLAPP') }}">
                                <btn class="btn btn-brand btn-elevate btn-icon-sm " id="btn_edit">
                                    <i class="la la-save"></i>
                                    Save @lang('public.profil.judul')
                            </div>
                        </div>
                    </div>
                </div>


                <div class="kt-portlet__body">
                    <!-- begin:: Alert -->
                    @include('include.alert')
                    <!-- end:: Alert -->
                    <!-- PUBLIC -->
                    <input type="hidden" id="id_user" value="{{ $user->id_user }}">
                    <input type="hidden" id="APP_URLAPP" value="{{ env('APP_URLAPP') }}">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="locate" value="{{ $locate }}">

                    <!-- BLADE -->
                    <input type="hidden" id="judul_blade" value="@lang('public.profil.judul')">
                    <input type="hidden" id="code_mobile_platfrom" value="{{ $code_mobile_platfrom }}">
                    <!--begin: Datatable -->
                    <form class="kt-form kt-form--label-right form_profil" method="post" action=""
                        enctype="multipart/form-data" autocomplete="off">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="locate" value="{{ $locate }}">
                        <input type="hidden" name="id" id="id" />
                        <input type="hidden" name="general_information_after" id="general_information_after" />
                        <input type="hidden" name="component_desc" id="component_desc" />
                        <input type="hidden" name="component_code" id="component_code" />

                        <table class="table table-striped- table-hover table-checkable">
                            <thead>
                                <tr>
                                    <th width="5%">@lang('public.public.no')</th>
                                    <th width="10%">@lang('public.profil.flag')</th>
                                    <th width="15%">@lang('public.profil.kode_komponen')</th>
                                    <th width="25%">@lang('public.profil.uraian')</th>
                                    <th>@lang('public.profil.info_umum')</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($data as $key => $list): ?>
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $list->flag_detail }}</td>
                                    <td>{{ $list->component_code }}</td>
                                    <td>{{ $list->component_desc }}</td>
                                    <td>
                                        <div class="validated">
                                            @if ($list->component_code == '170000000000')
                                                <!-- Dati1 -->
                                                <select class="form-control kt-select2 class_select2" id="provinsi"
                                                    name="general_information[<?php echo $list->t_id; ?>]" style="width: 100%">
                                                    <option value=""> -- SELECT -- </option>
                                                    <?php foreach ($provinsi as $key_prov => $prov): ?>
                                                    @if ($prov->province_code == $list->general_information)
                                                        <option value="<?= $prov->province_code ?>" selected>
                                                            <?= $prov->province_name ?></option>
                                                    @else
                                                        <option value="<?= $prov->province_code ?>">
                                                            <?= $prov->province_name ?></option>
                                                    @endif
                                                    <?php endforeach; ?>
                                                </select>
                                            @elseif($list->component_code == '180000000000')
                                                <!-- Dati2 -->
                                                <select class="form-control kt-select2 class_select2" id="kabkota"
                                                    name="general_information[<?php echo $list->t_id; ?>]" style="width: 100%">
                                                    <option value=""> -- SELECT -- </option>
                                                    <?php foreach ($kabkota as $key_dati => $kabkot): ?>
                                                    <option value="<?= $kabkot->city_code ?>" selected>
                                                        <?= $kabkot->city_name ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            @else
                                                <input type="text" class="form-control"
                                                    id="general_information-<?php echo $list->t_id; ?>"
                                                    name="general_information[<?php echo $list->t_id; ?>]"
                                                    value="{{ $list->general_information }}"
                                                    onchange="changeUraian(<?php echo $list->t_id; ?>)">
                                            @endif
                                            <div class="invalid-feedback alert-<?php echo $list->t_id; ?>"></div>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach;
                                ?>
                            </tbody>
                        </table>


                    </form>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>

        @include('include.modal')
        <!-- end:: Content -->
    </div>

    <!-- begin:: Footer -->
    <?php include 'resources/views/include/footer.php'; ?>
    <!-- end:: Footer -->
    </div>
    </div>
    </div>
    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <?php include 'resources/views/include/loadjs.php'; ?>
    <!--end::Page Scripts -->
</body>
<script src="<?php echo env('APP_URLAPP'); ?>/assets/javascript/profil/profil.js"></script>
<!-- end::Body -->

</html>

<script>
    function changeUraian(t_id) {

        $.ajax({
            url: url + 'getvalidator',
            type: "GET",
            dataType: "json",
            data: "id=" + t_id,
            success: function(response, textStatus, jqXHR) {
                var form_profil = '.form_profil';
                var pesan_mandatory = 'This field is required';
                var code_mobile_platfrom = $('#code_mobile_platfrom').val();
                var general_information = $('#general_information-' + t_id).val();

                return_submit = 0
                // $(form_profil+' .alert-'+t_id).html('');

                //COMPONENT VALIDATOR
                if (response.component_validator == 'M') {
                    if (general_information == '' || general_information == null) {
                        $(form_profil + ' .alert-' + t_id).html(pesan_mandatory);
                    } else {
                        return_submit = 1
                    }
                } else if (response.component_validator == 'C') {
                    if (response.component_code == '050000000000') { //t_id nama_mobile_platfrom
                        if (code_mobile_platfrom == '' || code_mobile_platfrom == null) {
                            return_submit = 1
                        } else {
                            if (general_information == '' || general_information == null) {
                                $(form_profil + ' .alert-' + t_id).html(
                                    'Jika Mobile Platform ada, maka Nama Mobile Platform Wajib Diisi');
                            } else {
                                return_submit = 1
                            }
                        }
                    } else {
                        return_submit = 1
                    }
                } else {
                    return_submit = 1;
                }

                //COMPONENT LENGTH


                if (return_submit == 1) {
                    // document.getElementById("btn_edit").disabled = false;
                    $("#btn_edit").removeClass("disabled");
                    $('#btn_edit').click(function() {
                        console.log('btn_edit click')
                        $(form_profil).attr('action', '' + url + 'edit');
                        $(form_profil).submit();
                    });
                } else {
                    $("#btn_edit").addClass("disabled");
                }
            }
        });
    }
</script>
