
<!DOCTYPE html>
<html lang="en">

    <!-- begin::Head -->
        <?php include 'resources/views/include/head.php' ?>

    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

        <?php include 'resources/views/include/navbar.php' ?>

                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        <!-- begin:: Subheader -->
                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid ">
                                <div class="kt-subheader__main">
                                    <h3 class="kt-subheader__title">
                                        <?php echo env('APP_PROJECTNAME')?> </h3>
                                    <span class="kt-subheader__separator kt-hidden"></span>
                                    <div class="kt-subheader__breadcrumbs">
                                        <a class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                        <span class="kt-subheader__breadcrumbs-separator"></span>
                                        <a class="kt-subheader__breadcrumbs-link">
                                            @lang('public.rpayment.modul') </a>
                                        <span class="kt-subheader__breadcrumbs-separator"></span>
                                        <a class="kt-subheader__breadcrumbs-link">
                                            @lang('public.rpayment.judul') </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- end:: Subheader -->

                        <!-- begin:: Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div class="kt-portlet kt-portlet--mobile" id="kt_page_portlet">

                                <!-- begin:: export and add -->
                                <div class="kt-portlet__head kt-portlet__head--lg">
                                    <div class="kt-portlet__head-label">
                                        <span class="kt-portlet__head-icon">
                                            <i class="kt-font-brand flaticon2-line-chart"></i>
                                        </span>
                                        <h3 class="kt-portlet__head-title">
                                            @lang('public.rpayment.judul')
                                        </h3>
                                    </div>

                                    @include('include.export')
                                </div>
                                <!-- end:: export and add -->

                                <!-- begin:: Alert -->
                                    @include('include.alert')
                                <!-- end:: Alert -->
                                <div class="kt-portlet__body">
                                    <!-- PUBLIC -->
                                    <input type="hidden" id="id_user" value="{{{ $user->id_user }}}">
                                    <input type="hidden" id="APP_URLAPP" value="{{{ env('APP_URLAPP') }}}">
                                    <input type="hidden" name="_token" id="_token" value="{{{csrf_token()}}}">
                                    <input type="hidden" id="locate" value="{{{ $locate }}}">

                                    <!-- BLADE -->
                                    <input type="hidden" id="judul_blade" value="@lang('public.rpayment.judul')">

                                    <!-- begin:: Search -->
                                        @include('include.search')
                                    <!-- end:: Search -->
                                    <hr>

                                    <form class="kt-form kt-form--label-right form_rpayment" method="post" action="" enctype="multipart/form-data" autocomplete="off">
                                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                        <input type="hidden" name="locate" value="{{{ $locate }}}">
                                        <input type="hidden" name="id" id="id" />
                                        <input type="hidden" name="component_code_value" id="component_code_value" />
                                        <input type="hidden" name="component_desc_value" id="component_desc_value" />
                                        <input type="hidden" name="general_information_value" id="general_information_value" />
                                        <table class="table table-striped- table-hover table-checkable dt_rpayment">
                                            <thead>
                                                <tr>
                                                    <th>@lang('public.public.no')</th>
                                                    <th>@lang('public.rpayment.flag_detail')</th>
                                                    <th>@lang('public.rpayment.component_code')</th>
                                                    <th>@lang('public.rpayment.component_desc')</th>
                                                    <th>@lang('public.rpayment.general_information')</th>
                                                    <th>@lang('public.public.aksi')</th>
                                                </tr>
                                            </thead>

                                        </table>
                                    </form>
                                    <!--end: Datatable -->
                                </div>
                            </div>
                        </div>

                        @include('include.modal')
                        <!-- end:: Content -->
                    </div>

                    <!-- begin:: Footer -->
                        <?php include 'resources/views/include/footer.php' ?>
                    <!-- end:: Footer -->
                </div>
            </div>
        </div>
        <!-- end:: Page -->

        <!-- begin::Global Config(global config for global JS sciprts) -->
            <?php include 'resources/views/include/loadjs.php' ?>
        <!--end::Page Scripts -->
    </body>
<script src="<?php echo env('APP_URLAPP')?>/assets/javascript/profil/rincian_payment.js"></script>
    <!-- end::Body -->
</html>

<!--begin::Modal Add-->
<div class="modal fade modal_add" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('public.public.btn_add') @lang('public.rpayment.judul')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form class="kt-form kt-form--label-right form_add" method="post" action="" enctype="multipart/form-data" autocomplete="off">
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <input type="hidden" name="id" id="id" />
                    <input type="hidden" name="action_flag" id="action_flag" />
                    <input type="hidden" name="report_year" id="report_year" />
                    <input type="hidden" name="report_month" id="report_month" />
                    <div class="form-group">
                        <label class="form-control-label">@lang('public.rpayment.component_code') <span style="color:red">*</span></label>
                        <input type="text" class="form-control" id="component_code" name="component_code" maxlength="12">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">@lang('public.rpayment.component_desc') <span style="color:red">*</span></label>
                        <input type="text" class="form-control" id="component_desc" name="component_desc">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">@lang('public.rpayment.general_information') <span style="color:red">*</span></label>
                        <input type="text" class="form-control" id="general_information" name="general_information">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-yes-add">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal Add-->
<script>
