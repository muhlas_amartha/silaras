
<!DOCTYPE html>
<html lang="en">

    <!-- begin::Head -->
        <?php include 'resources/views/include/head.php' ?>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

        <?php include 'resources/views/include/navbar.php' ?>
        
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        <!-- begin:: Subheader -->
                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid ">
                                <div class="kt-subheader__main">
                                    <h3 class="kt-subheader__title">
                                        User </h3>
                                    <span class="kt-subheader__separator kt-hidden"></span>
                                    <div class="kt-subheader__breadcrumbs">
                                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                        <span class="kt-subheader__breadcrumbs-separator"></span>
                                        <a href="" class="kt-subheader__breadcrumbs-link">
                                            @lang('public.user.modul') </a>
                                        <span class="kt-subheader__breadcrumbs-separator"></span>
                                        <a href="" class="kt-subheader__breadcrumbs-link">
                                            @lang('public.user.judul') </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- end:: Subheader -->

                        <!-- begin:: Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--lg">
                                    <div class="kt-portlet__head-label">
                                        <span class="kt-portlet__head-icon">
                                            <i class="kt-font-brand flaticon2-line-chart"></i>
                                        </span>
                                        <h3 class="kt-portlet__head-title">
                                            @lang('public.user.judul')
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-toolbar">
                                        <div class="kt-portlet__head-wrapper">
                                            <div class="kt-portlet__head-actions">
                                                <div class="dropdown dropdown-inline">
                                                    <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="la la-download"></i> Export
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <ul class="kt-nav">
                                                            <li class="kt-nav__section kt-nav__section--first">
                                                                <span class="kt-nav__section-text">Choose an option</span>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="user/export/?type=xls" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                                    <span class="kt-nav__link-text">Excel</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="user/export/?type=csv" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                                    <span class="kt-nav__link-text">CSV</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="user/export/?type=pdf" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                                                    <span class="kt-nav__link-text">PDF</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                &nbsp;
                                                <input type="hidden" id="APP_URLAPP" value="{{{env('APP_URLAPP')}}}">
                                                <btn class="btn btn-brand btn-elevate btn-icon-sm" id="btn-add">
                                                    <i class="la la-plus"></i>
                                                    @lang('public.public.btn_add') @lang('public.user.judul')
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- begin:: Alert -->
                                    @include('include.alert ')
                                <!-- end:: Alert -->
                                <div class="kt-portlet__body">
                                    <!-- PUBLIC -->
                                    <input type="hidden" id="id_user" value="{{{ $user->id_user }}}">
                                    <input type="hidden" id="APP_URLAPP" value="{{{ env('APP_URLAPP') }}}">
                                    <input type="hidden" name="_token" id="_token" value="{{{csrf_token()}}}">
                                    <input type="hidden" id="locate" value="{{{ $locate }}}">

                                    <!-- BLADE -->
                                    <input type="hidden" id="judul_blade" value="@lang('public.user.judul')">
                                    <!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable dt_user">
                                        <thead>
                                            <tr>
                                                <th>@lang('public.public.no')</th>
                                                <th>@lang('public.user.username')</th>
                                                <th>@lang('public.user.nama')</th>
                                                <th>@lang('public.user.email')</th>
                                                <th>@lang('public.user.nik')</th>
                                                <th>@lang('public.public.divisi')</th>
                                                <th>@lang('public.user.role')</th>
                                                <th>@lang('public.public.created_by')</th>
                                                <th>@lang('public.public.created_on')</th>
                                                <th>@lang('public.public.aksi')</th>
                                            </tr>
                                        </thead>
                                    </table>

                                    <!--end: Datatable -->
                                </div>
                            </div>
                        </div>
                        
                        @include('include.modal')
                        <!-- end:: Content -->
                    </div>

                    <!-- begin:: Footer -->
                        <?php include 'resources/views/include/footer.php' ?>
                    <!-- end:: Footer -->
                </div>
            </div>
        </div>
        <!-- end:: Page -->

        <!-- begin::Global Config(global config for global JS sciprts) -->
            <?php include 'resources/views/include/loadjs.php' ?>
        <!--end::Page Scripts -->
    </body>
<script src="/<?php echo env('APP_URLAPP')?>/assets/javascript/administration/user.js"></script>
    <!-- end::Body -->
</html>

<!--begin::Modal Add-->
<div class="modal fade modal_add" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('public.public.btn_add') @lang('public.user.judul')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form class="kt-form kt-form--label-right form_add" method="post" action="" enctype="multipart/form-data" autocomplete="off">
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <input type="hidden" name="id" id="id" />
                    <input type="hidden" name="action_flag" id="action_flag" />
                    <div class="form-group">
                        <label class="form-control-label">@lang('public.user.first_name') <span style="color:red">*</span></label>
                        <input type="text" class="form-control" id="first_name" name="first_name">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">@lang('public.user.last_name') </label>
                        <input type="text" class="form-control" id="last_name" name="last_name">
                    </div>
                    <div class="form-group ">
                        <label>@lang('public.user.username') <span style="color:red">*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-user kt-font-brand"></i></span></div>
                            <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1" name="username" id="username">
                        </div>
                    </div>
                    <div class="form-group" id="div_password">
                        <label>@lang('public.user.password') <span style="color:red">*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-key kt-font-brand"></i></span></div>
                            <input type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" name="password" id="password">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label>@lang('public.user.email')</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                            <input type="text" class="form-control" placeholder="Email" aria-describedby="basic-addon1" name="email" id="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">@lang('public.user.nik') </label>
                        <input type="text" class="form-control" id="nik" name="nik">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">@lang('public.user.phone_number')</label>
                        <input type="text" class="form-control" id="phone_number" name="phone_number">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">@lang('public.public.divisi') <span style="color:red">*</span></label>
                        <select class="form-control class_select2" id="id_divisi" name="id_divisi" style="width: 100%">
                            <option value="">Please Choose</option>
                            <?php foreach($m_divisi as $key => $list): ?>
                              <option value="<?=$list->id_divisi;?>"><?=$list->nama_divisi;?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">@lang('public.user.role') <span style="color:red">*</span></label>
                        <select class="form-control class_select2" id="id_role" name="id_role" style="width: 100%">
                            <option value="">Please Choose</option>
                            <?php foreach($sec_role as $key => $list): ?>
                              <option value="<?=$list->id_role;?>"><?=$list->role_name;?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-yes-add">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal Add-->

<!--start::Modal View-->
<div class="modal fade modal_view_user" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('public.public.detail') @lang('public.user.judul')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <table class="table" id="tableviewdata">
                    <tr><td width="30%">@lang('public.user.first_name')</td><td width="5%">:</td><td><span id="first_name"></span></td></tr>
                    <tr><td>@lang('public.user.last_name')</td><td>:</td><td><span id="last_name"></span></td></tr>
                    <tr><td>@lang('public.user.username')</td><td>:</td><td><span id="username"></span></td></tr>
                    <tr><td>@lang('public.user.email')</td><td>:</td><td><span id="email"></span></td></tr>
                    <tr><td>@lang('public.user.nik')</td><td>:</td><td><span id="nik"></span></td></tr>
                    <tr><td>@lang('public.user.phone_number')</td><td>:</td><td><span id="phone_number"></span></td></tr>
                    <tr><td>@lang('public.public.divisi')</td><td>:</td><td><span id="divisi"></span></td></tr>
                    <tr><td>@lang('public.user.role')</td><td>:</td><td><span id="role"></span></td></tr>

                    <tr><td>@lang('public.public.created')</td><td>:</td><td><span id="created"></span></td></tr>
                    <tr id="tr_updated" style="display: none"><td>@lang('public.public.updated')</td><td>:</td><td><span id="updated"></span></td></tr>
                    <tr><td colspan="3" bgcolor="#737373" style="width:1cm"/ ></td></tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!--start::Modal View-->
