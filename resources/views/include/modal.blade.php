
<!--begin::Modal Alert-->
<div class="modal modal-stick-to-bottom fade modal_alert" id="kt_modal_7" role="dialog" data-backdrop="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--label-right form_alert" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                    <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                    <input type="hidden" name="id_master" id="id_master">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="nama" id="nama">
                    <span id="span_alert"></span>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-yes-alert" id="btn-yes-alert">Proses</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal Alert-->


<!--begin::Modal Export-->
<div class="modal modal-stick-to-bottom fade modal_export" id="kt_modal_7" role="dialog" data-backdrop="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--label-right form_export" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                    <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                    <input type="hidden" name="type" id="type">
                    <input type="hidden" name="param" id="param">
                </form>
            </div>
        </div>
    </div>
</div>
<!--end::Modal Export-->