
                                    <div class="kt-portlet__head-toolbar">
                                        <div class="kt-portlet__head-wrapper">
                                            <div class="kt-portlet__head-actions">
                                                <btn class="btn btn-default btn-elevate btn-icon-sm" id="btn_export_csv">
                                                    <i class="la la-download"></i>
                                                    @lang('public.public.export')
                                                </btn>
                                                &nbsp;
                                                <input type="hidden" id="APP_URLAPP" value="{{{env('APP_URLAPP')}}}">
                                                <btn class="btn btn-brand btn-elevate btn-icon-sm" id="btn_add">
                                                    <i class="la la-plus"></i>
                                                    @lang('public.public.btn_add')
                                                </btn>
                                                &nbsp;
                                                
                                            </div>
                                        </div>
                                    </div>