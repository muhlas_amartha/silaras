<head>
    <base href="../../../">
    <meta charset="utf-8" />
    <title>Silaras</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700"> -->

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="<?php echo env('APP_URLAPP') ?>/assets/css/pages/login/login-3.css" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?php echo env('APP_URLAPP') ?>/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo env('APP_URLAPP') ?>/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?php echo env('APP_URLAPP') ?>/assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo env('APP_URLAPP') ?>/assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo env('APP_URLAPP') ?>/assets/css/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo env('APP_URLAPP') ?>/assets/css/skins/aside/dark.css" rel="stylesheet" type="text/css" />

    <!--end::Layout Skins -->
    <!-- <link rel="shortcut icon" href="assets/media/logos/favicon.ico" /> -->
    <style>
        .dataTables_filter {
            float: right;
            text-align: right;
        }

        .dataTables_paginate {
            /*width: 50%;*/
            float: right;
            text-align: right;
        }

        /* @media screen and (max-width: 1000px) {
				.responsive-neraca {
					height: 700px;
				}
			} */
    </style>
    <style>
        body {
            font-family: 'Roboto';
        }
    </style>
    <!--
        <style>
        table, th, td {
          border: 0px solid black;
        }
        </style> -->
    <script src="<?php echo env('APP_URLAPP') ?>/assets/js/ckeditor.js"></script>
</head>
