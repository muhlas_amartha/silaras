<div class="kt-section">
    <div class="kt-section__content">

        @if (Session::has('message'))
            <div class="alert alert-success fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-warning" style="font-size: 120%"></i></div>
                <div class="alert-text"><?php echo Session::get('message'); ?></div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        @endif

        @if (Session::has('error_message'))
            <div class="alert alert-warning fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-warning" style="font-size: 120%"></i></div>
                <div class="alert-text"><?php echo Session::get('error_message'); ?></div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        @endif

        {{-- @if (isset($error_log))
            @foreach ($error_log as $rows)
                @php
                    $data = json_decode($rows->error_log);
                    // count($data);
                    // dd(count($data));
                    $no = 1;
                @endphp
                @foreach ($data as $m)
                    <div class="alert alert-warning" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning" style="font-size: 120%"></i></div>
                        <div class="alert-text">{{ $m->message }}</div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="la la-close"></i></span>
                            </button>
                        </div>
                    </div>
                @endforeach

            @endforeach
        @endif --}}

    </div>
</div>

<input type="hidden" id="pesan_error" value="@lang('public.public.pesan_error')">
<input type="hidden" id="pesan_berhasil_simpan" value="@lang('public.public.pesan_berhasil_simpan')">
<input type="hidden" id="pesan_gagal_simpan" value="@lang('public.public.pesan_gagal_simpan')">
<input type="hidden" id="pesan_berhasil_hapus" value="@lang('public.public.pesan_berhasil_hapus')">
<input type="hidden" id="pesan_gagal_hapus" value="@lang('public.public.pesan_gagal_hapus')">
<input type="hidden" id="pesan_judul_hapus" value="@lang('public.public.pesan_judul_hapus')">
<input type="hidden" id="pesan_ket_hapus" value="@lang('public.public.pesan_ket_hapus')">
<input type="hidden" id="pesan_ya" value="@lang('public.public.pesan_ya')">
<input type="hidden" id="pesan_tidak" value="@lang('public.public.pesan_tidak')">
<input type="hidden" id="pesan_mandatory" value="@lang('public.public.pesan_mandatory')">
