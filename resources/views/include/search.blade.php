<!--begin: Search Form -->
<form class="kt-form kt-form--fit kt-margin-b-20">
    <div class="row kt-margin-b-20">
        <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
            <label for="exampleSelect1">@lang('public.rewallet.report_year') </label>
            <select class="form-control class_select2" id="search_report_year" name="search_report_year"
                style="width: 100%">
                <option value="">Please Choose</option>
                <?php
                $firstYear = (int) date('Y') - 20;
                $lastYear = $firstYear + 30;
                for ($i = $firstYear; $i <= $lastYear; $i++) { ?> @if ($max_year == $i)
                    <option value="<?= $i ?>" selected><?= $i ?></option>
                @else
                    <option value="<?= $i ?>"><?= $i ?></option>
                @endif
                <?php } ?>
            </select>
        </div>
        <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
            <label for="exampleSelect1">@lang('public.rewallet.report_month') </label>
            <select class="form-control class_select2" id="search_report_month" name="search_report_month"
                style="width: 100%">
                <option value="">@lang('public.public.pilih')</option>
                <?php foreach ($bulan as $key_bulan => $val_bulan): ?>
                @if ($max_month == $val_bulan->param_value)
                    <option value="<?= $val_bulan->param_value ?>" selected><?= $val_bulan->param_nama ?></option>
                @else
                    <option value="<?= $val_bulan->param_value ?>"><?= $val_bulan->param_nama ?></option>
                @endif
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-lg-0.7 kt-margin-b-10-tablet-and-mobile">
            <label for="exampleSelect1">&nbsp;</label>
            <button class="form-control btn btn-primary btn-brand--icon" id="btn_search" type="button">
                <span>
                    <i class="la la-search"></i>
                    <span>@lang('public.public.cari')</span>
                </span>
            </button>
        </div>
    </div>
</form>
<!--end: Search Form -->
