<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            <?php echo date('Y'); ?>&nbsp;&copy;&nbsp;<a href="" target="_blank" class="kt-link">Asta Protek Jiarsi</a>
        </div>
        <div class="kt-footer__menu">
            <a href="#" class="kt-footer__menu-link kt-link">Silaras 1.0.0</a>
        </div>
    </div>
</div>
