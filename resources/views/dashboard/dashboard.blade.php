
<!DOCTYPE html>
<html lang="en">

    <!-- begin::Head -->
        <?php include 'resources/views/include/head.php' ?>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

        <?php include 'resources/views/include/navbar.php' ?>
        
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        <!-- begin:: Subheader -->
                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid ">
                                <div class="kt-subheader__main">
                                    <h3 class="kt-subheader__title">
                                        Silaras </h3>
                                    <span class="kt-subheader__separator kt-hidden"></span>
                                    <div class="kt-subheader__breadcrumbs">
                                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                        <span class="kt-subheader__breadcrumbs-separator"></span>
                                        <a href="<?php echo env('APP_URLAPP')?>/dashboard/" class="kt-subheader__breadcrumbs-link">
                                           Dashboard </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- end:: Subheader -->

                        <!-- begin:: Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--lg">
                                    <div class="kt-portlet__head-label">
                                        <span class="kt-portlet__head-icon">
                                            <i class="kt-font-brand flaticon2-line-chart"></i>
                                        </span>
                                        <h3 class="kt-portlet__head-title">
                                            Dashboard
                                        </h3>
                                    </div>
                                </div>

                                <!-- begin:: Alert -->
                                    @include('include.alert')
                                <!-- end:: Alert -->
                                <div class="kt-portlet__body">
                                    Dashboard
                                </div>
                            </div>
                        </div>
                        
                        <!-- end:: Content -->
                    </div>

                    <!-- begin:: Footer -->
                        <?php include 'resources/views/include/footer.php' ?>
                    <!-- end:: Footer -->
                </div>
            </div>
        </div>
        <!-- end:: Page -->
    </body>
    <!-- end::Body -->
    
            <?php include 'resources/views/include/loadjs.php' ?>
</html>