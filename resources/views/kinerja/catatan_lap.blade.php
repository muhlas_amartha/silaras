<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<?php include 'resources/views/include/head.php'; ?>
<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <?php include 'resources/views/include/navbar.php'; ?>

    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Silaras </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="<?php echo env('APP_URLAPP'); ?>/dashboard/"
                            class="kt-subheader__breadcrumbs-link">
                            Laporan Kinerja Keuangan </a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Catatan Atas Laporan Keuangan </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <form action="<?php echo env('APP_URLAPP'); ?>/lap_cat/export_to_pdf"
            method="POST" enctype="multipart/form-data" target="_blank">
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Catatan Atas Laporan Keuangan
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <input type="hidden" id="APP_URLAPP" value="{{ env('APP_URLAPP') }}">
                                    <button type="button" class="btn btn-success btn-elevate btn-icon-sm " id="btn_add">
                                        <i class="la la-plus"></i>
                                        Add New Report
                                    </button>
                                    {{-- <button type="submit" class="btn btn-danger btn-elevate btn-icon-sm "
                                        id="btn_export">
                                        <i class="la la-print"></i>
                                        Export to Pdf
                                    </button> --}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- begin:: Alert -->
                    @include('include.alert')
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <!-- end:: Alert -->
                    <div class="kt-portlet__body">
                        <!-- PUBLIC -->
                        <input type="hidden" id="id_user" value="{{ $user->id_user }}">
                        <input type="hidden" id="APP_URLAPP" value="{{ env('APP_URLAPP') }}">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="locate" value="{{ $locate }}">

                        <!-- BLADE -->
                        <input type="hidden" id="judul_blade" value="@lang('public.lneraca.judul')">

                        <!-- begin:: Search -->
                        @include('include.search')
                        <!-- end:: Search -->

                    </div>
                </div>
            </div>
        </form>

        <!-- end:: Content -->
    </div>

    <!-- begin:: Footer -->
    <?php include 'resources/views/include/footer.php'; ?>
    <!-- end:: Footer -->
    </div>
    </div>
    </div>
    <!-- end:: Page -->
</body>
<!-- end::Body -->
<div class="modal fade" id="viewReport" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalHeading"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="kt-form kt-form--fit kt-margin-b-20" id="form_report" method="POST" action="">
                <div class="modal-body">
                    <!--begin: Search Form -->
                    <div class="row kt-margin-b-20">
                        <div class="col-lg-6 kt-margin-b-10-tablet-and-mobile">
                            <label for="exampleSelect1">@lang('public.rewallet.report_year') </label>
                            <select class="form-control class_select2" id="report_year_add" name="report_year"
                                style="width: 100%">
                                <option value="">Please Choose</option>
                                <?php
                                $firstYear = (int) date('Y') - 20;
                                $lastYear = $firstYear + 30;
                                for ($i = $firstYear; $i <= $lastYear; $i++) { ?> @if ($max_year == $i) <option value="<?= $i ?>" selected><?= $i ?></option>
@else
                                <option value="<?= $i ?>"><?= $i ?></option> @endif <?php } ?> </select>
                        </div>
                        <div class="col-lg-6 kt-margin-b-10-tablet-and-mobile">
                            <label for="exampleSelect1">@lang('public.rewallet.report_month') </label>
                            <select class="form-control class_select2" id="report_month_add" name="report_month"
                                style="width: 100%">
                                <option value="">@lang('public.public.pilih')</option>
                                <?php foreach ($bulan as $key_bulan => $val_bulan): ?>
                                @if ($max_month == $val_bulan->param_value)
                                    <option value="<?= $val_bulan->param_value ?>" selected><?= $val_bulan->param_nama ?></option>
@else
                                    <option value="<?= $val_bulan->param_value ?>"><?= $val_bulan->param_nama ?></option>
                                @endif
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-lg-12 kt-margin-b-10-tablet-and-mobile">
                            <br>
                            <label for="exampleSelect1">Isi Report</label>
                            <input type="hidden" value="{{ $max_year }}" name="max_year" id="max_year">
                            <input type="hidden" value="{{ $max_month }}" name="max_month" id="max_month">
                            <textarea id="editor" name="redaksi" data-sample-short></textarea>
                            <textarea id="note" name="note" style="display:none;"></textarea>
                        </div>
                        <script>
                            // These styles are used to provide the "page view" display style like in the demo and matching styles for export to PDF.
                        </script>
                    </div>
            <!--end: Search Form -->
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-brand--icon mr-auto" id="btnSave" type="submit">Save & Export to pdf</button>
                <button class="btn btn-success btn-brand--icon mr-auto" id="btnExport" type="submit">Export to pdf</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>
    </div>
</div>

<?php include 'resources/views/include/loadjs.php'; ?>

<script>
    $(document).ready(function() {

    });
    APP_URLAPP = $('#APP_URLAPP').val();
    var redaksi = $('#editor').val();
    var token = $('#_token').val();

    $('body').on('click', '#btn_add', function() {

        // console.log(CKEDITOR.instances.editor.getData());
        $('#btn_add').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled',
            true);
        var report_month = 0;
        var report_year = 0;
        var max_month = $('#max_month').val();
        var max_year = $('#max_year').val();
        $.ajax({
            url: APP_URLAPP + "/lap_cat/get_data?report_month=" + report_month +
                "&report_year=" + report_year,
            method: "GET",
            processData: false,
            contentType: false,
            success: function(result) {
                $('#editor').val(result.response.note);
                $('#modalHeading').html('Add New Report');
                $('#btnSave').show();
                $('#btnExport').hide();
                $('#report_month_add').prop('disabled', false);
                $('#report_year_add').prop('disabled', false);
                $('#report_month_add').val(max_month).trigger('change');
                $('#report_year_add').val(max_year).trigger('change');
                $('#viewReport').modal('show');
                load_ckeditor();
                $('#btn_add').removeClass(
                    'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr(
                    'disabled', false);
            },
            error: function(er) {}
        });

    });

    $('body').on('click', '#btnSave', function(e) {
        e.preventDefault();
        $('#btnSave').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled',
            true);
        $('#note').html(CKEDITOR.instances.editor.getData());
        var form = new FormData($("#form_report")[0]);
        // console.log(form);
        $.ajax({
            url: APP_URLAPP + "/lap_cat/send_data?_token=" + token,
            method: "POST",
            data: form,
            processData: false,
            contentType: false,
            success: function(result) {
                if (result.response == 'failedNumrow') {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top',
                        showConfirmButton: false,
                        timerProgressBar: true,
                        timer: 5000
                    });
                    Toast.fire({
                        type: 'error',
                        title: '&nbsp;&nbsp;Report Sudah Ada !'
                    });
                    $('#btnSave').removeClass(
                        'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr(
                        'disabled', false);
                } else {
                    $('#viewReport').modal('hide');
                    $('#btnSave').removeClass(
                        'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr(
                        'disabled', false);
                    // const Toast = Swal.mixin({
                    //     toast: true,
                    //     position: 'top',
                    //     showConfirmButton: false,
                    //     timerProgressBar: true,
                    //     timer: 5000
                    // });
                    // Toast.fire({
                    //     type: 'success',
                    //     title: '&nbsp;&nbsp;Report berhasil di simpan !'
                    // });
                    Swal.fire({
                        title: 'Berhasil !',
                        text: "Report berhasil disimpan !",
                        type: 'success',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Export Report'
                    }).then((a) => {
                        console.log(a);
                        if (a.value) {
                            window.open(APP_URLAPP +
                                '/lap_cat/export_to_pdf?report_month=' +
                                result.report_month + '&report_year=' + result
                                .report_year,
                                '_blank');
                            // Swal.fire(
                            //     'Deleted!',
                            //     'Your file has been deleted.',
                            //     'success'
                            // )
                        }
                    })
                }

            },
            error: function(er) {}
        });

    });

    $('body').on('click', '#btnExport', function(e) {
        e.preventDefault();
        var report_month = $('#report_month_add').val();
        var report_year = $('#report_year_add').val();
        window.open(APP_URLAPP +
            '/lap_cat/export_to_pdf?report_month=' +
            report_month + '&report_year=' + report_year,
            '_blank');


    });

    $('body').on('click', '#btn_search', function() {
        // var report_month = 0;
        // var report_year = 0;
        $('#btn_search').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr(
            'disabled', true);
        var report_month = $('#search_report_month').val();
        var report_year = $('#search_report_year').val();
        $.ajax({
            url: APP_URLAPP + "/lap_cat/get_data?report_month=" + report_month +
                "&report_year=" + report_year,
            method: "GET",
            processData: false,
            contentType: false,
            success: function(result) {
                // console.log(result.response.note);
                if (result.response) {
                    $('#editor').val(result.response.note);
                    $('#report_month_add').val(result.response.report_month).trigger('change');
                    $('#report_year_add').val(result.response.report_year).trigger('change');
                    $('#report_month_add').prop('disabled', true);
                    $('#report_year_add').prop('disabled', true);
                    $('#modalHeading').html('View Report');
                    $('#btnSave').hide();
                    $('#btnExport').show();
                    $('#viewReport').modal('show');
                    load_ckeditor();
                    CKEDITOR.instances.editor.config.readOnly = true;
                } else {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top',
                        showConfirmButton: false,
                        timerProgressBar: true,
                        timer: 5000
                    });
                    Toast.fire({
                        type: 'error',
                        title: '&nbsp;&nbsp;Bulan ' + bulanDesc(report_month) + ', ' +
                            report_year +
                            ' Belum Ada Report !'
                    });
                }

                $('#btn_search').removeClass(
                    'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr(
                    'disabled', false);
            },
            error: function(er) {}
        });

    });

    $('#viewReport').on('hidden.bs.modal', function() {
        // $('#cke_editor').remove();
        CKEDITOR.instances.editor.destroy();
        // console.log("modal hidden");
    });

    function load_ckeditor() {
        CKEDITOR.addCss(
            'body.document-editor { margin: 0.5cm auto; border: 1px #D3D3D3 solid; border-radius: 5px; background: white; box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); }' +
            'body.document-editor, div.cke_editable { width: 700px; padding: 1cm 2cm 2cm; } ' +
            'body.document-editor table td > p, div.cke_editable table td > p { margin-top: 0; margin-bottom: 0; padding: 4px 0 3px 5px;} ' +
            'blockquote { font-family: sans-serif, Arial, Verdana, "Trebuchet MS", "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"; } '
        );

        var editor = CKEDITOR.replace('editor', {
            height: 700,
            extraPlugins: 'colorbutton,font,justify,print,tableresize,liststyle,pagebreak',
            toolbar: [{
                    name: 'basicstyles',
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat',
                        'Subscript',
                        'Superscript'
                    ]
                },
                {
                    name: 'links',
                    items: ['Link', 'Unlink']
                },
                {
                    name: 'paragraph',
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent',
                        '-',
                        'Blockquote'
                    ]
                },
                {
                    name: 'insert',
                    items: ['Image', 'Table']
                },
                {
                    name: 'editing',
                    items: ['Scayt']
                },
                '/',
                {
                    name: 'styles',
                    items: ['Format', 'Font', 'FontSize']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor', 'CopyFormatting']
                },
                {
                    name: 'align',
                    items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight',
                        'JustifyBlock'
                    ]
                },
                {
                    name: 'document',
                    items: ['PageBreak', 'Source']
                }
            ],
            bodyClass: 'document-editor',
        });
    }

    function bulanDesc(value) {
        if (value == 1) {
            var bulan = 'Januari';
        } else if (value == 2) {
            var bulan = 'Februari';
        } else if (value == 3) {
            var bulan = 'Maret';
        } else if (value == 4) {
            var bulan = 'April';
        } else if (value == 5) {
            var bulan = 'Mei';
        } else if (value == 6) {
            var bulan = 'Juni';
        } else if (value == 7) {
            var bulan = 'Juli';
        } else if (value == 8) {
            var bulan = 'Agustus';
        } else if (value == 9) {
            var bulan = 'September';
        } else if (value == 10) {
            var bulan = 'Oktober';
        } else if (value == 11) {
            var bulan = 'November';
        } else if (value == 12) {
            var bulan = 'Desember';
        }
        return bulan;
    }
</script>


</html>
