<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<?php include 'resources/views/include/head.php'; ?>
<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <?php include 'resources/views/include/navbar.php'; ?>

    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        <?php echo env('APP_PROJECTNAME'); ?> </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            @lang('public.lmodal.modul') </a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            @lang('public.lmodal.judul') </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid modal-dialog-scrollable ">
            <div class="kt-portlet kt-portlet--mobile" id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg kt-portlet--fit">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            @lang('public.lmodal.judul')
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <button class="btn btn-default btn-elevate btn-icon-sm" id="btn_export_bulanan_csv"
                                    @if (!$data) disabled @endif>
                                    <i class="la la-download"></i>
                                    @lang('public.public.export') Bulanan
                                </button>
                                &nbsp;
                                <button class="btn btn-default btn-elevate btn-icon-sm" id="btn_export_tahunan_csv"
                                    @if (!$data) disabled @endif>
                                    <i class="la la-download"></i>
                                    @lang('public.public.export') Tahunan
                                </button>
                                &nbsp;
                                <input type="hidden" id="APP_URLAPP" value="{{ env('APP_URLAPP') }}">
                                <button class="btn btn-brand btn-elevate btn-icon-sm " id="btn_edit"
                                    @if (!$data or request()->get('report_year') < date('Y')) disabled @endif>
                                    <i class="la la-save"></i>
                                    Save @lang('public.lmodal.judul')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="kt-portlet__body">
                    <!-- begin:: Alert -->
                    @include('include.alert')
                    <!-- end:: Alert -->
                    <!-- PUBLIC -->
                    <input type="hidden" id="id_user" value="{{ $user->id_user }}">
                    <input type="hidden" id="APP_URLAPP" value="{{ env('APP_URLAPP') }}">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="locate" value="{{ $locate }}">

                    <!-- BLADE -->
                    <input type="hidden" id="judul_blade" value="@lang('public.lmodal.judul')">

                    <!-- begin:: Search -->
                    @include('include.search')
                    <!-- end:: Search -->
                    {{-- <hr> --}}



                    <!--begin: Datatable -->
                    <form class="kt-form kt-form--label-right form_lmodal" method="post" action=""
                        enctype="multipart/form-data" autocomplete="off">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="locate" value="{{ $locate }}">
                        <input type="hidden" value="{{ request()->get('report_year') }}" name="tmp_year">
                        <input type="hidden" value="{{ request()->get('report_month') }}" name="tmp_month">

                        <table class="table table-striped- table-hover table-checkable responsive-neraca">
                            <thead>
                                <tr>
                                    <th width="5%">@lang('public.public.no')</th>
                                    <th width="10%">@lang('public.lmodal.flag_detail')</th>
                                    <th width="15%">@lang('public.lmodal.component_code')</th>
                                    <th width="25%">@lang('public.lmodal.component_desc')</th>
                                    <th width="15%">Posisi {{ $month_now }} Tahun Laporan</th>
                                    <th width="15%">Posisi {{ $month_now }} Tahun Laporan Sebelumnya</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @forelse ($data as $key => $list)
                                    @if ($list->parent_id == null)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $list->flag_detail }}</td>
                                            <td>{{ $list->component_code }}</td>
                                            <td><b>{{ $list->component_desc }}</b></td>
                                            <td>
                                                @if ($list->component_order > 9000)
                                                    <input type="text" style="text-align: right"
                                                        class="form-control <?php echo $list->class_name; ?> input_nominal"
                                                        id="trx_amount-<?php echo $list->t_id; ?>"
                                                        name="trx_amount[<?php echo $list->t_id; ?>]"
                                                        value="{{ number_format($list->trx_amount) }}" readonly>
                                                @endif
                                            </td>
                                            <td style="padding-top: 20px; text-align:right;">
                                                @if ($list->trx_amount_before)
                                                    {{ number_format($list->trx_amount_before) }}
                                                @endif
                                            </td>
                                        </tr>
                                    @endif

                                    @foreach ($data as $key => $child)
                                        @if ($child->parent_id == $list->component_id)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $child->flag_detail }}</td>
                                                <td>{{ $child->component_code }}</td>
                                                <td>
                                                    @if ($child->component_order > 9000)
                                                        <b>{{ $child->component_desc }}</b>
                                                    @else
                                                        &nbsp;&nbsp;&nbsp;&nbsp;{{ $child->component_desc }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <input type="text" style="text-align: right"
                                                        class="form-control <?php echo $child->class_name; ?> input_nominal"
                                                        id="trx_amount-<?php echo $child->t_id; ?>"
                                                        name="trx_amount[<?php echo $child->t_id; ?>]"
                                                        value="{{ number_format($child->trx_amount) }}" @if (substr($child->class_name, 0, 5) == 'total')
                                                    readonly
                                        @endif
                                        >
                                        <div class="invalid-feedback alert-<?php echo $child->t_id; ?>">
                                        </div>
                                        </td>
                                        <td style="padding-top: 20px; text-align:right;">
                                            @if ($child->trx_amount_before)
                                                {{ number_format($child->trx_amount_before) }}
                                            @endif
                                        </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @empty
                                <tr>
                                    <td colspan="6" style="text-align: center"> Belum Ada Data</td>
                                </tr>

                                @endforelse

                            </tbody>
                        </table>
                        <br>


                    </form>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>

        @include('include.modal')
        <!-- end:: Content -->
    </div>

    <!-- begin:: Footer -->
    <?php include 'resources/views/include/footer.php'; ?>
    <!-- end:: Footer -->
    </div>
    </div>
    </div>
    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <?php include 'resources/views/include/loadjs.php'; ?>
    <!--end::Page Scripts -->
</body>
<script src="<?php echo env('APP_URLAPP'); ?>/assets/javascript/kinerja/lap_modal.js">
</script>
<!-- end::Body -->

</html>
