<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<?php include 'resources/views/include/head.php'; ?>
<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <?php include 'resources/views/include/navbar.php'; ?>

    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Silaras </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="<?php echo env('APP_URLAPP'); ?>/dashboard/"
                            class="kt-subheader__breadcrumbs-link">
                            Laporan Kinerja Keuangan </a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Catatan Atas Laporan Keuangan </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <form action="<?php echo env('APP_URLAPP'); ?>/lap_cat/export_to_pdf"
            method="POST" enctype="multipart/form-data" target="_blank">
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Catatan Atas Laporan Keuangan
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <input type="hidden" id="APP_URLAPP" value="{{ env('APP_URLAPP') }}">
                                    <button type="submit" class="btn btn-success btn-elevate btn-icon-sm "
                                        id="btn_export">
                                        <i class="la la-print"></i>
                                        Export to Pdf
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- begin:: Alert -->
                    @include('include.alert')
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <!-- end:: Alert -->
                    <div class="kt-portlet__body">
                        <textarea id="editor" name="redaksi" data-sample-short><h3 style="text-align:center"><strong>PT XXX</strong></h3>

                            <p style="text-align:center"><strong>CATATAN ATAS LAPORAN KEUANGAN<br />
                            PERIODE YANG BERAKHIR PADA 31 December 2020<br />
                            (DALAM RIBUAN RUPIAH)</strong></p>

                            <p style="text-align:center">&nbsp;</p></textarea>
                        <script>
                            // These styles are used to provide the "page view" display style like in the demo and matching styles for export to PDF.
                            CKEDITOR.addCss(
                                'body.document-editor { margin: 0.5cm auto; border: 1px #D3D3D3 solid; border-radius: 5px; background: white; box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); }' +
                                'body.document-editor, div.cke_editable { width: 700px; padding: 1cm 2cm 2cm; } ' +
                                'body.document-editor table td > p, div.cke_editable table td > p { margin-top: 0; margin-bottom: 0; padding: 4px 0 3px 5px;} ' +
                                'blockquote { font-family: sans-serif, Arial, Verdana, "Trebuchet MS", "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"; } '
                            );

                            var editor = CKEDITOR.replace('editor', {
                                height: 700,
                                extraPlugins: 'colorbutton,font,justify,print,tableresize,liststyle,pagebreak',
                                toolbar: [{
                                        name: 'basicstyles',
                                        items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat',
                                            'Subscript',
                                            'Superscript'
                                        ]
                                    },
                                    {
                                        name: 'links',
                                        items: ['Link', 'Unlink']
                                    },
                                    {
                                        name: 'paragraph',
                                        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent',
                                            '-',
                                            'Blockquote'
                                        ]
                                    },
                                    {
                                        name: 'insert',
                                        items: ['Image', 'Table']
                                    },
                                    {
                                        name: 'editing',
                                        items: ['Scayt']
                                    },
                                    '/',
                                    {
                                        name: 'styles',
                                        items: ['Format', 'Font', 'FontSize']
                                    },
                                    {
                                        name: 'colors',
                                        items: ['TextColor', 'BGColor', 'CopyFormatting']
                                    },
                                    {
                                        name: 'align',
                                        items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight',
                                            'JustifyBlock'
                                        ]
                                    },
                                    {
                                        name: 'document',
                                        items: ['PageBreak', 'Source']
                                    }
                                ],
                                bodyClass: 'document-editor',
                                // extraPlugins: 'autogrow',
                                // autoGrow_minHeight: 200,
                                // autoGrow_maxHeight: 600,
                                // autoGrow_bottomSpace: 50,
                                // removePlugins: 'resize'
                            });

                        </script>
                    </div>
                </div>
            </div>
        </form>

        <!-- end:: Content -->
    </div>

    <!-- begin:: Footer -->
    <?php include 'resources/views/include/footer.php'; ?>
    <!-- end:: Footer -->
    </div>
    </div>
    </div>
    <!-- end:: Page -->
</body>
<!-- end::Body -->

<?php include 'resources/views/include/loadjs.php'; ?>

<script>
    APP_URLAPP = $('#APP_URLAPP').val();
    var redaksi = $('#editor').val();
    var token = $('#_token').val();

    // $('#btn_export').click(function() {
    // console.log(APP_URLAPP + '/surat_pernyataan_laporan/export_to_pdf?redaksi=' + redaksi + '&_token=' +
    //     token);
    // e.preventDefault();
    // console.log($('#form_export').serialize());
    // $(this).html('Sending..');
    // $.ajax({
    //     data: $('#form_export').serialize(),
    //     url: APP_URLAPP + '/surat_pernyataan_laporan/export_to_pdf',
    //     type: "POST",
    //     dataType: 'json',
    //     xhrFields: {
    //         responseType: 'blob'
    //     },
    //     success: function(response) {
    //         $('#btn_export').html('Export to Pdf');
    //         var blob = new Blob([response]);
    //         var link = document.createElement('a');
    //         link.href = window.URL.createObjectURL(blob);
    //         link.download = "Sample.pdf";
    //         link.click();
    //     },
    //     error: function(data) {
    //         $('#btn_export').html('Export to Pdf');
    //     }
    // });
    //     window.open(APP_URLAPP + '/surat_pernyataan_laporan/export_to_pdf?redaksi=123&_token=' +
    //         token, '_blank');

    // });

</script>


</html>
