<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::any('/', 'WelcomeController@index');
// Route::post('welcome/proses', 'WelcomeController@welcome_proses');
Route::post('{locale}/welcome/proses', 'WelcomeController@welcome_proses');
Route::get('welcome/logout', 'WelcomeController@logout');


//=============================================== DASHBOARD ===============================================
Route::get('{locale}/dashboard', 'DashboardController@dashboard_index');


//=============================================== USER ===============================================
Route::get('{locale}/user', 'UserController@user_index');
Route::get('user/list', 'UserController@user_list');
Route::get('user/getdata', 'UserController@user_getdata');
Route::post('user/action', 'UserController@user_action');
Route::post('user/delete', 'UserController@user_delete');
Route::post('user/reset_password', 'UserController@user_reset_password');

//=============================================== EXPORT ===============================================
Route::post('export/profil', 'ExportController@export_profil');
Route::post('export/rincian_bank', 'ExportController@export_rincian_bank');
Route::post('export/rincian_payment', 'ExportController@export_rincian_payment');
Route::post('export/rincian_ewallet', 'ExportController@export_rincian_ewallet');
Route::post('export/lap_neraca', 'ExportController@export_lap_neraca');
Route::post('export/lap_laba', 'ExportController@export_lap_laba');
Route::post('export/lap_modal', 'ExportController@export_lap_modal');
Route::post('export/lap_arus', 'ExportController@export_lap_arus');
Route::post('export/inclusivity', 'ExportController@export_inclusivity');
Route::post('export/transaction_value', 'ExportController@export_transaction_value');
Route::post('export/loan_quality', 'ExportController@export_loan_quality');
Route::post('export/detail_outstanding_penyelenggara', 'ExportController@export_detail_outstanding_penyelenggara');
Route::post('export/detail_kualitas_pinjaman', 'ExportController@export_detail_kualitas_pinjaman');
Route::post('export/pengaduan_pengguna', 'ExportController@export_pengaduan_pengguna');
Route::post('export/all_report', 'ExportController@export_all_report');


//=============================================== LAPORAN PROFIL PERUSAHAAN : PROFIL PERUSAHAAN ===============================================
Route::get('{locale}/profil', 'ProfilController@profil_index');
Route::get('profil/list', 'ProfilController@profil_list');
Route::get('profil/getvalidator', 'ProfilController@profil_getvalidator');
Route::get('profil/getkab', 'ProfilController@profil_getkab');
Route::post('profil/edit', 'ProfilController@profil_edit');

//=============================================== LAPORAN PROFIL PERUSAHAAN : RINCIAN BANK ESCROW ===============================================
Route::get('{locale}/rincian_bank', 'RincianBankController@rincian_bank_index');
Route::get('rincian_bank/list', 'RincianBankController@rincian_bank_list');
Route::get('rincian_bank/getdata', 'RincianBankController@rincian_bank_getdata');
Route::post('rincian_bank/edit', 'RincianBankController@rincian_bank_edit');
Route::post('rincian_bank/action', 'RincianBankController@rincian_bank_action');
Route::post('rincian_bank/delete', 'RincianBankController@rincian_bank_delete');

//=============================================== LAPORAN PROFIL PERUSAHAAN : RINCIAN PAYMENT GATEAWAY ===============================================
Route::get('{locale}/rincian_payment', 'RincianPaymentController@rincian_payment_index');
Route::get('rincian_payment/list', 'RincianPaymentController@rincian_payment_list');
Route::get('rincian_payment/getdata', 'RincianPaymentController@rincian_payment_getdata');
Route::post('rincian_payment/edit', 'RincianPaymentController@rincian_payment_edit');
Route::post('rincian_payment/action', 'RincianPaymentController@rincian_payment_action');
Route::post('rincian_payment/delete', 'RincianPaymentController@rincian_payment_delete');

//=============================================== LAPORAN PROFIL PERUSAHAAN : RINCIAN E-WALLET ===============================================
Route::get('{locale}/rincian_ewallet', 'RincianEWalletController@rincian_ewallet_index');
Route::get('rincian_ewallet/list', 'RincianEWalletController@rincian_ewallet_list');
Route::get('rincian_ewallet/getdata', 'RincianEWalletController@rincian_ewallet_getdata');
Route::post('rincian_ewallet/edit', 'RincianEWalletController@rincian_ewallet_edit');
Route::post('rincian_ewallet/action', 'RincianEWalletController@rincian_ewallet_action');
Route::post('rincian_ewallet/delete', 'RincianEWalletController@rincian_ewallet_delete');

//=============================================== LAPORAN KINERJA PENYELENGGARA BULANAN : LAPORAN NERACA ===============================================
Route::get('{locale}/lap_neraca', 'LapNeracaController@lap_neraca_index');
Route::post('{locale}/lap_neraca', 'LapNeracaController@lap_neraca_index');
Route::get('lap_neraca/list', 'LapNeracaController@lap_neraca_list');
Route::get('lap_neraca/getvalidator', 'LapNeracaController@lap_neraca_getvalidator');
Route::get('lap_neraca/getkab', 'LapNeracaController@lap_neraca_getkab');
Route::post('lap_neraca/edit', 'LapNeracaController@lap_neraca_edit');

//=============================================== LAPORAN KINERJA PENYELENGGARA BULANAN : LAPORAN LABA RUGI ===============================================
Route::get('{locale}/lap_laba', 'LapLabaController@lap_laba_index');
Route::post('{locale}/lap_laba', 'LapLabaController@lap_laba_index');
Route::get('lap_laba/list', 'LapLabaController@lap_laba_list');
Route::get('lap_laba/getdata', 'LapLabaController@lap_laba_getdata');
Route::post('lap_laba/edit', 'LapLabaController@lap_laba_edit');
Route::post('lap_laba/action', 'LapLabaController@lap_laba_action');
Route::post('lap_laba/delete', 'LapLabaController@lap_laba_delete');

//=============================================== LAPORAN KINERJA PENYELENGGARA BULANAN : LAPORAN PERUBAHAN MODAL ===============================================
Route::get('{locale}/lap_modal', 'LapModalController@lap_modal_index');
Route::post('{locale}/lap_modal', 'LapModalController@lap_modal_index');
Route::get('lap_modal/list', 'LapModalController@lap_modal_list');
Route::get('lap_modal/getdata', 'LapModalController@lap_modal_getdata');
Route::post('lap_modal/edit', 'LapModalController@lap_modal_edit');
Route::post('lap_modal/action', 'LapModalController@lap_modal_action');
Route::post('lap_modal/delete', 'LapModalController@lap_modal_delete');

//=============================================== LAPORAN KINERJA PENYELENGGARA BULANAN : LAPORAN ARUS KAS ===============================================
Route::get('{locale}/lap_arus', 'LapArusController@lap_arus_index');
Route::post('{locale}/lap_arus', 'LapArusController@lap_arus_index');
Route::get('lap_arus/list', 'LapArusController@lap_arus_list');
Route::get('lap_arus/getdata', 'LapArusController@lap_arus_getdata');
Route::post('lap_arus/edit', 'LapArusController@lap_arus_edit');
Route::post('lap_arus/action', 'LapArusController@lap_arus_action');
Route::post('lap_arus/delete', 'LapArusController@lap_arus_delete');

//=============================================== LAPORAN KINERJA PENYELENGGARA BULANAN : CATATAN ATAS LAPORAN KEUANGAN ===============================================
// Route::get('{locale}/lap_cat', 'LaporanCatatanController@lap_cat_index');
// Route::post('lap_cat/export_to_pdf', 'LaporanCatatanController@export_to_pdf');

//=============================================== SURAT PERNYATAAN LAPORAN ===============================================
Route::get('{locale}/surat_pernyataan_laporan', 'SuratPernyataanController@surat_pernyataan_index');
Route::get('surat_pernyataan_laporan/export_to_pdf', 'SuratPernyataanController@export_to_pdf');
Route::get('surat_pernyataan_laporan/get_data', 'SuratPernyataanController@get_surat_pernyataan');
Route::post('surat_pernyataan_laporan/send_data', 'SuratPernyataanController@send_surat_pernyataan');

//=============================================== CATATAN ATAS LAPORAN KEUANGAN ===============================================
Route::get('{locale}/lap_cat', 'CatatanLapController@catatan_laporan_index');
Route::get('lap_cat/export_to_pdf', 'CatatanLapController@export_to_pdf');
Route::get('lap_cat/get_data', 'CatatanLapController@get_catatan_laporan');
Route::post('lap_cat/send_data', 'CatatanLapController@send_catatan_laporan');

//=============================================== LAPORAN KINERJA PENYELENGGARA BULANAN : LAPORAN INCLUSIVITY ===============================================
Route::get('{locale}/inclusivity', 'LapInclusivityController@inclusivity_index');
Route::post('{locale}/inclusivity', 'LapInclusivityController@inclusivity_index');
Route::get('inclusivity/list', 'LapInclusivityController@inclusivity_list');
Route::get('inclusivity/getvalidator', 'LapInclusivityController@inclusivity_getvalidator');
Route::get('inclusivity/getkab', 'LapInclusivityController@inclusivity_getkab');
Route::post('inclusivity/edit', 'LapInclusivityController@inclusivity_edit');

Route::get('{locale}/transaction_value', 'LapTransactionValueController@transaction_value_index');
Route::post('{locale}/transaction_value', 'LapTransactionValueController@transaction_value_index');
Route::get('transaction_value/list', 'LapTransactionValueController@transaction_value_list');
Route::get('transaction_value/getvalidator', 'LapTransactionValueController@transaction_value_getvalidator');
Route::get('transaction_value/getkab', 'LapTransactionValueController@transaction_value_getkab');
Route::post('transaction_value/edit', 'LapTransactionValueController@transaction_value_edit');

Route::get('{locale}/loan_quality', 'LapLoanQualityController@loan_quality_index');
Route::post('{locale}/loan_quality', 'LapLoanQualityController@loan_quality_index');
Route::get('loan_quality/list', 'LapLoanQualityController@loan_quality_list');
Route::get('loan_quality/getvalidator', 'LapLoanQualityController@loan_quality_getvalidator');
Route::get('loan_quality/getkab', 'LapLoanQualityController@loan_quality_getkab');
Route::post('loan_quality/edit', 'LapLoanQualityController@loan_quality_edit');

Route::get('{locale}/detail_outstanding_penyelenggara', 'LapDetailOutstandingPenyelenggaraController@detail_outstanding_penyelenggara_index');
Route::post('{locale}/detail_outstanding_penyelenggara', 'LapDetailOutstandingPenyelenggaraController@detail_outstanding_penyelenggara_index');
Route::get('detail_outstanding_penyelenggara/list', 'LapDetailOutstandingPenyelenggaraController@detail_outstanding_penyelenggara_list');
Route::get('detail_outstanding_penyelenggara/getvalidator', 'LapDetailOutstandingPenyelenggaraController@detail_outstanding_penyelenggara_getvalidator');
Route::get('detail_outstanding_penyelenggara/getkab', 'LapDetailOutstandingPenyelenggaraController@detail_outstanding_penyelenggara_getkab');
Route::post('detail_outstanding_penyelenggara/edit', 'LapDetailOutstandingPenyelenggaraController@detail_outstanding_penyelenggara_edit');

Route::get('{locale}/detail_kualitas_pinjaman', 'LapDetailKualitasPinjamanController@detail_kualitas_pinjaman_index');
Route::post('{locale}/detail_kualitas_pinjaman', 'LapDetailKualitasPinjamanController@detail_kualitas_pinjaman_index');
Route::get('detail_kualitas_pinjaman/list', 'LapDetailKualitasPinjamanController@detail_kualitas_pinjaman_list');
Route::get('detail_kualitas_pinjaman/getvalidator', 'LapDetailKualitasPinjamanController@detail_kualitas_pinjaman_getvalidator');
Route::get('detail_kualitas_pinjaman/getkab', 'LapDetailKualitasPinjamanController@detail_kualitas_pinjaman_getkab');
Route::post('detail_kualitas_pinjaman/edit', 'LapDetailKualitasPinjamanController@detail_kualitas_pinjaman_edit');

Route::get('{locale}/pengaduan_pengguna', 'LapPengaduanPenggunaController@pengaduan_pengguna_index');
Route::post('{locale}/pengaduan_pengguna', 'LapPengaduanPenggunaController@pengaduan_pengguna_index');
Route::get('pengaduan_pengguna/list', 'LapPengaduanPenggunaController@pengaduan_pengguna_list');
Route::get('pengaduan_pengguna/getvalidator', 'LapPengaduanPenggunaController@pengaduan_pengguna_getvalidator');
Route::get('pengaduan_pengguna/getkab', 'LapPengaduanPenggunaController@pengaduan_pengguna_getkab');
Route::post('pengaduan_pengguna/edit', 'LapPengaduanPenggunaController@pengaduan_pengguna_edit');

Route::get('{locale}/all_report', 'ReportAllController@all_report_index');
