
$(document).ready(function(){
    init();
    loadData();
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init(){
    var table = '.dt_user';
    var action = 'user';
    APP_URLAPP = $('#APP_URLAPP').val();
    locate = $('#locate').val();
    url = APP_URLAPP+'/'+action+'/'

    //TAMBAH
    $('#btn-add').click(function() {
        modal = '.modal_add';
        $(modal+" #action_flag").val('A');
        $('.modal_add').modal('show');
    }); 

    var form = '.form_add'
    var pesan_error = 'There are some errors in your submission. Please correct them.'

    $('#btn-yes-add').click(function(){
        $(form).submit();
    });

    $( form ).validate({
        rules: {
            first_name : {required: true},
            last_name : {required: false},
            username : {required: true},
            // password : {required: true},
            password: { required: function(element) { return $(modal+' #action_flag').val() == 'A' }, },
            email : {required: false},
            nik : {required: false},
            phone_number : {required: false},
            id_divisi : {required: true},
            id_role : {required: true},
        },
        messages: {
            id_pelaporan: {
                required: "There is something wrong with id pelaporan, please call administrator"
            },
        },
        
        //display error alert on form submit
        invalidHandler: function(event, validator) {
            swal.fire({
                "title": "",
                "text": pesan_error,
                "type": "error",
                "confirmButtonClass": "btn btn-secondary",
                "onClose": function(e) {
                    console.log('on close event fired!');
                }
            });

            event.preventDefault();
        },

        submitHandler: function (event) {
            var token = $('#_token').val();
            $.ajax({
                url : url+'action'+'?_token=' + token,
                type: "POST",
                dataType: "json",
                data : {
                    "id": $(form+" #id").val(),
                    "action_flag" : $(form+" #action_flag").val(),
                    "first_name" : $(form+" #first_name").val(),
                    "last_name" : $(form+" #last_name").val(),
                    "username" : $(form+" #username").val(),
                    "password" : $(form+" #password").val(),
                    "email" : $(form+" #email").val(),
                    "nik" : $(form+" #nik").val(),
                    "phone_number" : $(form+" #phone_number").val(),
                    "id_divisi" : $(form+" #id_divisi").val(),
                    "id_role" : $(form+" #id_role").val(),
                },
                success: function(response, textStatus, jqXHR)
                {
                    if(response.status == 1){
                        var pesan_add = 'Data User Berhasil Disimpan'
                        var type = 'success'
                    }else{
                        var pesan_add = 'Data User Gagal Disimpan'
                        var type = 'error';
                    }

                    swal.fire({
                        title: 'Done!',
                        text: pesan_add,
                        type: type,
                        "confirmButtonClass": "btn-finish"
                    })

                    $('.btn-finish').click(function() {
                        location.href = APP_URLAPP+'/'+locate+'/'+action
                    });

                }
            });
        }
    });

    //VIEW
    $(document).delegate( '.btn-view', "click",function (event) {
        id = $(this).attr('data-id');
        modal = '.modal_view_user';
        $.ajax({
            url : url+'getdata',
            type: "GET",
            dataType: "json",
            data : "id="+id,
            success: function(response, textStatus, jqXHR){
                var detail = ''
                var data = response

                $(modal+' #first_name').html(data.first_name)
                $(modal+' #last_name').html(data.last_name)
                $(modal+' #username').html(data.username)
                $(modal+' #email').html(data.email)
                $(modal+' #nik').html(data.nik)
                $(modal+' #phone_number').html(data.phone_number)
                $(modal+' #divisi').html(data.nama_divisi)
                $(modal+' #role').html(data.role_name)
                $(modal+' #created').html(data.created_name+' | '+data.created_on)
                if(data.updated_by){
                    $('#tr_updated').show();
                    $(modal+' #updated').html(data.updated_name+' | '+data.updated_on)
                }
                $(modal).modal('show');
            }
        });
    });

    //EDIT
    $(document).delegate( '.btn-edit', "click",function (event) {
        id = $(this).attr('data-id');
        $.ajax({
            url : url+'getdata',
            type: "GET",
            dataType: "json",
            data : "id="+id,
            success: function(data, textStatus, jqXHR)
            {
                modal = '.modal_add';
                $(modal+' .modal-title').html('Edit');
                $(modal+' .form_add').attr('action',''+action+'/edit');
                $(modal+' #id').val(id);
                $(modal+" #action_flag").val('E');
                $(modal+' #first_name').val(data.first_name);
                $(modal+' #last_name').val(data.last_name);
                $(modal+' #username').val(data.username);
                $(modal+' #password').val(data.password);
                $(modal+' #email').val(data.email);
                $(modal+' #nik').val(data.nik);
                $(modal+' #phone_number').val(data.phone_number);
                $(modal+' #id_divisi').val(data.id_divisi).trigger('change');
                $(modal+' #id_role').val(data.id_role).trigger('change');
                $('#div_password').hide();
                $(modal).modal('show');
            }
        });
    });

    //HAPUS
    $(document).delegate( '.btn-hapus', "click",function (event) {
        id = $(this).attr('data-id');
        nama = $(this).attr('data-nama');
        modal = '.modal_alert';

        $(modal+' .form_alert').attr('action',''+url+'delete');
        $(modal+' #id').val(id);
        $(modal+' #nama').val(nama);

        pesan_judul_hapus = $('#pesan_judul_hapus').val();
        pesan_ket_hapus = $('#pesan_ket_hapus').val();
        pesan_ya = $('#pesan_ya').val();
        pesan_tidak = $('#pesan_tidak').val();
        judul_blade = $('#judul_blade').val();

        swal.fire({
            title: pesan_judul_hapus,
            text: pesan_ket_hapus+judul_blade+' ?',
            type: "warning",

            buttonsStyling: false,

            confirmButtonText: "<i class='la la-check-square'></i>"+pesan_ya+"!",
            confirmButtonClass: "btn btn-danger btn-yes-hapus",

            showCancelButton: true,
            cancelButtonText: "<i class='la la-close'></i> "+pesan_tidak+"",
            cancelButtonClass: "btn btn-default btn-no-hapus"
        });

        $('.btn-yes-hapus').click(function(){
            $('.form_alert').submit();
        }); 
    });

    //RESET PASSWORD
    $(document).delegate( '.btn-hapus', "click",function (event) {
        id = $(this).attr('data-id');
        nama = $(this).attr('data-nama');
        modal = '.modal_alert';

        $(modal+' .form_alert').attr('action',''+url+'reset_password');
        $(modal+' #id').val(id);
        $(modal+' #nama').val(nama);

        pesan_judul_hapus = $('#pesan_judul_hapus').val();
        pesan_ket_hapus = $('#pesan_ket_hapus').val();
        pesan_ya = $('#pesan_ya').val();
        pesan_tidak = $('#pesan_tidak').val();
        judul_blade = $('#judul_blade').val();

        swal.fire({
            title: pesan_judul_hapus,
            text: pesan_ket_hapus+judul_blade+' ?',
            type: "warning",

            buttonsStyling: false,

            confirmButtonText: "<i class='la la-check-square'></i>"+pesan_ya+"!",
            confirmButtonClass: "btn btn-danger btn-yes-hapus",

            showCancelButton: true,
            cancelButtonText: "<i class='la la-close'></i> "+pesan_tidak+"",
            cancelButtonClass: "btn btn-default btn-no-hapus"
        });

        $('.btn-yes-hapus').click(function(){
            $('.form_alert').submit();
        }); 
    });


}


/*-------------------------------------------
 * RELOAD PAGE
 *-----------------------------------------*/
 function reloadPage(){
    location.reload(true);
 }

/*-------------------------------------------
 * LOAD DATA LIST
 *-----------------------------------------*/
 function loadData(){
    APP_URLAPP = $('#APP_URLAPP').val();

    table = $('.dt_user').DataTable({
        'ajax': {
            "type"   : "GET",
            "url"    : APP_URLAPP+'/user/list',
        },
        "order": [[ 1, "asc" ]],
        processing: true,
        serverSide: true,
        columns: [
            {data: "id_user"},
            {data: "username"},
            {data: function(data){
                status = data.first_name+' '+data.last_name
                return status;
            }},
            {data: "email"},
            {data: "nik"},
            {data: "nama_divisi"},
            {data: "role_name"},
            {data: "created_name"},
            {data: "created_on"},
            {data: function(data){
                id = data.id_user;
                view = '<btn class="dropdown-item btn-view" data-id='+id+'><i class="la la-list"></i> View</btn>'
                edit = '<btn class="dropdown-item btn-edit" data-id='+id+'><i class="la la-edit"></i> Edit</btn>'
                hapus ='<btn class="dropdown-item btn-hapus" data-id='+id+' data-nama='+data.username+'><i class="la la-trash"></i> Delete</btn>'
                reset = '<btn class="dropdown-item btn-reset"data-id='+id+' data-nama='+data.username+'><i class="la la-key"></i> Reset Password</btn>'
                
                btn1 = ('<div class="dropdown">\
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                            </button>\
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">')

                btn2 = view + edit + hapus + reset
                
                btn3 = ('</div></div>')

                btn =  btn1+btn2+btn3;
                return btn;
            },
            orderable: false},
        ]
    });


    // num row
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}

