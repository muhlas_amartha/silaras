
$(document).ready(function () {
    init();
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {

    var form_lmodal = '.form_lmodal';
    var action = 'lap_modal';
    APP_URLAPP = $('#APP_URLAPP').val();
    locate = $('#locate').val();
    url = APP_URLAPP + '/' + action + '/';

    $('.input_nominal').on('keyup', function () {
        var $this = $(this);
        var value = $(this).val();
        w = value.replace(/(?!^-)[^0-9.]/g, '');
        // console.log("w = " + w);
        x = w.replace(',', '');
        y = x.replace(/,/g, '');
        // console.log(y);
        $this.val(numberFormat(y));
    });

    $('#btn_edit').click(function () {
        $(form_lmodal).attr('action', '' + url + 'edit');
        $(form_lmodal).submit();
    });

    $('#btn_export_bulanan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/lap_modal?tipe=bulanan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_export_tahunan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/lap_modal?tipe=tahunan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_search').click(function () {
        form = '.kt-form';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        $(form).attr('action', APP_URLAPP + '/id/lap_modal?report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('.saldo_awal').on('change', function () {
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_saldo_akhir').val(numberFormat(totalSaldoAkhir()));
    });

    $('.transaksi_lain').on('change', function () {
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_saldo_akhir').val(numberFormat(totalSaldoAkhir()));
    });

    $('.deviden').on('change', function () {
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_saldo_akhir').val(numberFormat(totalSaldoAkhir()));
    });

}

function numberFormat(x) {
    var result = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return result;
}

function addCurrency(x) {
    var value = x.replace('.00', '');
    value += '';
    x = value.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function totalSaldoAkhir() {
    var saldo_awal = $('.saldo_awal').val().replace(/,/g, '');
    var total_pendapatan_net = $('.total_pendapatan_net').val().replace(/,/g, '');
    var transaksi_lain = $('.transaksi_lain').val().replace(/,/g, '');
    var deviden = $('.deviden').val().replace(/,/g, '');
    var total_saldo_akhir = parseInt(saldo_awal) + parseInt(total_pendapatan_net) + parseInt(transaksi_lain) - parseInt(deviden);
    return total_saldo_akhir;
}





