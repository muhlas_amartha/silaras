
$(document).ready(function () {
    init();
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {

    var action = 'detail_outstanding_penyelenggara';
    var form_ldetail_outstanding_penyelenggara = '.form_ldetail_outstanding_penyelenggara';
    APP_URLAPP = $('#APP_URLAPP').val();
    locate = $('#locate').val();
    url = APP_URLAPP + '/' + action + '/';

    $('.input_nominal').on('keyup', function () {
        var $this = $(this);
        var value = $(this).val();
        w = value.replace(/(?!^-)[^0-9.]/g, '');
        // console.log("w = " + w);
        x = w.replace(',', '');
        y = x.replace(/,/g, '');
        // console.log(y);
        $this.val(numberFormat(y));
    });

    $('#btn_edit').click(function () {
        console.log('btn_edit click')
        $(form_ldetail_outstanding_penyelenggara).attr('action', '' + url + 'edit');
        $(form_ldetail_outstanding_penyelenggara).submit();
    });

    $('#btn_export_bulanan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/detail_outstanding_penyelenggara?tipe=bulanan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_export_tahunan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/detail_outstanding_penyelenggara?tipe=tahunan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_search').click(function () {
        form = '.kt-form';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        $(form).attr('action', APP_URLAPP + '/id/detail_outstanding_penyelenggara?report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    // Lender Dalam Negeri
    $('.perorangan_dalam_negeri').on('change', function () {
        $('.total_lender_dalam_negeri').val(numberFormat(setTotalLenderDalamNegeri()));
        $('.total_lender').val(numberFormat(setTotalLender()));
    });

    $('.lender_perbankan_dalam_negeri').on('change', function () {
        var sum = 0;

        var inputs = $(".lender_perbankan_dalam_negeri");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_lender_perbankan_dalam_negeri').val(numberFormat(result));
        $('.total_lender_dalam_negeri').val(numberFormat(setTotalLenderDalamNegeri()));
        $('.total_lender').val(numberFormat(setTotalLender()));
    });

    $('.lender_industri_dalam_negeri').on('change', function () {
        var sum = 0;

        var inputs = $(".lender_industri_dalam_negeri");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_lender_industri_dalam_negeri').val(numberFormat(result));
        $('.total_lender_dalam_negeri').val(numberFormat(setTotalLenderDalamNegeri()));
        $('.total_lender').val(numberFormat(setTotalLender()));
    });

    $('.koperasi_dalam_negeri').on('change', function () {
        $('.total_lender_dalam_negeri').val(numberFormat(setTotalLenderDalamNegeri()));
        $('.total_lender').val(numberFormat(setTotalLender()));
    });

    $('.institusi_dalam_negeri').on('change', function () {
        $('.total_lender_dalam_negeri').val(numberFormat(setTotalLenderDalamNegeri()));
        $('.total_lender').val(numberFormat(setTotalLender()));
    });

    // Lender Luar Negeri
    $('.perorangan_luar_negeri').on('change', function () {
        $('.total_lender_luar_negeri').val(numberFormat(setTotalLenderLuarNegeri()));
        $('.total_lender').val(numberFormat(setTotalLender()));
    });

    $('.perbankan_luar_negeri').on('change', function () {
        $('.total_lender_luar_negeri').val(numberFormat(setTotalLenderLuarNegeri()));
        $('.total_lender').val(numberFormat(setTotalLender()));
    });

    $('.institusi_luar_negeri').on('change', function () {
        $('.total_lender_luar_negeri').val(numberFormat(setTotalLenderLuarNegeri()));
        $('.total_lender').val(numberFormat(setTotalLender()));
    });

    $('.lender_industri_luar_negeri').on('change', function () {
        var sum = 0;

        var inputs = $(".lender_industri_luar_negeri");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_lender_industri_luar_negeri').val(numberFormat(result));
        $('.total_lender_luar_negeri').val(numberFormat(setTotalLenderLuarNegeri()));
        $('.total_lender').val(numberFormat(setTotalLender()));
    });

    // Borrower
    $('.borrower').on('change', function () {
        var sum = 0;

        var inputs = $(".borrower");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_borrower').val(numberFormat(result));
    });



}

function numberFormat(x) {
    var result = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return result;
}

function addCurrency(x) {
    var value = x.replace('', '');
    value += '';
    x = value.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function setTotalLenderLuarNegeri() {
    var perorangan_luar_negeri = $('.perorangan_luar_negeri').val().replace(/,/g, '');
    var perbankan_luar_negeri = $('.perbankan_luar_negeri').val().replace(/,/g, '');
    var total_lender_industri_luar_negeri = $('.total_lender_industri_luar_negeri').val().replace(/,/g, '');
    var institusi_luar_negeri = $('.institusi_luar_negeri').val().replace(/,/g, '');
    var total_lender_luar_negeri = parseInt(perorangan_luar_negeri) + parseInt(perbankan_luar_negeri) + parseInt(total_lender_industri_luar_negeri) + parseInt(institusi_luar_negeri);
    var result = total_lender_luar_negeri;
    return result;
}

function setTotalLenderDalamNegeri() {
    var perorangan_dalam_negeri = $('.perorangan_dalam_negeri').val().replace(/,/g, '');
    var total_lender_perbankan_dalam_negeri = $('.total_lender_perbankan_dalam_negeri').val().replace(/,/g, '');
    var total_lender_industri_dalam_negeri = $('.total_lender_industri_dalam_negeri').val().replace(/,/g, '');
    var koperasi_dalam_negeri = $('.koperasi_dalam_negeri').val().replace(/,/g, '');
    var institusi_dalam_negeri = $('.institusi_dalam_negeri').val().replace(/,/g, '');
    var total_lender_dalam_negeri = parseInt(perorangan_dalam_negeri) + parseInt(total_lender_perbankan_dalam_negeri) + parseInt(total_lender_industri_dalam_negeri) + parseInt(koperasi_dalam_negeri) + parseInt(institusi_dalam_negeri);
    var result = total_lender_dalam_negeri;
    return result;
}

function setTotalLender() {
    var total_lender_dalam_negeri = $('.total_lender_dalam_negeri').val().replace(/,/g, '');
    var total_lender_luar_negeri = $('.total_lender_luar_negeri').val().replace(/,/g, '');
    var total_lender = parseInt(total_lender_dalam_negeri) + parseInt(total_lender_luar_negeri);
    var result = total_lender;
    return result;
}




