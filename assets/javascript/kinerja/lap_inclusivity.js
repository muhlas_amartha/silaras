
$(document).ready(function () {
    init();
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {

    var action = 'inclusivity';
    var form_linclusivity = '.form_linclusivity';
    APP_URLAPP = $('#APP_URLAPP').val();
    locate = $('#locate').val();
    url = APP_URLAPP + '/' + action + '/';

    $('.input_nominal').on('keyup', function () {
        var $this = $(this);
        var value = $(this).val();
        w = value.replace(/(?!^-)[^0-9.]/g, '');
        // console.log("w = " + w);
        x = w.replace(',', '');
        y = x.replace(/,/g, '');
        // console.log(y);
        $this.val(numberFormat(y));
    });

    $('#btn_edit').click(function () {
        console.log('btn_edit click')
        $(form_linclusivity).attr('action', '' + url + 'edit');
        $(form_linclusivity).submit();
    });

    $('#btn_export_bulanan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/inclusivity?tipe=bulanan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_export_tahunan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/inclusivity?tipe=tahunan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_search').click(function () {
        form = '.kt-form';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        $(form).attr('action', APP_URLAPP + '/id/inclusivity?report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('.unique_lender_jawa').on('change', function () {
        var sum = 0;

        var inputs = $(".unique_lender_jawa");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_unique_lender_jawa').val(numberFormat(result));
    });

    $('.unique_lender_luar_jawa').on('change', function () {
        var sum = 0;

        var inputs = $(".unique_lender_luar_jawa");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_unique_lender_luar_jawa').val(numberFormat(result));
    });

    $('.unique_lender_agregat').on('change', function () {
        var sum = 0;

        var inputs = $(".unique_lender_agregat");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_unique_lender_agregat').val(numberFormat(result));
    });

    $('.unique_lender_usia').on('change', function () {
        var sum = 0;

        var inputs = $(".unique_lender_usia");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        // $('.total_unique_lender_usia').val(numberFormat(result));
    });

    $('.unique_borrower_jawa').on('change', function () {
        var sum = 0;

        var inputs = $(".unique_borrower_jawa");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_unique_borrower_jawa').val(numberFormat(result));
    });

    $('.unique_borrower_luar_jawa').on('change', function () {
        var sum = 0;

        var inputs = $(".unique_borrower_luar_jawa");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_unique_borrower_luar_jawa').val(numberFormat(result));
    });

    $('.unique_borrower_agregat').on('change', function () {
        var sum = 0;

        var inputs = $(".unique_borrower_agregat");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_unique_borrower_agregat').val(numberFormat(result));
    });

    $('.unique_borrower_usia').on('change', function () {
        var sum = 0;

        var inputs = $(".unique_borrower_usia");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        // $('.total_unique_borrower_usia').val(numberFormat(result));
    });

    $('.lending_account_jawa').on('change', function () {
        var sum = 0;

        var inputs = $(".lending_account_jawa");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_lending_account_jawa').val(numberFormat(result));
    });

    $('.lending_account_luar_jawa').on('change', function () {
        var sum = 0;

        var inputs = $(".lending_account_luar_jawa");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_lending_account_luar_jawa').val(numberFormat(result));
    });

    $('.lending_account_agregat').on('change', function () {
        var sum = 0;

        var inputs = $(".lending_account_agregat");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_lending_account_agregat').val(numberFormat(result));
    });

    $('.lending_account_usia').on('change', function () {
        var sum = 0;

        var inputs = $(".lending_account_usia");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        // $('.total_lending_account_usia').val(numberFormat(result));
    });

    $('.credit_account_jawa').on('change', function () {
        var sum = 0;

        var inputs = $(".credit_account_jawa");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_credit_account_jawa').val(numberFormat(result));
    });

    $('.credit_account_luar_jawa').on('change', function () {
        var sum = 0;

        var inputs = $(".credit_account_luar_jawa");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_credit_account_luar_jawa').val(numberFormat(result));
    });

    $('.credit_account_agregat').on('change', function () {
        var sum = 0;

        var inputs = $(".credit_account_agregat");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_credit_account_agregat').val(numberFormat(result));
    });

    $('.credit_account_usia').on('change', function () {
        var sum = 0;

        var inputs = $(".credit_account_usia");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
            console.log(sum);
        }
        result = sum;
        var input = $(this).val();
        $(this).val(addCurrency(input));
        // $('.total_credit_account_usia').val(numberFormat(result));
    });

}

function numberFormat(x) {
    var result = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return result;
}

function addCurrency(x) {
    var value = x.replace('.00', '');
    value += '';
    x = value.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function totalKewajibanDanEkuitas() {
    var total_kewajiban = $('.total_kewajiban').val().replace(/,/g, '');
    var total_ekuitas = $('.total_ekuitas').val().replace(/,/g, '');
    var total_kewajiban_dan_ekuitas = parseInt(total_kewajiban) + parseInt(total_ekuitas);
    var result = total_kewajiban_dan_ekuitas;
    return result;
}




