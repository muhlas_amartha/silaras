
$(document).ready(function () {
    init();
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {

    var form_larus = '.form_larus';
    var action = 'lap_arus';
    APP_URLAPP = $('#APP_URLAPP').val();
    locate = $('#locate').val();
    url = APP_URLAPP + '/' + action + '/';

    $('.input_nominal').on('keyup', function () {
        var $this = $(this);
        var value = $(this).val();
        w = value.replace(/(?!^-)[^0-9.]/g, '');
        // console.log("w = " + w);
        x = w.replace(',', '');
        y = x.replace(/,/g, '');
        // console.log(y);
        $this.val(numberFormat(y));
    });

    $('#btn_edit').click(function () {
        $(form_larus).attr('action', '' + url + 'edit');
        $(form_larus).submit();
    });

    $('#btn_export_bulanan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/lap_arus?tipe=bulanan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_export_tahunan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/lap_arus?tipe=tahunan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_search').click(function () {
        form = '.kt-form';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        $(form).attr('action', APP_URLAPP + '/id/lap_arus?report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('.arus_kas').on('change', function () {
        var sum = 0;

        var inputs = $(".arus_kas");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
        }

        var result = sum;
        $('.total_saldo_kas').val(numberFormat(result));

        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_saldo_akhir').val(numberFormat(totalSaldoAkhir()));

    });

    $('.saldo_awal').on('change', function () {
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_saldo_akhir').val(numberFormat(totalSaldoAkhir()));

    });

}

function numberFormat(x) {
    var result = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return result;
}

function addCurrency(x) {
    var value = x.replace('.00', '');
    value += '';
    x = value.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function totalSaldoAkhir() {
    var total_saldo_kas = $('.total_saldo_kas').val().replace(/,/g, '');
    var saldo_awal = $('.saldo_awal').val().replace(/,/g, '');
    var total_saldo_akhir = parseInt(total_saldo_kas) + parseInt(saldo_awal);
    return total_saldo_akhir;
}





