
$(document).ready(function () {
    init();
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {

    var form_llaba = '.form_llaba';
    var action = 'lap_laba';
    APP_URLAPP = $('#APP_URLAPP').val();
    locate = $('#locate').val();
    url = APP_URLAPP + '/' + action + '/';

    $('.input_nominal').on('keyup', function () {
        var $this = $(this);
        var value = $(this).val();
        w = value.replace(/(?!^-)[^0-9.]/g, '');
        // console.log("w = " + w);
        x = w.replace(',', '');
        y = x.replace(/,/g, '');
        // console.log(y);
        $this.val(numberFormat(y));
    });

    $('#btn_edit').click(function () {
        $(form_llaba).attr('action', '' + url + 'edit');
        $(form_llaba).submit();
    });

    $('#btn_export_bulanan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/lap_laba?tipe=bulanan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_export_tahunan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/lap_laba?tipe=tahunan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_search').click(function () {
        form = '.kt-form';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        $(form).attr('action', APP_URLAPP + '/id/lap_laba?report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('.pendapatan').on('change', function () {
        var sum = 0;

        var inputs = $(".pendapatan");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
        }

        var result = sum;
        $('.total_pendapatan').val(numberFormat(result));

        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_pendapatan_bruto').val(numberFormat(totalPendapatanBruto()));
        $('.total_pendapatan_net').val(numberFormat(totalPendapatanNet()));
        $('.total_pendapatan_komprehensif').val(numberFormat(totalPendapatanKomprehensif()));

    });

    $('.beban').on('change', function () {
        var sum = 0;

        var inputs = $(".beban");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
        }

        var result = sum;
        $('.total_beban').val(numberFormat(result));

        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_pendapatan_bruto').val(numberFormat(totalPendapatanBruto()));
        $('.total_pendapatan_net').val(numberFormat(totalPendapatanNet()));
        $('.total_pendapatan_komprehensif').val(numberFormat(totalPendapatanKomprehensif()));

    });

    $('.pendapatan_lainnya').on('change', function () {
        var sum_pendapatan = 0;
        var inputs = $(".pendapatan_lainnya");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum_pendapatan += Number(amount);
        }
        var result_pendapatan = sum_pendapatan;

        var sum_beban = 0;
        var inputs = $(".beban_lainnya");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum_beban += Number(amount);
        }
        var result_beban = sum_beban;

        var result = result_pendapatan - result_beban;
        $('.total_pendapatan_beban_lainnya').val(numberFormat(result));

        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_pendapatan_bruto').val(numberFormat(totalPendapatanBruto()));
        $('.total_pendapatan_net').val(numberFormat(totalPendapatanNet()));
        $('.total_pendapatan_komprehensif').val(numberFormat(totalPendapatanKomprehensif()));

    });

    $('.beban_lainnya').on('change', function () {
        var sum_pendapatan = 0;
        var inputs = $(".pendapatan_lainnya");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum_pendapatan += Number(amount);
        }
        var result_pendapatan = sum_pendapatan;

        var sum_beban = 0;
        var inputs = $(".beban_lainnya");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum_beban += Number(amount);
        }
        var result_beban = sum_beban;

        var result = result_pendapatan - result_beban;
        $('.total_pendapatan_beban_lainnya').val(numberFormat(result));

        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_pendapatan_bruto').val(numberFormat(totalPendapatanBruto()));
        $('.total_pendapatan_net').val(numberFormat(totalPendapatanNet()));
        $('.total_pendapatan_komprehensif').val(numberFormat(totalPendapatanKomprehensif()));

    });

    $('.beban_bunga').on('change', function () {
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_pendapatan_net').val(numberFormat(totalPendapatanNet()));
        $('.total_pendapatan_komprehensif').val(numberFormat(totalPendapatanKomprehensif()));
    });

    $('.beban_pajak').on('change', function () {
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_pendapatan_net').val(numberFormat(totalPendapatanNet()));
        $('.total_pendapatan_komprehensif').val(numberFormat(totalPendapatanKomprehensif()));
    });

    $('.pendapatan_beban_komprehensif_lainnya').on('change', function () {
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_pendapatan_komprehensif').val(numberFormat(totalPendapatanKomprehensif()));
    });

}

function numberFormat(x) {
    var result = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return result;
}

function addCurrency(x) {
    var value = x.replace('.00', '');
    value += '';
    x = value.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function totalPendapatanBruto() {
    var total_pendapatan = $('.total_pendapatan').val().replace(/,/g, '');
    var total_beban = $('.total_beban').val().replace(/,/g, '');
    var total_pendapatan_beban_lainnya = $('.total_pendapatan_beban_lainnya').val().replace(/,/g, '');
    var total_pendapatan_bruto = (parseInt(total_pendapatan) - parseInt(total_beban)) + parseInt(total_pendapatan_beban_lainnya);
    // console.log("Total Pendapatan Bruto = " + total_pendapatan_bruto);
    return total_pendapatan_bruto;
}

function totalPendapatanNet() {
    var total_pendapatan_bruto = $('.total_pendapatan_bruto').val().replace(/,/g, '');
    var beban_bunga = $('.beban_bunga').val().replace(/,/g, '');
    // console.log("Beban Bunga = " + beban_bunga);
    var beban_pajak = $('.beban_pajak').val().replace(/,/g, '');
    var total_pendapatan_net = parseInt(total_pendapatan_bruto) - parseInt(beban_bunga) - parseInt(beban_pajak);
    // console.log("Total Pendapatan Net = " + total_pendapatan_net);
    return total_pendapatan_net;
}

function totalPendapatanKomprehensif() {
    var total_pendapatan_net = $('.total_pendapatan_net').val().replace(/,/g, '');
    var pendapatan_beban_komprehensif_lainnya = $('.pendapatan_beban_komprehensif_lainnya').val().replace(/,/g, '');
    var total_pendapatan_komprehensif = parseInt(total_pendapatan_net) + parseInt(pendapatan_beban_komprehensif_lainnya);
    // console.log("Total Pendapatan Komprehensif = " + total_pendapatan_komprehensif);
    return total_pendapatan_komprehensif;
}




