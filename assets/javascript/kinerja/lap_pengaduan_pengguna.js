
$(document).ready(function () {
    init();
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {

    var action = 'pengaduan_pengguna';
    var form_lpengaduan_pengguna = '.form_lpengaduan_pengguna';
    APP_URLAPP = $('#APP_URLAPP').val();
    locate = $('#locate').val();
    url = APP_URLAPP + '/' + action + '/'

    $('#btn_edit').click(function () {
        console.log('btn_edit click')
        $(form_lpengaduan_pengguna).attr('action', '' + url + 'edit');
        $(form_lpengaduan_pengguna).submit();
    });

    $('#btn_export_bulanan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/pengaduan_pengguna?tipe=bulanan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_export_tahunan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/pengaduan_pengguna?tipe=tahunan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_search').click(function () {
        form = '.kt-form';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        $(form).attr('action', APP_URLAPP + '/id/pengaduan_pengguna?report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    // Bagian I : Jenis Produk dan / atau layanan dan permasalahan yang diadukan

    $('.jenis_produk1').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk1");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk1').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    $('.jenis_produk2').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk2");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk2').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    $('.jenis_produk3').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk3");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk3').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    $('.jenis_produk4').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk4");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk4').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    $('.jenis_produk5').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk5");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk5').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    $('.jenis_produk6').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk6");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk6').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    $('.jenis_produk7').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk7");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk7').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    $('.jenis_produk8').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk8");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk8').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    $('.jenis_produk9').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk9");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk9').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    $('.jenis_produk10').on('change', function () {
        var sum = 0;

        var inputs = $(".jenis_produk10");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_jenis_produk10').val(sum);
        var total = totalJenisProduk();
        $('.total_jenis_produk').val(total);
    });

    // Bagian II : Pengaduan yang diselesaikan dalam masa laporan
    // 20
    $('.pengaduan_selesai_sebelumnya_20').on('change', function () {
        var sum = 0;

        var inputs = $(".pengaduan_selesai_sebelumnya_20");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_pengaduan_selesai_sebelumnya_20').val(sum);
        var total = total20();
        $('.total_20').val(total);
    });

    $('.pengaduan_diterima_dalam_periode_20').on('change', function () {
        var sum = 0;

        var inputs = $(".pengaduan_diterima_dalam_periode_20");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_pengaduan_diterima_dalam_periode_20').val(sum);
        var total = total20();
        $('.total_20').val(total);
    });

    // H
    $('.pengaduan_selesai_sebelumnya_h').on('change', function () {
        var sum = 0;

        var inputs = $(".pengaduan_selesai_sebelumnya_h");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_pengaduan_selesai_sebelumnya_h').val(sum);
        var total = totalH();
        $('.total_h').val(total);
    });

    $('.pengaduan_diterima_dalam_periode_h').on('change', function () {
        var sum = 0;

        var inputs = $(".pengaduan_diterima_dalam_periode_h");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_pengaduan_diterima_dalam_periode_h').val(sum);
        var total = totalH();
        $('.total_h').val(total);
    });

    // 40
    $('.pengaduan_selesai_sebelumnya_40').on('change', function () {
        var sum = 0;

        var inputs = $(".pengaduan_selesai_sebelumnya_40");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_pengaduan_selesai_sebelumnya_40').val(sum);
        var total = total40();
        $('.total_40').val(total);
    });

    $('.pengaduan_diterima_dalam_periode_40').on('change', function () {
        var sum = 0;

        var inputs = $(".pengaduan_diterima_dalam_periode_40");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_pengaduan_diterima_dalam_periode_40').val(sum);
        var total = total40();
        $('.total_40').val(total);
    });

    // Bagian III: Penyebab Pengaduan
    $('.penyebab_pengaduan').on('change', function () {
        var sum = 0;

        var inputs = $(".penyebab_pengaduan");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_penyebab_pengaduan').val(sum);
    });

    // Bagian IV: Publikasi Negatif Pengaduan
    $('.publikasi_negatif').on('change', function () {
        var sum = 0;

        var inputs = $(".publikasi_negatif");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            sum += parseInt(value);
            console.log(sum);
        }
        $('.total_publikasi_negatif').val(sum);
    });

}

function numberFormat(x) {
    var result = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return result;
}

function addCurrency(x) {
    var value = x.replace('.00', '');
    value += '';
    x = value.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2 + '.00';
}

function totalJenisProduk() {
    sum = 0;
    for (let i = 1; i < 11; i++) {
        sum += parseInt($('.total_jenis_produk' + i).val());
        console.log(i);
    }
    return sum;
}

function total20() {
    var a = parseInt($('.total_pengaduan_selesai_sebelumnya_20').val());
    var b = parseInt($('.total_pengaduan_diterima_dalam_periode_20').val());
    var sum = a + b;
    return sum;
}

function totalH() {
    var a = parseInt($('.total_pengaduan_selesai_sebelumnya_h').val());
    var b = parseInt($('.total_pengaduan_diterima_dalam_periode_h').val());
    var sum = a + b;
    return sum;
}

function total40() {
    var a = parseInt($('.total_pengaduan_selesai_sebelumnya_40').val());
    var b = parseInt($('.total_pengaduan_diterima_dalam_periode_40').val());
    var sum = a + b;
    return sum;
}




