$(document).ajaxStart(function () {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
});

$(document).ajaxStop(function () {
    setTimeout(function () {
        KTApp.unblockPage();
    });
});

$(document).ready(function () {
    init();
    loadData(null);
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {
    var table = '.dt_rbank';
    var action = 'rincian_bank';
    var form_rbank = '.form_rbank';
    var form = '.form_add';
    var APP_URLAPP = $('#APP_URLAPP').val();
    var locate = $('#locate').val();
    var token = $('#_token').val();
    var url = APP_URLAPP + '/' + action + '/';

    //EDIT
    $(document).delegate('.btn_edit', "click", function (event) {
        var id = $(this).data('id');
        var component_desc = $(this).data('component_desc');
        $('#modal_heading').html("Edit Component " + component_desc);
        $('#modal_edit').modal('show');
        var APP_URLAPP = $('#APP_URLAPP').val();

        var table = $('.dt_report_edit').DataTable({
            'ajax': {
                "type": "GET",
                "url": APP_URLAPP + '/report_list/edit?id=' + id,
                "dataSrc": "",
            },
            'destroy': true,
            columns: [
                { data: "component_id" },
                { data: "component_desc" },
                { data: "line_code" },
                {
                    data: function (data) {
                        check = '<input type="checkbox" value="' + data.component_id + '|' + data.line_code + '"><span></span>';
                        return check;
                    }
                },
            ]
        });

    });

    $('#component_id').on('change', function (e) {
        var id = this.value;
        var APP_URLAPP = $('#APP_URLAPP').val();

        $.ajax({
            url: APP_URLAPP + '/report_list/edit?id=' + id,
            type: "GET",
            success: function (data, textStatus, jqXHR) {
                console.log(data);

                // $('#list_component').remove();
                $('#list_component').html(data);
                // $('#list_component').KTDualListbox('refresh', true);
                KTDualListbox.init()
            }
        });
        // alert(valueSelected);
    });
}

/*-------------------------------------------
 * RELOAD PAGE
 *-----------------------------------------*/
function reloadPage() {
    location.reload(true);
}

/*-------------------------------------------
 * LOAD DATA LIST
 *-----------------------------------------*/
function loadData(parameter) {
    console.log("load datatable");
    var APP_URLAPP = $('#APP_URLAPP').val();
    var locate = $('#locate').val();
    var id = $('#id').val();
    var table = $('.dt_report_list_component').DataTable({
        'ajax': {
            "type": "GET",
            "url": APP_URLAPP + '/report_list/list_component?id=' + id,
            "dataSrc": "",
        },
        'destroy': true,
        columns: [
            { data: "num_row" },
            { data: "component_code" },
            { data: "component_desc" },
            {
                data: function (data) {
                    edit = '<button type="button" data-id="' + data.component_id + '" data-component_desc="' + data.component_desc + '"  class="btn btn-primary btn-sm btn-icon btn_edit"><i class="fa fa-eye"></i></button>';
                    return edit;
                }, orderable: false
            },
        ]
    });

}

