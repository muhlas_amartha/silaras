$(document).ajaxStart(function () {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
});

$(document).ajaxStop(function () {
    setTimeout(function () {
        KTApp.unblockPage();
    });
});

$(document).ready(function () {
    init();
    loadData(null);
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {
    var table = '.dt_rbank';
    var action = 'rincian_bank';
    var form_rbank = '.form_rbank';
    var form = '.form_add';
    var APP_URLAPP = $('#APP_URLAPP').val();
    var locate = $('#locate').val();
    var token = $('#_token').val();
    var url = APP_URLAPP + '/' + action + '/';

    //EDIT
    $(document).delegate('.btn_edit', "click", function (event) {
        var id = $(this).data('id');
        var report_name = $(this).data('report_name');
        $('#modal_heading').html("Edit Report " + report_name);
        $('#modal_edit').modal('show');
        var APP_URLAPP = $('#APP_URLAPP').val();

        var table = $('.dt_report_edit').DataTable({
            'ajax': {
                "type": "GET",
                "url": APP_URLAPP + '/report_list/edit?id=' + id,
                "dataSrc": "",
            },
            'destroy': true,
            columns: [
                { data: "component_id" },
                { data: "line_code" },
                // {
                //     data: function (data) {
                //         edit = '<button type="button" class="btn btn-primary btn-sm btn-icon btn_edit" data-id="' + data.report_id + '" data-report_name="' + data.report_name + '"><i class="fa fa-edit"></i></button>&nbsp;';
                //         return edit;
                //     }, orderable: false
                // },
            ]
        });

    });
}

/*-------------------------------------------
 * RELOAD PAGE
 *-----------------------------------------*/
function reloadPage() {
    location.reload(true);
}

/*-------------------------------------------
 * LOAD DATA LIST
 *-----------------------------------------*/
function loadData(parameter) {
    var APP_URLAPP = $('#APP_URLAPP').val();
    var locate = $('#locate').val();
    var table = $('.dt_report_list').DataTable({
        'ajax': {
            "type": "GET",
            "url": APP_URLAPP + '/report_list/list',
            "dataSrc": "",
        },
        'destroy': true,
        columns: [
            { data: "report_id" },
            { data: "report_name" },
            {
                data: function (data) {
                    edit = '<a href="' + APP_URLAPP + '/' + locate + '/report_list/component_form?id=' + data.report_id + '" target="_blank" class="btn btn-primary btn-sm btn-icon"><i class="fa fa-eye"></i></a>&nbsp;';
                    return edit;
                }, orderable: false
            },
        ]
    });

}

