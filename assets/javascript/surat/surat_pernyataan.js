
$(document).ready(function () {
    init();
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {

    var action = 'lap_neraca';
    var form_lneraca = '.form_lneraca';
    APP_URLAPP = $('#APP_URLAPP').val();
    locate = $('#locate').val();
    url = APP_URLAPP + '/' + action + '/'

    $('#btn_edit').click(function () {
        console.log('btn_edit click')
        $(form_lneraca).attr('action', '' + url + 'edit');
        $(form_lneraca).submit();
    });

    $('#btn_export_bulanan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/lap_neraca?tipe=bulanan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_export_tahunan_csv').click(function () {
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/lap_neraca?tipe=tahunan&report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('#btn_search').click(function () {
        form = '.kt-form';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        $(form).attr('action', APP_URLAPP + '/id/lap_neraca?report_year=' + search_report_year + '&report_month=' + search_report_month);
        $(form).submit();
    });

    $('.aset').on('change', function () {
        var sum = 0;

        var inputs = $(".aset");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            sum += Number(amount);
        }
        result = sum.toFixed(2);
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_aset').val(numberFormat(result));
    });

    $('.kewajiban').on('change', function () {
        var sum = 0;
        var inputs = $(".kewajiban");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            console.log(i + " => " + amount);
            sum += Number(amount);
        }
        result = sum.toFixed(2);
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_kewajiban').val(numberFormat(result));
        $('.total_kewajiban_dan_ekuitas').val(numberFormat(totalKewajibanDanEkuitas()));
    });

    $('.ekuitas').on('change', function () {
        var sum = 0;
        var inputs = $(".ekuitas");
        for (var i = 0; i < inputs.length; i++) {
            var value = $(inputs[i]).val();
            var amount = value.replace(/,/g, '');
            console.log(i + " => " + amount);
            sum += Number(amount);
        }
        result = sum.toFixed(2);
        var input = $(this).val();
        $(this).val(addCurrency(input));
        $('.total_ekuitas').val(numberFormat(result));
        $('.total_kewajiban_dan_ekuitas').val(numberFormat(totalKewajibanDanEkuitas()));
    });

}

function numberFormat(x) {
    var result = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return result;
}

function addCurrency(x) {
    var value = x.replace('.00', '');
    value += '';
    x = value.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2 + '.00';
}

function totalKewajibanDanEkuitas() {
    var total_kewajiban = $('.total_kewajiban').val().replace(/,/g, '');
    var total_ekuitas = $('.total_ekuitas').val().replace(/,/g, '');
    var total_kewajiban_dan_ekuitas = parseInt(total_kewajiban) + parseInt(total_ekuitas);
    var result = total_kewajiban_dan_ekuitas.toFixed(2);
    return result;
}




