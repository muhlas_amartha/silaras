$(document).ajaxStart(function(){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
});

$(document).ajaxStop(function(){
  setTimeout(function() {
    KTApp.unblockPage();
  },);
});

$(document).ready(function(){
    init();
    loadData(null);
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init(){
    $('.btn_edit').hide();
    var table = '.dt_user';
    var action = 'rincian_ewallet';
    var form_rewallet = '.form_rewallet';
    var form = '.form_add';
    var APP_URLAPP = $('#APP_URLAPP').val();
    var locate = $('#locate').val();
    var token = $('#_token').val();
    var url = APP_URLAPP+'/'+action+'/'

    //TAMBAH
    $('#btn_add').click(function() {
        modal = '.modal_add';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        $(modal+" #action_flag").val('A');
        $(modal+" #report_year").val(search_report_year);
        $(modal+" #report_month").val(search_report_month);
        $('.modal_add').modal('show');
    }); 

    var form = '.form_add'
    var pesan_error = 'There are some errors in your submission. Please correct them.'

    $('#btn-yes-add').click(function(){
        $(form).submit();
    });

    $( form ).validate({
        rules: {
            component_code : {required: true, number: true, maxlength: 12, minlength: 12},
            component_desc : {required: true},
            general_information : {required: true},
        },
        messages: {
            id_pelaporan: {
                required: "There is something wrong with id pelaporan, please call administrator"
            },
        },
        
        //display error alert on form submit
        invalidHandler: function(event, validator) {
            swal.fire({
                "title": "",
                "text": pesan_error,
                "type": "error",
                "confirmButtonClass": "btn btn-secondary",
                "onClose": function(e) {
                    console.log('on close event fired!');
                }
            });

            event.preventDefault();
        },

        submitHandler: function (event) {
            var token = $('#_token').val();
            $.ajax({
                url : url+'action'+'?_token=' + token,
                type: "POST",
                dataType: "json",
                data : {
                    "id": $(form+" #id").val(),
                    "action_flag" : $(form+" #action_flag").val(),
                    "report_year" : $(form+" #report_year").val(),
                    "report_month" : $(form+" #report_month").val(),
                    "component_code" : $(form+" #component_code").val(),
                    "component_desc" : $(form+" #component_desc").val(),
                    "general_information" : $(form+" #general_information").val(),
                },
                success: function(response, textStatus, jqXHR)
                {
                    $('.modal_add').modal('hide');
                    if(response.status == 1){
                        var pesan_add = 'Data Rincian E-Wallet Berhasil Disimpan'
                        var type = 'success'
                    }else{
                        var pesan_add = 'Data Rincian E-Wallet Gagal Disimpan'
                        var type = 'error';
                    }

                    swal.fire({
                        title: 'Done!',
                        text: pesan_add,
                        type: type,
                        "confirmButtonClass": "btn-finish"
                    })

                    $('.btn-finish').click(function() {
                        // location.href = APP_URLAPP+'/'+locate+'/'+action
                        $('.dt_rewallet').DataTable().ajax.reload();
                    });

                }
            });
        }
    });

    //EDIT
    $(document).delegate( '.btn_edit', "click",function (event) {
        var form_rewallet = '.form_rewallet';
        var return_submit = 0;
        var pesan_mandatory = $('#pesan_mandatory').val();

        t_id = $(this).attr('data-id');
        component_code = $('#component_code'+t_id).val();
        component_desc = $('#component_desc'+t_id).val();
        general_information = $('#general_information_'+t_id).val();

        if(component_code == '' || component_code == null){
            $(form_rewallet+' .alert_component_code_'+t_id).html(pesan_mandatory);
        }else if(component_desc == '' || component_desc == null){
            $(form_rewallet+' .alert_component_desc_'+t_id).html(pesan_mandatory);
        }else if(general_information == '' || general_information == null){
            $(form_rewallet+' .alert_general_information_'+t_id).html(pesan_mandatory);
        }else{
            return_submit = 1
        }

        if(return_submit == 1){
            $.ajax({
                url : ''+url+'edit'+'?_token=' + token,
                type: "POST",
                dataType: "json",
                data : {
                  "id": t_id,
                  "component_code": component_code,
                  "component_desc": component_desc,
                  "general_information": general_information,
                },
                success: function(response, textStatus, jqXHR)
                {
                    pesan_gagal_simpan = $('#pesan_gagal_simpan').val();
                    pesan_berhasil_simpan = $('#pesan_berhasil_simpan').val();
                    $('.dt_rewallet').DataTable().ajax.reload();

                    if(response.status == 1){
                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "200",
                          "hideDuration": "500",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        };
                        toastr.success(pesan_berhasil_simpan);
                    }else{
                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "200",
                          "hideDuration": "500",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        };

                        toastr.success(pesan_gagal_simpan);
                    }

                }
            });
        }
        // console.log(component_code)
    });

    //HAPUS
    $(document).delegate( '.btn-hapus', "click",function (event) {
        id = $(this).attr('data-id');
        nama = $(this).attr('data-nama');
        modal = '.modal_alert';

        $(modal+' .form_alert').attr('action',''+url+'delete');
        $(modal+' #id').val(id);
        $(modal+' #nama').val(nama);

        pesan_judul_hapus = $('#pesan_judul_hapus').val();
        pesan_ket_hapus = $('#pesan_ket_hapus').val();
        pesan_ya = $('#pesan_ya').val();
        pesan_tidak = $('#pesan_tidak').val();
        judul_blade = $('#judul_blade').val();

        swal.fire({
            title: pesan_judul_hapus,
            text: pesan_ket_hapus+judul_blade+' '+nama+' ?',
            type: "warning",

            buttonsStyling: false,

            confirmButtonText: "<i class='la la-check-square'></i>"+pesan_ya+"!",
            confirmButtonClass: "btn btn-danger btn-yes-hapus",

            showCancelButton: true,
            cancelButtonText: "<i class='la la-close'></i> "+pesan_tidak+"",
            cancelButtonClass: "btn btn-default btn-no-hapus"
        });

        $('.btn-yes-hapus').click(function(){
            // $('.form_alert').submit();

            $.ajax({
                url : ''+url+'delete'+'?_token=' + token,
                type: "POST",
                dataType: "json",
                data : {
                  "id": id,
                },
                success: function(response, textStatus, jqXHR)
                {
                    pesan_gagal_hapus = $('#pesan_gagal_hapus').val();
                    pesan_berhasil_hapus = $('#pesan_berhasil_hapus').val();
                    $('.dt_rewallet').DataTable().ajax.reload();

                    if(response.status == 1){
                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "200",
                          "hideDuration": "500",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        };
                        toastr.success(pesan_berhasil_hapus);
                    }else{
                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "200",
                          "hideDuration": "500",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        };

                        toastr.success(pesan_gagal_hapus);
                    }

                }
            });
        }); 
    });

    //EXPORT
    $('#btn_export_csv').click(function(){
        modal = '.modal_export';
        form = '.form_export';
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        var param = search_report_year+'#'+search_report_month
        $(form+' #type').val('csv');
        $(form+' #param').val(param);

        $(modal+' .form_export').attr('action',APP_URLAPP+'/export/rincian_ewallet');
        $(form).submit();
    });

    //SEARCH
    $('#btn_search').click(function(){
        var search_report_year = $('#search_report_year').val();
        var search_report_month = $('#search_report_month').val();

        var param = search_report_year+'#'+search_report_month
        loadData(param);
    });

}


/*-------------------------------------------
 * RELOAD PAGE
 *-----------------------------------------*/
 function reloadPage(){
    location.reload(true);
 }

 function enabledButton(t_id){
    $(".btn_edit_"+t_id).removeClass("disabled");
    $(".btn_edit_"+t_id).prop("disabled", false);
    $(".btn_edit_"+t_id).show();
 }

 function changeComponentCode(t_id){
    var form_rewallet = '.form_rewallet';
    var pesan_mandatory = $('#pesan_mandatory').val();

    component_code = $('#component_code'+t_id).val();
    component_code_length = component_code.length;

    if(component_code == '' || component_code == null){
        $(form_rewallet+' .alert_component_code_'+t_id).html(pesan_mandatory);
        $(form_rewallet+' .alert_component_code_'+t_id).val('1');
    }else if(!/^[0-9]+$/.test(component_code)){
        $(form_rewallet+' .alert_component_code_'+t_id).html('Please enter a valid number.');
        $(form_rewallet+' .alert_component_code_'+t_id).val('1');
    }else if(component_code_length !== 12){
        $(form_rewallet+' .alert_component_code_'+t_id).html('Please enter at least 12 characters.');
        $(form_rewallet+' .alert_component_code_'+t_id).val('1');
    }else{
        $(form_rewallet+' .alert_component_code_'+t_id).html('');
        $(form_rewallet+' .alert_component_code_'+t_id).val('0');
    }

    alert_component_code = $(form_rewallet+' .alert_component_code_'+t_id).val();
    alert_component_desc = $(form_rewallet+' .alert_component_desc_'+t_id).val();
    alert_general_information = $(form_rewallet+' .alert_general_information_'+t_id).val();

    if(alert_component_code == 1 || alert_component_desc == 1 || alert_general_information == 1){
        $(".btn_edit_"+t_id).addClass("disabled");
        $(".btn_edit_"+t_id).hide();
    }else{
        enabledButton(t_id)
    }
 }

 function changeComponentDesc(t_id){
    var form_rewallet = '.form_rewallet';
    var pesan_mandatory = $('#pesan_mandatory').val();

    component_desc = $('#component_desc'+t_id).val();

    if(component_desc == '' || component_desc == null){
        $(form_rewallet+' .alert_component_desc_'+t_id).html(pesan_mandatory);
        $(form_rewallet+' .alert_component_desc_'+t_id).val('1');
        $(".btn_edit_"+t_id).addClass("disabled");
        $(".btn_edit_"+t_id).hide();
    }else{
        $(form_rewallet+' .alert_component_desc_'+t_id).html('');
        $(form_rewallet+' .alert_component_desc_'+t_id).val('0');
        enabledButton(t_id)
    }
    
    alert_component_code = $(form_rewallet+' .alert_component_code_'+t_id).val();
    alert_component_desc = $(form_rewallet+' .alert_component_desc_'+t_id).val();
    alert_general_information = $(form_rewallet+' .alert_general_information_'+t_id).val();

    if(alert_component_code == 1 || alert_component_desc == 1 || alert_general_information == 1){
        $(".btn_edit_"+t_id).addClass("disabled");
        $(".btn_edit_"+t_id).hide();
    }else{
        enabledButton(t_id)
    }
 }

 function changeGeneralInformation(t_id){
    var form_rewallet = '.form_rewallet';
    var pesan_mandatory = $('#pesan_mandatory').val();

    general_information = $('#general_information_'+t_id).val();

    if(general_information == '' || general_information == null){
        $(form_rewallet+' .alert_general_information_'+t_id).html(pesan_mandatory);
        $(form_rewallet+' .alert_general_information_'+t_id).val('1');
        $(".btn_edit_"+t_id).addClass("disabled");
        $(".btn_edit_"+t_id).hide();
    }else{
        $(form_rewallet+' .alert_general_information_'+t_id).html('');
        $(form_rewallet+' .alert_general_information_'+t_id).val('0');
        enabledButton(t_id)
    }

    alert_component_code = $(form_rewallet+' .alert_component_code_'+t_id).val();
    alert_component_desc = $(form_rewallet+' .alert_component_desc_'+t_id).val();
    alert_general_information = $(form_rewallet+' .alert_general_information_'+t_id).val();

    if(alert_component_code == 1 || alert_component_desc == 1 || alert_general_information == 1){
        $(".btn_edit_"+t_id).addClass("disabled");
        $(".btn_edit_"+t_id).hide();
    }else{
        enabledButton(t_id)
    }
 }

 function clickTxt(t_id){
    $(".btn_edit").addClass("disabled");
    $(".btn_edit").hide();

    var form_rewallet = '.form_rewallet';
    alert_component_code = $(form_rewallet+' .alert_component_code_'+t_id).val();
    alert_component_desc = $(form_rewallet+' .alert_component_desc_'+t_id).val();
    alert_general_information = $(form_rewallet+' .alert_general_information_'+t_id).val();

    if(alert_component_code == 1 || alert_component_desc == 1 || alert_general_information == 1){
        $(".btn_edit_"+t_id).addClass("disabled");
        $(".btn_edit_"+t_id).hide();
    }else{
        enabledButton(t_id)
    }
 }

/*-------------------------------------------
 * LOAD DATA LIST
 *-----------------------------------------*/
 function loadData(parameter){
    APP_URLAPP = $('#APP_URLAPP').val();
    var search_report_year = $('#search_report_year').val();
    var search_report_month = $('#search_report_month').val();

    var search_param = search_report_year+'#'+search_report_month
    if (parameter == null) {
        param = search_param;
    } else {
        param = parameter;
    }

    table = $('.dt_rewallet').DataTable({
        'ajax': {
            "type"   : "GET",
            "url"    : APP_URLAPP+'/rincian_ewallet/list',
            "dataSrc": "",
            "data"  : "param"+param,
            "data" : function(d){
                d.param = param
            }
        },
        'destroy': true,
        "searching": false,
        "paging": false,
        "info":     false,
        "order": [[ 3, "asc" ]],
        // processing: true,
        // serverSide: true,
        columns: [
            {data: "t_id"},
            {data: "flag_detail"},
            {data: function(data){
                text = '<div class="validated">\
                        <input type="text" class="form-control" id="component_code'+data.t_id+'" name="component_code" value="'+data.component_code+'" onchange="changeComponentCode('+data.t_id+')" onclick="clickTxt('+data.t_id+')" maxlength="12">\
                        <div class="invalid-feedback alert_component_code_'+data.t_id+'" value=""></div>\
                    </div>'
                return text;
            }, orderable : false},
            {data: function(data){
                text = '<div class="validated">\
                        <input type="text" class="form-control" id="component_desc'+data.t_id+'" name="component_desc" value="'+data.component_desc+'" onchange="changeComponentDesc('+data.t_id+')" onclick="clickTxt('+data.t_id+')">\
                        <div class="invalid-feedback alert_component_desc_'+data.t_id+'" value=""></div>\
                    </div>'
                return text;
            }, orderable : false},
            {data: function(data){
                text = '<div class="validated">\
                        <input type="text" class="form-control" id="general_information_'+data.t_id+'" name="general_information" value="'+data.general_information+'" onchange="changeGeneralInformation('+data.t_id+')" onclick="clickTxt('+data.t_id+')">\
                        <div class="invalid-feedback alert_general_information_'+data.t_id+'" value=""></div>\
                    </div>'
                return text;
            }, orderable : false},
            {data: function(data){
                edit = '<button type="button" class="btn btn-success btn-sm btn-icon btn_edit btn_edit_'+data.t_id+' disabled" style="display: none;" disabled data-id="'+data.t_id+'"><i class="fa fa-save"></i></button>&nbsp;'
                hapus = '<button type="button" class="btn btn-danger btn-sm btn-icon btn-hapus" data-id="'+data.t_id+'" data-nama="'+data.general_information+'"><i class="fa fa-trash-alt"></i></button>&nbsp;'
                // btn = '<button type="button" class="btn btn-primary btn-sm btn-icon"><i class="fa fa-dollar-sign"></i></button>&nbsp;'
                return  edit + hapus;
            },orderable: false},
        ]
    });

    // num row
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}

