
$(document).ready(function () {
    init();
    loadData();
});

/*-------------------------------------------
 * INIT
 *-----------------------------------------*/
function init() {
    var table = '.dt_user';
    var action = 'profil';
    var form_profil = '.form_profil';
    APP_URLAPP = $('#APP_URLAPP').val();
    locate = $('#locate').val();
    url = APP_URLAPP + '/' + action + '/'

    $('#btn_edit').click(function () {
        console.log('btn_edit click')
        $(form_profil).attr('action', '' + url + 'edit');
        $(form_profil).submit();
    });

    $('#provinsi').change(function () {
        provinsi_code = $(form_profil + ' #provinsi').val();
        $.ajax({
            url: url + 'getkab',
            type: "GET",
            dataType: "json",
            data: "provinsi_code=" + provinsi_code,
            success: function (response) {
                var options = "";
                var i;

                for (var i = 0; i < response.length; i++) {
                    options += "<option value='" + response[i].city_code + "'>" + response[i].city_name + "</option>";
                    document.getElementById("kabkota").innerHTML = options;
                };
            }
        });
    });

    $('#btn_export_xlsx').click(function () {
        // alert("ok");
        modal = '.modal_export';
        form = '.form_export';
        $(form + ' #type').val('xlsx');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/profil');
        $(form).submit();
    });

    $('#btn_export_csv').click(function () {
        // alert("ok");
        modal = '.modal_export';
        form = '.form_export';
        $(form + ' #type').val('csv');

        $(modal + ' .form_export').attr('action', APP_URLAPP + '/export/profil');
        $(form).submit();
    });

}


/*-------------------------------------------
 * RELOAD PAGE
 *-----------------------------------------*/
function reloadPage() {
    location.reload(true);
}

/*-------------------------------------------
 * LOAD DATA LIST
 *-----------------------------------------*/
function loadData() {
    APP_URLAPP = $('#APP_URLAPP').val();

    table = $('.dt_profil').DataTable({
        'ajax': {
            "type": "GET",
            "url": APP_URLAPP + '/profil/list',
        },
        "order": [[3, "asc"]],
        processing: true,
        serverSide: true,
        columns: [
            { data: "t_id" },
            { data: "flag_detail" },
            { data: "component_code" },
            { data: "component_desc" },
            // {data: "general_information"},
            {
                data: function (data) {
                    text = '<div class="form-group">\
                        <input type="text" class="form-control" id="uraian" name="uraian">\
                    </div>'
                    return text;
                }, orderable: false
            },
            {
                data: function (data) {
                    btn = '<button type="button" class="btn btn-primary btn-sm btn-icon"><i class="fa fa-dollar-sign"></i></button>&nbsp;'
                    return btn;
                }, orderable: false
            },
        ]
    });


    // num row
    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

