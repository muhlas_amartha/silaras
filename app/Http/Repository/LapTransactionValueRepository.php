<?php

namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;
use App\Http\Helpers\BaseHelper;
use App\Http\Repository\MainRepository;

class LapTransactionValueRepository
{
    public $database = 'pgsql_silaras';
    public $table = 'silaras.t_690238300_lap_transaction_value';
    public $table_m_component = 'm_component';
    public $table_m_region = 'm_region';
    public $prirmay_key = 't_id';

    public function DataTable($param)
    {
        $data = DB::connection($this->database)->select(DB::raw(
            "SELECT
            NEW.t_id,
                NEW.parent_id,
                NEW.flag_detail,
                NEW.component_code,
                NEW.component_desc,
                NEW.province_code,
                NEW.province_desc,
                NEW.component_order,
                NEW.class_name,
                NEW.amount_position_current_month AS trx_amount,
                NEW.component_id,
                NEW.report_year,
                (
                SELECT SUM
                    ( ALL_BEFORE.amount_position_current_month )
                FROM
                    silaras.t_690238300_lap_transaction_value ALL_BEFORE
                WHERE
                    ALL_BEFORE.report_year :: INT <= NEW.report_year :: INT
                    AND ( CASE WHEN ALL_BEFORE.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE.report_month :: INT < NEW.report_month :: INT ELSE TRUE END )
                        AND ALL_BEFORE.component_id = NEW.component_id
                    ) AS trx_amount_all_before,
                    (
		SELECT
			ONE_MONTH_BEFORE.amount_position_current_month
		FROM
			silaras.t_690238300_lap_transaction_value ONE_MONTH_BEFORE
		WHERE
			( CASE WHEN NEW.report_month = '1' THEN ONE_MONTH_BEFORE.report_month = '12' ELSE ONE_MONTH_BEFORE.report_month :: INT = ( NEW.report_month :: INT - 1 ) END )
				AND (
				CASE

						WHEN NEW.report_month = '1' THEN
						ONE_MONTH_BEFORE.report_year :: INT = ( NEW.report_year :: INT - 1 ) ELSE ONE_MONTH_BEFORE.report_year = NEW.report_year
					END
					)
				) AS one_month_before
                FROM
            silaras.t_690238300_lap_transaction_value NEW where NEW.report_year = '$param[year]' AND NEW.report_month = '$param[month]' AND is_printed = 'Y' ORDER BY t_id ASC"
        ));
        return $data;
    }

    public function DatatableExport($param)
    {
        $data = DB::connection($this->database)->select(DB::raw(
            "SELECT NEW
                .t_id,
                NEW.parent_id,
                NEW.flag_detail,
                NEW.component_code,
                NEW.component_desc,
                NEW.province_code,
                NEW.province_desc,
                NEW.component_order,
                NEW.class_name,
                NEW.amount_position_current_month AS trx_amount,
                NEW.component_id,
                NEW.report_year,
                (
                SELECT SUM
                    ( ALL_BEFORE.amount_position_current_month )
                FROM
                    silaras.t_690238300_lap_transaction_value ALL_BEFORE
                WHERE
                    ALL_BEFORE.report_year :: INT <= NEW.report_year :: INT
                    AND ( CASE WHEN ALL_BEFORE.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE.report_month :: INT < NEW.report_month :: INT ELSE TRUE END )
                        AND ALL_BEFORE.component_id = NEW.component_id
                    ) AS trx_amount_all_before
                FROM
            silaras.t_690238300_lap_transaction_value NEW where NEW.report_year = '$param[year]' AND NEW.report_month = '$param[month]' AND is_printed = 'Y' ORDER BY t_id ASC"
        ));
        return $data;
    }

    public function DatatableExportYearly($param)
    {
        $data = DB::connection($this->database)->select(DB::raw(
            "SELECT
                NEW.t_id,
                NEW.parent_id,
                NEW.flag_detail,
                NEW.component_code,
                NEW.component_desc,
                NEW.province_code,
                NEW.province_desc,
                NEW.component_order,
                NEW.class_name,
                NEW.amount_position_current_month AS trx_amount,
                NEW.component_id,
                NEW.report_year,
                -- JANUARI -- 1 --
                ( SELECT SUM( ALL_BEFORE_JAN.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_JAN WHERE ALL_BEFORE_JAN.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_JAN.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_JAN.report_month :: INT < 1 ELSE TRUE END ) AND ALL_BEFORE_JAN.component_id = NEW.component_id) AS trx_amount_before_january,
                ( SELECT JAN.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value JAN WHERE JAN.report_year :: INT = NEW.report_year :: INT AND JAN.report_month :: INT = 1 AND JAN.component_id = NEW.component_id) AS trx_amount_january,
                -- FEBRUARI -- 2 --
                ( SELECT SUM( ALL_BEFORE_FEB.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_FEB WHERE ALL_BEFORE_FEB.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_FEB.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_FEB.report_month :: INT < 2 ELSE TRUE END ) AND ALL_BEFORE_FEB.component_id = NEW.component_id) AS trx_amount_before_february,
                ( SELECT FEB.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value FEB WHERE FEB.report_year :: INT = NEW.report_year :: INT AND FEB.report_month :: INT = 2 AND FEB.component_id = NEW.component_id) AS trx_amount_february,
                -- MARET -- 3 --
                ( SELECT SUM( ALL_BEFORE_MAR.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_MAR WHERE ALL_BEFORE_MAR.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_MAR.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_MAR.report_month :: INT < 3 ELSE TRUE END ) AND ALL_BEFORE_MAR.component_id = NEW.component_id) AS trx_amount_before_march,
                ( SELECT MAR.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value MAR WHERE MAR.report_year :: INT = NEW.report_year :: INT AND MAR.report_month :: INT = 3 AND MAR.component_id = NEW.component_id) AS trx_amount_march,
                -- APRIL -- 4 --
                ( SELECT SUM( ALL_BEFORE_APR.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_APR WHERE ALL_BEFORE_APR.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_APR.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_APR.report_month :: INT < 4 ELSE TRUE END ) AND ALL_BEFORE_APR.component_id = NEW.component_id) AS trx_amount_before_april,
                ( SELECT APR.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value APR WHERE APR.report_year :: INT = NEW.report_year :: INT AND APR.report_month :: INT = 4 AND APR.component_id = NEW.component_id) AS trx_amount_april,
                -- MEI -- 5 --
                ( SELECT SUM( ALL_BEFORE_MEI.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_MEI WHERE ALL_BEFORE_MEI.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_MEI.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_MEI.report_month :: INT < 5 ELSE TRUE END ) AND ALL_BEFORE_MEI.component_id = NEW.component_id) AS trx_amount_before_mei,
                ( SELECT MEI.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value MEI WHERE MEI.report_year :: INT = NEW.report_year :: INT AND MEI.report_month :: INT = 5 AND MEI.component_id = NEW.component_id) AS trx_amount_mei,
                -- JUNI -- 6 --
                ( SELECT SUM( ALL_BEFORE_JUN.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_JUN WHERE ALL_BEFORE_JUN.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_JUN.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_JUN.report_month :: INT < 6 ELSE TRUE END ) AND ALL_BEFORE_JUN.component_id = NEW.component_id) AS trx_amount_before_june,
                ( SELECT JUN.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value JUN WHERE JUN.report_year :: INT = NEW.report_year :: INT AND JUN.report_month :: INT = 3 AND JUN.component_id = NEW.component_id) AS trx_amount_june,
                -- JULI -- 7 --
                ( SELECT SUM( ALL_BEFORE_JUL.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_JUL WHERE ALL_BEFORE_JUL.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_JUL.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_JUL.report_month :: INT < 7 ELSE TRUE END ) AND ALL_BEFORE_JUL.component_id = NEW.component_id) AS trx_amount_before_july,
                ( SELECT JUL.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value JUL WHERE JUL.report_year :: INT = NEW.report_year :: INT AND JUL.report_month :: INT = 7 AND JUL.component_id = NEW.component_id) AS trx_amount_july,
                -- AGUSTUS -- 8 --
                ( SELECT SUM( ALL_BEFORE_AUG.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_AUG WHERE ALL_BEFORE_AUG.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_AUG.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_AUG.report_month :: INT < 8 ELSE TRUE END ) AND ALL_BEFORE_AUG.component_id = NEW.component_id) AS trx_amount_before_august,
                ( SELECT AUG.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value AUG WHERE AUG.report_year :: INT = NEW.report_year :: INT AND AUG.report_month :: INT = 8 AND AUG.component_id = NEW.component_id) AS trx_amount_august,
                -- SEPTEMBER -- 9 --
                ( SELECT SUM( ALL_BEFORE_SEP.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_SEP WHERE ALL_BEFORE_SEP.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_SEP.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_SEP.report_month :: INT < 9 ELSE TRUE END ) AND ALL_BEFORE_SEP.component_id = NEW.component_id) AS trx_amount_before_september,
                ( SELECT SEP.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value SEP WHERE SEP.report_year :: INT = NEW.report_year :: INT AND SEP.report_month :: INT = 9 AND SEP.component_id = NEW.component_id) AS trx_amount_september,
                -- OKTOBER -- 10 --
                ( SELECT SUM( ALL_BEFORE_OCT.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_OCT WHERE ALL_BEFORE_OCT.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_OCT.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_OCT.report_month :: INT < 10 ELSE TRUE END ) AND ALL_BEFORE_OCT.component_id = NEW.component_id) AS trx_amount_before_october,
                ( SELECT OCT.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value OCT WHERE OCT.report_year :: INT = NEW.report_year :: INT AND OCT.report_month :: INT = 10 AND OCT.component_id = NEW.component_id) AS trx_amount_october,
                -- NOVEMBER -- 11 --
                ( SELECT SUM( ALL_BEFORE_NOV.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_NOV WHERE ALL_BEFORE_NOV.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_NOV.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_NOV.report_month :: INT < 11 ELSE TRUE END ) AND ALL_BEFORE_NOV.component_id = NEW.component_id) AS trx_amount_before_november,
                ( SELECT NOV.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value NOV WHERE NOV.report_year :: INT = NEW.report_year :: INT AND NOV.report_month :: INT = 11 AND NOV.component_id = NEW.component_id) AS trx_amount_november,
                -- DESEMBER -- 12 --
                ( SELECT SUM( ALL_BEFORE_DEC.amount_position_current_month ) FROM silaras.t_690238300_lap_transaction_value ALL_BEFORE_DEC WHERE ALL_BEFORE_DEC.report_year :: INT <= NEW.report_year :: INT AND ( CASE WHEN ALL_BEFORE_DEC.report_year :: INT = NEW.report_year :: INT THEN ALL_BEFORE_DEC.report_month :: INT < 12 ELSE TRUE END ) AND ALL_BEFORE_DEC.component_id = NEW.component_id) AS trx_amount_before_december,
                ( SELECT DEC.amount_position_current_month FROM silaras.t_690238300_lap_transaction_value DEC WHERE DEC.report_year :: INT = NEW.report_year :: INT AND DEC.report_month :: INT = 12 AND DEC.component_id = NEW.component_id) AS trx_amount_december
            FROM
                silaras.t_690238300_lap_transaction_value NEW
            WHERE
            NEW.report_year = '$param[year]' AND is_printed = 'Y' ORDER BY t_id ASC"
        ));
        return $data;
    }

    //=============================================== GETDATA ===============================================

    public function GetHeader($report_name)
    {
        $data = DB::connection($this->database)
            ->table('silaras.h_000000000_header')
            ->where('report_name', $report_name)
            ->first();

        return $data;
    }

    public function FirstDataComponentCode($component_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->where('component_code', $component_code)
            ->first();

        return $data;
    }

    public function FirstDataValidator($id)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->select(
                DB::raw('
                t_110038300_lap_neraca.*
                ')
            )
            // ->join('m_component', 'm_component.component_code', '=', 't_000038300_profil_perusahaan.component_code')
            // ->where('t_000038300_profil_perusahaan.t_id', $id)
            ->where('t_id', $id)
            ->first();

        return $data;
    }

    public function GetData()
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->get();

        return $data;
    }

    public function GetProvinsi()
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_region)
            ->select(
                DB::raw('
                    province_code, province_name
                ')
            )
            ->groupBy('province_code', 'province_name')
            ->orderBy('province_name')
            ->get();

        return $data;
    }

    public function GetKabKota($province_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_region)
            ->where('province_code', $province_code)
            ->orderBy('city_name')
            ->get();

        return $data;
    }

    public function GetKabKotaCityCode($city_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_region)
            ->where('city_code', $city_code)
            ->get();

        return $data;
    }

    public function GetMax()
    {
        $max_year = DB::connection($this->database)
            ->table($this->table)
            // ->where('status', true)
            ->max('report_year');

        $max_month = DB::connection($this->database)
            ->table($this->table)
            // ->where('status', true)
            ->max('report_month');

        $data = array(
            'max_year' => $max_year,
            'max_month' => $max_month
        );

        return $data;
    }

    //=============================================== POSTDATA ===============================================

    public function Edit($data)
    {
        // dd($data);
        date_default_timezone_set("Asia/Jakarta");
        foreach ($data['trx_amount'] as $key => $value) {
            // echo str_replace(',', '', $value) . '<br>';
            $proses = DB::connection($this->database)->table($this->table)
                ->where($this->prirmay_key, $key)
                ->update(
                    array(
                        'amount_position_current_month' => str_replace(',', '', $value),
                        'updated_by' => $data['id_user'],
                        'updated_on' => date('Y-m-d H:i:s'),
                    )
                );
        }
        // die;

        if ($proses) {
            $hasil = array('status' => 1, 'message' => 'Amount Berhasil Diubah');
        } else {
            $hasil = array('status' => 0, 'message' => 'Amount Gagal Diubah');
        }

        return $hasil;
    }
}
