<?php

namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;
use App\Http\Helpers\BaseHelper;
use App\Http\Repository\MainRepository;

class LapNeracaRepository
{
    public $database = 'pgsql_silaras';
    public $table = 'silaras.t_110038300_lap_neraca';
    public $table_m_component = 'm_component';
    public $table_m_region = 'm_region';
    public $prirmay_key = 't_id';

    public function DataTable($param)
    {
        $data = DB::connection($this->database)->select(DB::raw(
            "select new.t_id, new.parent_id, new.flag_detail, new.component_code, new.component_desc, new.component_order, new.class_name, new.trx_amount, new.component_id, new.report_year,
                    (
                        Select
                            old.trx_amount
                            from silaras.t_110038300_lap_neraca old
                            where old.report_year::int = new.report_year::int-1
                            and old.component_id = new.component_id
                    )as trx_amount_before

            from silaras.t_110038300_lap_neraca new where new.report_year = '$param[year]' AND new.report_month = '$param[month]' ORDER BY t_id ASC"
        ));
        return $data;
    }

    public function DatatableExport($param)
    {
        $data = DB::connection($this->database)->select(DB::raw(
            "select new.t_id, new.parent_id, new.flag_detail, new.component_code, new.component_desc, new.component_order, new.class_name, new.trx_amount, new.component_id, new.report_year,
                    (
                        Select
                            old.trx_amount
                            from silaras.t_110038300_lap_neraca old
                            where old.report_year::int = new.report_year::int-1
                            and old.component_id = new.component_id
                    )as trx_amount_before

            from silaras.t_110038300_lap_neraca new where new.report_year = '$param[year]' AND new.report_month = '$param[month]' AND is_printed = 'Y' ORDER BY t_id ASC"
        ));
        return $data;
    }

    //=============================================== GETDATA ===============================================

    public function GetHeader($report_name)
    {
        $data = DB::connection($this->database)
            ->table('silaras.h_000000000_header')
            ->where('report_name', $report_name)
            ->first();

        return $data;
    }

    public function FirstDataComponentCode($component_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->where('component_code', $component_code)
            ->first();

        return $data;
    }

    public function FirstDataValidator($id)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->select(
                DB::raw('
                t_110038300_lap_neraca.*
                ')
            )
            // ->join('m_component', 'm_component.component_code', '=', 't_000038300_profil_perusahaan.component_code')
            // ->where('t_000038300_profil_perusahaan.t_id', $id)
            ->where('t_id', $id)
            ->first();

        return $data;
    }

    public function GetData()
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->get();

        return $data;
    }

    public function GetProvinsi()
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_region)
            ->select(
                DB::raw('
                    province_code, province_name
                ')
            )
            ->groupBy('province_code', 'province_name')
            ->orderBy('province_name')
            ->get();

        return $data;
    }

    public function GetKabKota($province_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_region)
            ->where('province_code', $province_code)
            ->orderBy('city_name')
            ->get();

        return $data;
    }

    public function GetKabKotaCityCode($city_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_region)
            ->where('city_code', $city_code)
            ->get();

        return $data;
    }

    public function GetMax()
    {
        $max_year = DB::connection($this->database)
            ->table($this->table)
            // ->where('status', true)
            ->max('report_year');

        $max_month = DB::connection($this->database)
            ->table($this->table)
            // ->where('status', true)
            ->max('report_month');

        $data = array(
            'max_year' => $max_year,
            'max_month' => $max_month
        );

        return $data;
    }

    //=============================================== POSTDATA ===============================================

    public function Edit($data)
    {
        // dd($data);
        date_default_timezone_set("Asia/Jakarta");
        foreach ($data['trx_amount'] as $key => $value) {
            // echo str_replace(',', '', $value) . '<br>';
            $proses = DB::connection($this->database)->table($this->table)
                ->where($this->prirmay_key, $key)
                ->update(
                    array(
                        'trx_amount' => str_replace(',', '', $value),
                        'updated_by' => $data['id_user'],
                        'updated_on' => date('Y-m-d H:i:s'),
                    )
                );
        }
        // die;

        if ($proses) {
            $hasil = array('status' => 1, 'message' => 'Amount Berhasil Diubah');
        } else {
            $hasil = array('status' => 0, 'message' => 'Amount Gagal Diubah');
        }

        return $hasil;
    }
}
