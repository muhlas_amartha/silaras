<?php

namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;
use App\Http\Helpers\BaseHelper;

class MainRepository
{
    public $database = 'pgsql_silaras';
    public $m_parameter = 'm_parameter';


    //=============================================== GETDATA ===============================================
    public function GetIdMenu($url_menu)
    {
        $data = DB::connection($this->database)
            ->table('sec_menu')
            ->where('url_menu', $url_menu)
            ->first();

        return $data;
    }

    public function GetMenu($username)
    {
        $data = DB::connection($this->database)
            ->table('sec_user')
            ->select(
                DB::raw('
                            sec_menu.id_menu, sec_menu.nama_menu, sec_menu.url_menu, sec_menu.icon_menu, sec_menu.parent_menu, sec_menu.status_menu, sec_menu.order_menu, sec_menu.menu_is_parent
                            ')
            )
            ->leftJoin('sec_mapping', 'sec_mapping.id_role', '=', 'sec_user.id_role')
            ->leftJoin('sec_menu', 'sec_menu.id_menu', '=', 'sec_mapping.id_menu')
            ->where('username', $username)
            ->where('status_user', 1)
            ->where('status_menu', 1)
            ->where('status_mapping', 1)
            ->orderBy('sec_menu.order_menu', 'ASC')
            ->get();
        // dd($data);
        return $data;
    }

    public function GetNotif($param)
    {
        // dd($param);
        $data = DB::connection($this->database)
            ->table('m_notif')
            ->select(
                DB::raw('
                id_notif, judul_notif, ket_notif, status_notif
                ')
            )
            ->where('status_notif', 1)
            ->where('id_param_level', $param['user_level'])
            ->where('notif_for_divisi', $param['id_divisi'])
            ->get();

        $count = DB::connection($this->database)
            ->table('m_notif')
            ->where('status_notif', 1)
            ->where('id_param_level', $param['user_level'])
            ->where('notif_for_divisi', $param['id_divisi'])
            ->count();

        $return = array(
            'data' => $data,
            'count' => $count
        );

        return $return;
    }

    public function GetMDivisi()
    {
        $data = DB::connection($this->database)
            ->table('m_divisi')
            ->orderBy('nama_divisi')
            ->where('status_divisi', 1)
            ->get();

        return $data;
    }

    public function GetSecRole()
    {
        $data = DB::connection($this->database)
            ->table('sec_role')
            ->orderBy('id_role')
            ->where('status_role', 1)
            ->get();

        return $data;
    }

    public function FirstParam($param_title, $param_type)
    {
        $data = DB::connection($this->database)
            ->table($this->m_parameter)
            ->where('param_title', $param_title)
            ->where('param_type', $param_type)
            ->first();

        return $data;
    }

    public function GetParamTitleType($param_title, $param_type)
    {
        $data = DB::connection($this->database)
            ->table($this->m_parameter)
            ->select(
                DB::raw('
                id_param, param_nama, param_status, param_value
                ')
            )
            ->where('param_title', $param_title)
            ->where('param_type', $param_type)
            ->orderBy('param_order')
            ->get();

        return $data;
    }

    //=============================================== POSTDATA ===============================================
    public function AddLogActivity($data)
    {
        date_default_timezone_set("Asia/Jakarta");
        $data = DB::connection($this->database)->table('m_log_activity')->insert(
            array(
                'current_url' => $data['current_url'],
                'event' => $data['event'],
                'ip_address' => $data['ip_address'],
                'description' => $data['description'],
                'created_by' => $data['created_by'],
                'created_name' => $data['created_name'],
                'created_on' => date('Y-m-d H:i:s'),
                'before_log' => $data['before_log'],
                'after_log' => $data['after_log'],
            )
        );

        if ($data) {
            $hasil = array('status' => 1, 'message' => 'Log Succesfully');
        } else {
            $hasil = array('status' => 0, 'message' => 'Log UnSuccesfully');
        }

        return $hasil;
    }

    public function GetErrorLog($report_id, $report_year, $report_month)
    {
        // dd($report_id);
        $data = DB::connection($this->database)
            ->table('silaras.error_log')
            ->select('*')
            ->where('report_id', '=', $report_id)
            ->where('report_year', '=', $report_year)
            ->where('report_month', '=', $report_month)
            ->get();

        return $data;
    }
}
