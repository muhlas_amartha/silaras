<?php namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;
use App\Http\Helpers\BaseHelper;

Class WelcomeRepository
{
    public $database = 'pgsql_silaras';


    //=============================================== GETDATA ===============================================
    public function GetUserData($username)
    {
        $data = DB::connection($this->database)
                ->table('sec_user')
                ->select(
                    DB::raw('
                        sec_user.*, sec_role.role_name, m_divisi.nama_divisi, m_parameter.id_param as id_param_role, m_parameter.param_value as user_level
                    '))
                ->join('sec_role', 'sec_role.id_role', '=', 'sec_user.id_role')
                ->join('m_divisi', 'm_divisi.id_divisi', '=', 'sec_user.id_divisi')
                ->join('m_parameter', 'm_parameter.id_param', '=', 'sec_role.id_param_level')
                ->where('username', $username)
                ->first();          
        return $data;
    }

    public function GetMenu($username)
    {
        $data = DB::connection($this->database)
                        ->table('sec_user')
                        ->select(
                            DB::raw('
                                sec_menu.*
                            '))
                        ->leftJoin('sec_mapping', 'sec_mapping.id_role', '=', 'sec_user.id_role')
                        ->leftJoin('sec_menu', 'sec_menu.id_menu', '=', 'sec_mapping.id_menu')
                        ->where('username', $username)
                        ->where('status_user', 1)
                        ->where('status_menu', 1)
                        ->where('status_mapping', 1)
                        ->orderBy('sec_menu.order_menu', 'ASC')
                        ->get();
        // dd($data);
        return $data;
    }

    //=============================================== POSTDATA ===============================================
    public function AddLogActivity($data){
        date_default_timezone_set("Asia/Jakarta");
        $data = DB::connection($this->database)->table('m_log_activity')->insert(
                array(
                    'current_url' => $data['current_url'],
                    'event' => $data['event'],
                    'ip_address' => $data['ip_address'],
                    'description' => $data['description'],
                    'created_by' => $data['created_by'],
                    'created_name' => $data['created_name'],
                    'created_on' => date('Y-m-d H:i:s'),
                )
            );
    }
}
?>