<?php namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;
use App\Http\Helpers\BaseHelper;
use App\Http\Repository\MainRepository;

Class UserRepository
{
    public $database = 'pgsql_silaras';
    public $table = 'sec_user';

	public function DataTable($param){
        $data = DB::connection($this->database)
            ->table($this->table)
            ->select(
                DB::raw('
                    sec_user.*, sec_role.role_name, m_divisi.nama_divisi
                '))
            ->join('sec_role', 'sec_role.id_role', '=', 'sec_user.id_role')
            ->join('m_divisi', 'm_divisi.id_divisi', '=', 'sec_user.id_divisi')
            ->where('status_user', 1)
            ;

        $data = $data->get();
        return $data;
    }

    //=============================================== GETDATA ===============================================
    public function GetDataId($id){
        $data = DB::connection($this->database)
            ->table($this->table)
            ->select(
                DB::raw('
                    sec_user.*, sec_role.role_name, m_divisi.nama_divisi
                '))
            ->join('sec_role', 'sec_role.id_role', '=', 'sec_user.id_role')
            ->join('m_divisi', 'm_divisi.id_divisi', '=', 'sec_user.id_divisi')
            ->where('sec_user.id_user', $id)
            ;

        $data = $data->first();
        return $data;
    }

    public function GetPassword($id){
        $data = DB::connection($this->database)
            ->table($this->table)
            ->where('id_user', $id)
            ->first();

        return $data;
    }
    //=============================================== POSTDATA ===============================================
    public function Add($data){
        date_default_timezone_set("Asia/Jakarta");
        $proses = DB::connection($this->database)->table($this->table)->insert(
                array(
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'username' => $data['username'],
                    'password' => $data['password'],
                    'email' => $data['email'],
                    'nik' => $data['nik'],
                    'phone_number' => $data['phone_number'],
                    'id_divisi' => $data['id_divisi'],
                    'id_role' => $data['id_role'],
                    'status_user' => $data['status'],
                    'created_by' => $data['id_user'],
                    'created_name' => $data['nama_user'],
                    'created_on' => date('Y-m-d H:i:s'),
                )
            );
    
        if($proses){
            $hasil = array('status' => 1, 'message' => 'User Berhasil Disimpan');
        }else{
            $hasil = array('status' => 0, 'message' => 'User Gagal Disimpan');
        }

        return $hasil;
    }

    public function Edit($data)
    {
        date_default_timezone_set("Asia/Jakarta");
        $proses = DB::connection($this->database)->table($this->table)->where('id_user', $data['id'])
        ->update(
                array(
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'username' => $data['username'],
                    'password' => $data['password'],
                    'email' => $data['email'],
                    'nik' => $data['nik'],
                    'phone_number' => $data['phone_number'],
                    'id_divisi' => $data['id_divisi'],
                    'id_role' => $data['id_role'],
                    'updated_by' => $data['id_user'],
                    'updated_name' => $data['nama_user'],
                    'updated_on' => date('Y-m-d H:i:s'),
                )
            );
        
        if($proses){
            $hasil = array('status' => 1, 'message' => 'User Berhasil Diubah');
        }else{
            $hasil = array('status' => 0, 'message' => 'User Gagal Diubah');
        }
        
        return $hasil;
    }

    public function Deleted($data)
    {
        date_default_timezone_set("Asia/Jakarta");
        $proses = DB::connection($this->database)->table($this->table)->where('id_user', $data['id'])
        ->update(
                array(
                    'status_user' => $data['status'],
                    'deleted_by' => $data['id_user'],
                    'deleted_name' => $data['nama_user'],
                    'deleted_on' => date('Y-m-d H:i:s'),
                )
            );
        
        if($proses){
            $hasil = array('status' => 1, 'message' => 'User Berhasil Dihapus');
        }else{
            $hasil = array('status' => 0, 'message' => 'User Gagal Dihapus');
        }
        
        return $hasil;
    }

    public function ChangePassword($data)
    {
        // dd($data);
        date_default_timezone_set("Asia/Jakarta");
        $edit = array('status' => 0, 'message' => 'Gagal');

        $proses = DB::connection($this->database)->table($this->table)->where('id_user', $data['id_user'])
        ->update(
                array(
                    'password' => $data['password'],
                    'updated_password_by' => $data['id_user'],
                    'updated_password_name' => $data['nama_user'],
                    'updated_password_on' => date('Y-m-d H:i:s'),
                )
            );

        $proses = DB::connection($this->database)->table('sec_user_password')->insert(
                array(
                    'id_user' => $data['id_user'],
                    'password' => $data['password'],
                    'created_by' => $data['id_user'],
                    'created_on' => date('Y-m-d H:i:s'),
                )
            );
        
        if($proses){
            $edit = array('status' => 1, 'message' => 'Password Successfully Changed');
        }else{
            $edit = array('status' => 0, 'message' => 'Password Failed to Changed');
        }
        
        return $edit;
    }

    public function ResetPassword($data)
    {
        // dd($data);
        date_default_timezone_set("Asia/Jakarta");
        $edit = array('status' => 0, 'message' => 'Gagal');

        $proses = DB::connection($this->database)->table($this->table)->where('id_user', $data['id'])
        ->update(
                array(
                    'password' => $data['password'],
                    'updated_password_by' => $data['id_user'],
                    'updated_password_name' => $data['nama_user'],
                    'updated_password_on' => date('Y-m-d H:i:s'),
                )
            );

        $proses = DB::connection($this->database)->table('sec_user_password')->insert(
                array(
                    'id_user' => $data['id'],
                    'password' => $data['password'],
                    'created_by' => $data['id_user'],
                    'created_on' => date('Y-m-d H:i:s'),
                )
            );
        
        if($proses){
            $edit = array('status' => 1, 'message' => 'Password Successfully Reset');
        }else{
            $edit = array('status' => 0, 'message' => 'Password Failed to Reset');
        }
        
        return $edit;
    }

}
?>