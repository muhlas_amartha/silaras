<?php

namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;
use App\Http\Helpers\BaseHelper;
use App\Http\Repository\MainRepository;

class LapLabaRepository
{
    public $database = 'pgsql_silaras';
    public $table = 'silaras.t_120038300_lap_laba_rugi';
    public $prirmay_key = 't_id';

    public function DataTable($param)
    {
        $data = DB::connection($this->database)->select(DB::raw(
            "select  new.*,
                    (
                        Select
                            old.trx_amount
                            from silaras.t_120038300_lap_laba_rugi old
                            where old.report_year::int = new.report_year::int-1
                            and old.component_id = new.component_id
                    )as trx_amount_before

            from silaras.t_120038300_lap_laba_rugi new where new.report_year = '$param[year]' AND new.report_month = '$param[month]' ORDER BY t_id ASC"
        ));
        return $data;
    }

    public function DatatableExport($param)
    {
        $data = DB::connection($this->database)->select(DB::raw(
            "select  new.*,
                    (
                        Select
                            old.trx_amount
                            from silaras.t_120038300_lap_laba_rugi old
                            where old.report_year::int = new.report_year::int-1
                            and old.component_id = new.component_id
                    )as trx_amount_before

            from silaras.t_120038300_lap_laba_rugi new where new.report_year = '$param[year]' AND new.report_month = '$param[month]' AND is_printed = 'Y' ORDER BY t_id ASC"
        ));
        return $data;
    }

    //=============================================== GETDATA ===============================================

    public function GetHeader($report_name)
    {
        $data = DB::connection($this->database)
            ->table('silaras.h_000000000_header')
            ->where('report_name', $report_name)
            ->first();

        return $data;
    }

    public function FirstDataComponentCode($component_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->where('component_code', $component_code)
            ->first();

        return $data;
    }

    public function FirstDataId($id)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->where($this->prirmay_key, $id)
            ->first();

        return $data;
    }

    public function GetData()
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->get();

        return $data;
    }

    public function GetMax()
    {
        $max_year = DB::connection($this->database)
            ->table($this->table)
            ->where('status', true)
            ->max('report_year');

        $max_month = DB::connection($this->database)
            ->table($this->table)
            ->where('status', true)
            ->max('report_month');

        $data = array(
            'max_year' => $max_year,
            'max_month' => $max_month
        );

        return $data;
    }
    //=============================================== POSTDATA ===============================================
    public function Add($data)
    {
        date_default_timezone_set("Asia/Jakarta");
        $proses = DB::connection($this->database)->table($this->table)->insert(
            array(
                'report_year' => $data['report_year'],
                'report_month' => $data['report_month'],
                'component_code' => $data['component_code'],
                'component_desc' => $data['component_desc'],
                'general_information' => $data['general_information'],
                'status' => 't',
                'created_by' => $data['nama_user'],
                'created_on' => date('Y-m-d H:i:s'),
            )
        );

        if ($proses) {
            $hasil = array('status' => 1, 'message' => 'Laporan Laba Rugi Berhasil Disimpan');
        } else {
            $hasil = array('status' => 0, 'message' => 'Laporan Laba Rugi Gagal Disimpan');
        }

        return $hasil;
    }

    public function Edit($data)
    {
        date_default_timezone_set("Asia/Jakarta");
        foreach ($data['trx_amount'] as $key => $value) {
            $proses = DB::connection($this->database)->table($this->table)
                ->where($this->prirmay_key, $key)
                ->update(
                    array(
                        'trx_amount' => str_replace(',', '', $value),
                        'updated_by' => $data['id_user'],
                        'updated_on' => date('Y-m-d H:i:s'),
                    )
                );
        }
        $total_arus_kas = DB::connection($this->database)
            ->select(DB::raw("SELECT
            ( SELECT trx_amount FROM silaras.t_120138300_lap_perubahan_modal_ekuitas WHERE report_year = '$data[year]' AND report_month = '$data[month]' AND component_code = '341000000000' ) AS saldo_awal,
            ( SELECT trx_amount FROM silaras.t_120138300_lap_perubahan_modal_ekuitas WHERE report_year = '$data[year]' AND report_month = '$data[month]' AND component_code = '342000000000' ) AS total_pendapatan_net,
            ( SELECT trx_amount FROM silaras.t_120138300_lap_perubahan_modal_ekuitas WHERE report_year = '$data[year]' AND report_month = '$data[month]' AND component_code = '343000000000' ) AS transaksi_lain,
            ( SELECT trx_amount FROM silaras.t_120138300_lap_perubahan_modal_ekuitas WHERE report_year = '$data[year]' AND report_month = '$data[month]' AND component_code = '344000000000' ) AS deviden
            "));
        if ($total_arus_kas) {
            $total_laba = DB::connection($this->database)
                ->table($this->table)
                ->select('trx_amount')
                ->where('report_year', '=', $data['year'])
                ->where('report_month', '=', $data['month'])
                ->where('component_code', '=', '340302000000')
                ->first();
            $saldo_akhir = $total_arus_kas[0]->saldo_awal + $total_laba->trx_amount + $total_arus_kas[0]->transaksi_lain - $total_arus_kas[0]->deviden;
            $update_total_pendapatan_net = DB::connection($this->database)
                ->table('silaras.t_120138300_lap_perubahan_modal_ekuitas')
                ->where('report_year', '=', $data['year'])
                ->where('report_month', '=', $data['month'])
                ->where('component_code', '=', '342000000000')
                ->update(['trx_amount' => $total_laba->trx_amount]);

            $update_saldo_akhir = DB::connection($this->database)
                ->table('silaras.t_120138300_lap_perubahan_modal_ekuitas')
                ->where('report_year', '=', $data['year'])
                ->where('report_month', '=', $data['month'])
                ->where('component_code', '=', '345000000000')
                ->update(['trx_amount' => (string)$saldo_akhir . '.00']);
        }

        if ($proses) {
            $hasil = array('status' => 1, 'message' => 'Amount Berhasil Diubah');
        } else {
            $hasil = array('status' => 0, 'message' => 'Amount Gagal Diubah');
        }

        return $hasil;
    }

    public function Deleted($data)
    {
        date_default_timezone_set("Asia/Jakarta");
        $proses = DB::connection($this->database)->table($this->table)->where($this->prirmay_key, $data['id'])
            ->update(
                array(
                    'status' => $data['status'],
                    'deleted_by' => $data['nama_user'],
                    'deleted_on' => date('Y-m-d H:i:s'),
                )
            );

        if ($proses) {
            $hasil = array('status' => 1, 'message' => 'Laporan Laba Rugi Berhasil Dihapus');
        } else {
            $hasil = array('status' => 0, 'message' => 'Laporan Laba Rugi Gagal Dihapus');
        }

        return $hasil;
    }
}
