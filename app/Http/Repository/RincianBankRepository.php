<?php namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;
use App\Http\Helpers\BaseHelper;
use App\Http\Repository\MainRepository;

Class RincianBankRepository
{
    public $database = 'pgsql_silaras';
    public $table = 'silaras.t_000138300_bank_rincian_bank_escrow_dan_virtual_account';
    public $prirmay_key = 't_id';

    public function DataTable($param){

        $max_year = DB::connection($this->database)
            ->table($this->table)
            ->where('status', true)
            ->max('report_year');

        $max_month = DB::connection($this->database)
            ->table($this->table)
            ->where('status', true)
            ->max('report_month');

        // dd($max_year, $max_month, $param);

        $data = DB::connection($this->database)
            ->table($this->table)
            ->orderBy('t_id', 'asc')
            ->where('status', true)
            ;

        if($param['search_report_year']){
            $data = $data->where('report_year', $param['search_report_year']);
        }else{
            $data = $data->where('report_year', $max_year);
        }

        if($param['search_report_month']){
            $data = $data->where('report_month', $param['search_report_month']);
        }else{
            $data = $data->where('report_month', $max_month);
        }

        $data = $data->get();
        // dd($data);
        return $data;
    }

    //=============================================== GETDATA ===============================================

    public function FirstDataComponentCode($component_code){
        $data = DB::connection($this->database)
            ->table($this->table)
            ->where('component_code', $component_code)
            ->first();

        return $data;
    }

    public function FirstDataId($id){
        $data = DB::connection($this->database)
            ->table($this->table)
            ->where($this->prirmay_key, $id)
            ->first();

        return $data;
    }

    public function GetData(){
        $data = DB::connection($this->database)
            ->table($this->table)
            ->get();

        return $data;
    }

    public function GetMax(){
        $max_year = DB::connection($this->database)
            ->table($this->table)
            ->where('status', true)
            ->max('report_year');

        $max_month = DB::connection($this->database)
            ->table($this->table)
            ->where('status', true)
            ->max('report_month');

        $data = array(
            'max_year' => $max_year,
            'max_month' => $max_month
        );

        return $data;
    }
    //=============================================== POSTDATA ===============================================
    public function Add($data){
        date_default_timezone_set("Asia/Jakarta");
        $proses = DB::connection($this->database)->table($this->table)->insert(
                array(
                    'report_year' => $data['report_year'],
                    'report_month' => $data['report_month'],
                    'component_code' => $data['component_code'],
                    'component_desc' => $data['component_desc'],
                    'general_information' => $data['general_information'],
                    'status' => 't',
                    'created_by' => $data['nama_user'],
                    'created_on' => date('Y-m-d H:i:s'),
                )
            );
    
        if($proses){
            $hasil = array('status' => 1, 'message' => 'Rincian Bank Account Berhasil Disimpan');
        }else{
            $hasil = array('status' => 0, 'message' => 'Rincian Bank Account Gagal Disimpan');
        }

        return $hasil;
    }

    public function Edit($data)
    {
        // dd($data);
        date_default_timezone_set("Asia/Jakarta");
        $proses = DB::connection($this->database)->table($this->table)->where($this->prirmay_key, $data['id'])
        ->update(
                array(
                    'component_code' => $data['component_code'],
                    'component_desc' => $data['component_desc'],
                    'general_information' => $data['general_information'],
                    'updated_by' => $data['nama_user'],
                    'updated_on' => date('Y-m-d H:i:s'),
                )
            );
        if($proses){
            $hasil = array('status' => 1, 'message' => 'Rincian Bank Account Berhasil Diubah');
        }else{
            $hasil = array('status' => 0, 'message' => 'Rincian Bank Account Gagal Diubah');
        }
        
        return $hasil;
    }

    public function Deleted($data)
    {
        date_default_timezone_set("Asia/Jakarta");
        $proses = DB::connection($this->database)->table($this->table)->where($this->prirmay_key, $data['id'])
        ->update(
                array(
                    'status' => $data['status'],
                    'deleted_by' => $data['nama_user'],
                    'deleted_on' => date('Y-m-d H:i:s'),
                )
            );
        
        if($proses){
            $hasil = array('status' => 1, 'message' => 'Rincian Bank Account Berhasil Dihapus');
        }else{
            $hasil = array('status' => 0, 'message' => 'Rincian Bank Account Gagal Dihapus');
        }
        
        return $hasil;
    }

}
?>