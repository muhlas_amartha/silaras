<?php

namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;
use App\Http\Helpers\BaseHelper;
use App\Http\Repository\MainRepository;

class ReportListRepository
{
    public $database = 'pgsql_silaras';
    public $table = 'public.m_report';
    public $table_m_component = 'public.m_component';
    public $table_m_region = 'm_region';
    public $prirmay_key = 't_id';

    public function DataTable()
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->select('*')
            ->orderBy('report_id', 'asc');

        $data = $data->get();
        // dd($data);
        return $data;
    }

    public function DataTableComponent($id)
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_component)
            ->selectRaw('row_number() over (ORDER BY component_order ASC) as num_row,*')
            ->where('report_id', '=', $id)
            ->orderBy('component_order', 'asc');

        $data = $data->get();
        // dd($data);
        return $data;
    }

    public function DataTableEdit($report_id)
    {
        $data = DB::connection($this->database)
            ->table('parameter.line_mapping')
            ->select('*')
            ->join('public.m_component', 'parameter.line_mapping.component_id', '=', 'public.m_component.component_id')
            ->where('parameter.line_mapping.component_id', '=', $report_id)
            ->orderBy('parameter.line_mapping.line_id', 'asc');

        $data = $data->get();
        // dd($data);
        return $data;
    }

    public function getReportListById($id)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->select('*')
            ->where('report_id', '=', $id);

        $data = $data->first();
        // dd($data);
        return $data;
    }

    //=============================================== GETDATA ===============================================

    public function FirstDataComponentCode($component_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->where('component_code', $component_code)
            ->first();

        return $data;
    }

    public function FirstDataValidator($id)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->select(
                DB::raw('
                    m_component.*
                ')
            )
            ->join('m_component', 'm_component.component_code', '=', 't_000038300_profil_perusahaan.component_code')
            ->where('t_000038300_profil_perusahaan.t_id', $id)
            ->first();

        return $data;
    }

    public function GetData()
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->get();

        return $data;
    }

    public function GetProvinsi()
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_region)
            ->select(
                DB::raw('
                    province_code, province_name
                ')
            )
            ->groupBy('province_code', 'province_name')
            ->orderBy('province_name')
            ->get();

        return $data;
    }

    public function GetKabKota($province_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_region)
            ->where('province_code', $province_code)
            ->orderBy('city_name')
            ->get();

        return $data;
    }

    public function GetKabKotaCityCode($city_code)
    {
        $data = DB::connection($this->database)
            ->table($this->table_m_region)
            ->where('city_code', $city_code)
            ->get();

        return $data;
    }
    //=============================================== POSTDATA ===============================================

    public function Edit($data)
    {
        date_default_timezone_set("Asia/Jakarta");
        foreach ($data['general_information'] as $key => $value) {
            $proses = DB::connection($this->database)->table($this->table)
                ->where($this->prirmay_key, $key)
                ->update(
                    array(
                        'general_information' => $value,
                        'updated_by' => $data['id_user'],
                        'updated_on' => date('Y-m-d H:i:s'),
                    )
                );
        }

        if ($proses) {
            $hasil = array('status' => 1, 'message' => 'Profil Perusahaan Berhasil Diubah');
        } else {
            $hasil = array('status' => 0, 'message' => 'Profil Perusahaan Gagal Diubah');
        }

        return $hasil;
    }
}
