<?php

namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;
use App\Http\Helpers\BaseHelper;
use App\Http\Repository\MainRepository;

class CatatanLapRepository
{
    public $database = 'pgsql_silaras';
    public $table = 'silaras.t_880038300_catatan_lap_keuangan';
    public $prirmay_key = 't_id';

    //=============================================== GETDATA ===============================================

    public function GetData($report_month, $report_year)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->select(
                DB::raw('
                t_id, report_month, report_year, note
                ')
            )
            // ->join('m_component', 'm_component.component_code', '=', 't_000038300_profil_perusahaan.component_code')
            ->where('t_880038300_catatan_lap_keuangan.report_month', $report_month)
            ->where('t_880038300_catatan_lap_keuangan.report_year', $report_year)
            ->first();

        return $data;
    }

    public function AddReport($data)
    {
        date_default_timezone_set("Asia/Jakarta");
        $proses = DB::connection($this->database)->table($this->table)->insert(
            array(
                'note' => $data['note'],
                'report_month' => $data['report_month'],
                'report_year' => $data['report_year'],
                'created_by' => $data['nama_user'],
                'created_on' => date('Y-m-d H:i:s')
            )
        );

        if ($proses) {
            $hasil = array('status' => 1, 'message' => 'Laporan Laba Rugi Berhasil Disimpan');
        } else {
            $hasil = array('status' => 0, 'message' => 'Laporan Laba Rugi Gagal Disimpan');
        }

        return $hasil;
    }

    public function GetNumRow($data)
    {
        $data = DB::connection($this->database)
            ->table($this->table)
            ->select('*')
            // ->join('m_component', 'm_component.component_code', '=', 't_000038300_profil_perusahaan.component_code')
            ->where('t_880038300_catatan_lap_keuangan.report_month', $data['report_month'])
            ->where('t_880038300_catatan_lap_keuangan.report_year', $data['report_year'])
            ->count();

        return $data;
    }
}
