<?php namespace App\Http\Controllers;
use Validator;
use Storage;
use ZipArchive;
use DOMDocument;
use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\BaseHelper;
use App\Http\Repository\WelcomeRepository;
use App\Http\Repository\UserRepository;
use App\Http\Repository\MainRepository;
use App\Http\Repository\AturanRepository;
use Illuminate\Support\Facades\Hash;
use Elasticsearch\ClientBuilder;

class WelcomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->repository = new WelcomeRepository();
        $this->url = '/';

        if(!empty($_SERVER['REDIRECT_URL'])){
            $data = $_SERVER['REDIRECT_URL'];   
            $url = explode('/', $data);
            $this->locate = $url[2];
        }else{
            $this->locate = 'id';
        }
        App::setLocale($this->locate);

        // dd($this->)
    }

    public function index(Request $request)
    {
        // dd($data);
        $data = array();
        return view('welcome.login', $data);
    }

    public function change_langguage(Request $request)
    {
        // dd('change', $request->all());
        $change_langguage = $this->repository->UpdateLangguage($request->value);
        return Redirect::to($this->url);
            
    }

    public function logout(Request $request)
    {
        // dd('asdf');
        // $update_logout = $this->repository->UpdateLogout();
        return Redirect::to('/');
            
    }

    public function welcome_proses(Request $request){
        // dd('proses', $this->locate);
        $rules = array(
            'username' => 'required|min:3|max:100',
            'password' => 'required|min:3|max:20'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to($this->url)->withErrors($validator);
        }else{
            $username = trim($request->input('username'));
            $encryptedPassword = encrypt($request->password);
            $user = $this->repository->GetUserData($username);
            // dd($user);
            if(!empty($user)){
                $menu = $this->repository->GetMenu($username);

                $pass = md5($request->password);
                // dd($pass, $request->password, $user->password);
                if($user->status_user == 1){
                    if($user->password == $pass){
                        $request->session()->put('user', $user);
                        $data_log = array(
                            'current_url' => $_SERVER['REQUEST_URI'],
                            'event' => 'Login '.$user->username,
                            'description' => json_encode($user),
                            'created_by' => $user->id_user,
                            'created_name' => $user->first_name.' '.$user->last_name,
                            'ip_address' => $_SERVER['REMOTE_ADDR'] 
                        );
                        $add_log = $this->repository->AddLogActivity($data_log);
                        return Redirect::to($this->locate.'/dashboard');
                    }else{
                        Session::flash('error_message', 'Mohon maaf, password Anda salah!');
                        return Redirect::to($this->url);
                    
                    }
                }else{
                    Session::flash('error_message', 'Mohon maaf, user tidak aktif!');
                    return Redirect::to($this->url);
                }
            }else{
                Session::flash('error_message', 'Mohon maaf, username tidak ditemukan!');
                return Redirect::to($this->url);
            }
        }
    }

}

?>