<?php

namespace App\Http\Controllers;

use Validator;
use App;
use DataTables;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Repository\MainRepository;
use App\Http\Repository\RincianBankRepository;

class RincianBankController extends Controller
{

    public function __construct(Request $request)
    {
        $this->repository = new RincianBankRepository();
        $this->main_repository = new MainRepository();

        $data = $_SERVER['REDIRECT_URL'];   
        $url = explode('/', $data);
        $this->locate = $url[2];

        $this->url = '/'.basename($_SERVER['REQUEST_URI']);
        $this->redirect = '/rincian_bank';
        $this->event = 'Rincian Bank ';
    }

    public function rincian_bank_index(Request $request)
    {
        App::setLocale($this->locate);
        $user = $request->session()->get('user');
        if(empty($user)){
            Session::flash('error_message', 'Silahkan Login Terlebih Dahulu!');
            return Redirect::to('/');
        }
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
        );

        //PUBLIC
        $data['menu'] = $this->main_repository->GetMenu($user->username);
        $data['m_notif'] = $this->main_repository->GetNotif($param);
        $data['user'] = $user;

        //KEPERLUAN BLADE
        $data['bulan'] = $this->main_repository->GetParamTitleType('APPLICATION_PARAM', 'IS_MONTH');
        $data['locate']  = $this->locate;

        $getmax = $this->repository->GetMax();
        if($request->search_report_year){
            $max_year = $request->search_report_year;
        }else{
            $max_year = $getmax['max_year'];
        }
        $data['max_year'] = $max_year;

        if($request->search_report_month){
            $max_month = $request->search_report_month;
        }else{
            $max_month = $getmax['max_month'];
        }
        $data['max_month'] = $max_month;

        // dd($data);
        return view('profil.rincian_bank', $data);
    }

    public function rincian_bank_list(Request $request)
    {
        // dd('asdf',$request->all());
        $request_param = explode('#', $request->param);
        $user = $request->session()->get('user');
        if(count($request_param) != 2){
            Session::flash('error_message', 'There is something wrong with parameter list Rincian E-Wallet');
            return Redirect::to($redirect);
        }
        // dd($request_param);
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'search_report_year' => $request_param[0],
            'search_report_month' => $request_param[1],
        );
        $data_list = $this->repository->DataTable($param);
        // dd($data_list);
        // return Datatables::of($data_list)->make();
        echo json_encode($data_list);
    }

    //=============================================== GETDATA ===============================================
    public function rincian_bank_getdata(Request $request){
        $data = $this->repository->FirstDataId($request->id);
        return response()->json($data);
    }
    //=============================================== POSTDATA ===============================================

    public function rincian_bank_edit(Request $request)
    {   
        // dd($request->all());
        $redirect = '/'.$request->locate.$this->redirect;
        $user = $request->session()->get('user');

        $data = array(
            'id' => $request->id,
            'component_code' => $request->component_code,
            'component_desc' => $request->component_desc,
            'general_information' => $request->general_information,
            'id_user' => $user->id_user,
            'nama_user' => $user->first_name.' '.$user->last_name,
        );

        $before_log = $this->repository->FirstDataId($request->id);
        $data_log = array(
            'current_url' => $_SERVER['REQUEST_URI'],
            'event' => 'Edit '.$this->event,
            'description' => json_encode($data),
            'created_by' => $user->id_user,
            'created_name' => $user->first_name.' '.$user->last_name,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'before_log' => json_encode($before_log),
            'after_log' => json_encode($data),
        );
        $add = $this->repository->Edit($data);
        $add_log = $this->main_repository->AddLogActivity($data_log);
        // dd($add);
        return $add;

    }

    public function rincian_bank_action(Request $request)
    {
        // dd($this->redirect, $request->all());
        $action_flag = $request->action_flag;
        $user = $request->session()->get('user');

        $data = array(
            'id' => $request->id,
            'report_year' => $request->report_year,
            'report_month' => $request->report_month,
            'component_code' => $request->component_code,
            'component_desc' => $request->component_desc,
            'general_information' => $request->general_information,
            'id_user' => $user->id_user,
            'nama_user' => $user->first_name.' '.$user->last_name,
        );

        if($action_flag == 'A'){
            $ket_log = 'Add ';
            $action = $this->repository->Add($data);
        }else{
            $ket_log = 'Edit ';
            $action = $this->repository->Edit($data);
        }

        $before_log = $this->repository->GetData();
        foreach ($before_log as $key => $value) {
            $general_information_before[] = $value->t_id.':'.$value->general_information;
        }
        // dd($general_information_before);
        $data_log = array(
            'current_url' => $_SERVER['REQUEST_URI'],
            'event' =>  $ket_log.$this->event,
            'description' => json_encode($data),
            'created_by' => $user->id_user,
            'created_name' => $user->first_name.' '.$user->last_name,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'before_log' => json_encode($general_information_before),
            'after_log' => json_encode($data),
        );
        $add_log = $this->main_repository->AddLogActivity($data_log);
        
        if($action['status'] == 1){
            $result[] = array("status" => $action['status'], "message" => 'Item has been '.$ket_log.'ed');  
            return response()->json($result[0]);
        }else{
            $result[] = array("status" => $action['status'], "message" => 'Item has been '.$ket_log.'ed');  
            return response()->json($result[0]);
        }
    }

    public function rincian_bank_delete(Request $request)
    {
        // dd($request->all());
        $redirect = env('APP_URLAPP').'/'.$this->locate.'/'.$this->redirect;
        $user = $request->session()->get('user');
        $data = array(
            'id' => $request->id,
            'status' => 'f',
            'id_user' => $user->id_user,
            'nama_user' => $user->first_name.' '.$user->last_name,
        );
        // $data_del = $this->repository->GetDataId($request->id);
        $data_log = array(
            'current_url' => $_SERVER['REQUEST_URI'],
            'event' => 'Delete '.$this->event,
            'description' => json_encode($data),
            'created_by' => $user->id_user,
            'created_name' => $user->first_name.' '.$user->last_name,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'before_log' => '',
            'after_log' => '',
        );
        $add = $this->repository->Deleted($data);
        $add_log = $this->main_repository->AddLogActivity($data_log);
        return $add;

    }

}
