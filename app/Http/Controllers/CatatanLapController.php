<?php

namespace App\Http\Controllers;

use Mpdf\Mpdf;
// use Validator;
// use Excel;
// use App;
// use Lang;
// use Illuminate\Foundation\Bus\DispatchesJobs;
// use Illuminate\Routing\Controller as BaseController;
// use Illuminate\Foundation\Validation\ValidatesRequests;
// use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Storage;
// use Illuminate\Support\Facades\Input;
// use Illuminate\Support\Facades\URL;
use App\Http\Repository\CatatanLapRepository;
use App\Http\Repository\MainRepository;

class CatatanLapController extends Controller
{

    public function __construct(Request $request)
    {
        $this->repository = new CatatanLapRepository();
        $this->main_repository = new MainRepository();

        $data = $_SERVER['REDIRECT_URL'];
        $url = explode('/', $data);
        $this->locate = $url[2];

        $this->url = 'dashboard';
    }

    public function catatan_laporan_index(Request $request)
    {
        $user = $request->session()->get('user');
        if ($user == null) {
            Session::flash('error_message', 'Silahkan Login Terlebih Dahulu');
            return Redirect::to('/');
        }
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level
        );

        $data['menu'] = $this->main_repository->GetMenu($user->username);
        $data['m_notif'] = $this->main_repository->GetNotif($param);
        $data['user'] = $user;
        $data['locate']  = $this->locate;
        $data['max_year'] = ($request->report_year) ? $request->report_year : date('Y');
        $data['max_month'] = ($request->report_month) ? $request->report_month : date('n');
        $data['bulan'] = $this->main_repository->GetParamTitleType('APPLICATION_PARAM', 'IS_MONTH');

        if ($data['max_month'] == '1') {
            $month_now = 'Januari';
        } elseif ($data['max_month'] == '2') {
            $month_now = 'Februari';
        } elseif ($data['max_month'] == '3') {
            $month_now = 'Maret';
        } elseif ($data['max_month'] == '4') {
            $month_now = 'April';
        } elseif ($data['max_month'] == '5') {
            $month_now = 'Mei';
        } elseif ($data['max_month'] == '6') {
            $month_now = 'Juni';
        } elseif ($data['max_month'] == '7') {
            $month_now = 'Juli';
        } elseif ($data['max_month'] == '8') {
            $month_now = 'Agustus';
        } elseif ($data['max_month'] == '9') {
            $month_now = 'September';
        } elseif ($data['max_month'] == '10') {
            $month_now = 'Oktober';
        } elseif ($data['max_month'] == '11') {
            $month_now = 'November';
        } elseif ($data['max_month'] == '12') {
            $month_now = 'Desember';
        }
        $data['month_now'] = $month_now;
        return view('kinerja.catatan_lap', $data);
    }

    public function get_catatan_laporan(Request $request)
    {
        // dd($request->all());
        $data_list = $this->repository->GetData($request->report_month, $request->report_year);
        // dd($data_list);
        return response()->json(['response' => $data_list], 200);
    }

    public function send_catatan_laporan(Request $request)
    {
        // dd($request->all());
        $numRow = $this->repository->GetNumRow($request);
        if ($numRow > 0) {
            return response()->json(['response' => 'failedNumrow'], 200);
        } else {
            $action = $this->repository->AddReport($request);
            if ($action['status'] == 1) {
                return response()->json(['response' => 'success', 'report_month' => $request->report_month, 'report_year' => $request->report_year], 200);
            } else {
                return response()->json(['response' => 'failed'], 200);
            }
        }
    }

    public function export_to_pdf(Request $request)
    {
        // dd($request->all());
        $data_list = $this->repository->GetData($request->report_month, $request->report_year);
        // dd($data_list->note);
        $mpdf = new Mpdf(['debug' => FALSE, 'mode' => 'utf-8', 'format' => 'A4-P']);
        $mpdf->WriteHTML($data_list->note);
        $mpdf->Output('surat_pernyataan_direksi.pdf', 'I');
        exit;
    }
}
