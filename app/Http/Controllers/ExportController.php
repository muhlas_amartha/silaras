<?php

namespace App\Http\Controllers;

use Validator;
use Excel;
use ZipArchive;
use File;
use App;
use DataTables;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Repository\MainRepository;
use App\Http\Repository\ProfilRepository;
use App\Http\Repository\RincianBankRepository;
use App\Http\Repository\RincianPaymentRepository;
use App\Http\Repository\RincianEWalletRepository;
use App\Http\Repository\LapLabaRepository;
use App\Http\Repository\LapModalRepository;
use App\Http\Repository\LapArusRepository;
use App\Http\Repository\LapNeracaRepository;
use App\Http\Repository\LapInclusivityRepository;
use App\Http\Repository\LapTransactionValueRepository;
use App\Http\Repository\LapLoanQualityRepository;
use App\Http\Repository\LapDetailOutstandingPenyelenggaraRepository;
use App\Http\Repository\LapDetailKualitasPinjamanRepository;
use App\Http\Repository\LapPengaduanPenggunaRepository;

use App\Exports\ProfilExport;
use App\Exports\RincianBankExport;
use App\Exports\RincianPaymentExport;
use App\Exports\RincianEWalletExport;
use App\Exports\RincianLapNeracaExport;
use App\Exports\RincianLapLabaExport;
use App\Exports\RincianLapArusExport;
use App\Exports\RincianLapModalExport;
use App\Exports\RincianLapInclusivityExport;
use App\Exports\RincianLapTransactionValueExport;
use App\Exports\RincianLapLoanQualityExport;
use App\Exports\RincianLapDetailOutstandingPenyelenggaraExport;
use App\Exports\RincianLapDetailKualitasPinjamanExport;
use App\Exports\RincianLapPengaduanPenggunaExport;

class ExportController extends Controller
{

    public function __construct(Request $request)
    {
        $this->main_repository = new MainRepository();
        $this->repository_profil = new ProfilRepository();
        $this->repository_rbank = new RincianBankRepository();
        $this->repository_rpayment = new RincianPaymentRepository();
        $this->repository_ewallet = new RincianEWalletRepository();
        $this->repository_lneraca = new LapNeracaRepository();
        $this->repository_llaba = new LapLabaRepository();
        $this->repository_lmodal = new LapModalRepository();
        $this->repository_larus = new LapArusRepository();
        $this->repository_linclusivity = new LapInclusivityRepository();
        $this->repository_ltransaction_value = new LapTransactionValueRepository();
        $this->repository_lloan_quality = new LapLoanQualityRepository();
        $this->repository_ldetail_outstanding_penyelenggara = new LapDetailOutstandingPenyelenggaraRepository();
        $this->repository_ldetail_kualitas_pinjaman = new LapDetailKualitasPinjamanRepository();
        $this->repository_lpengaduan_pengguna = new LapPengaduanPenggunaRepository();
    }

    //=============================================== GETDATA ===============================================
    public function export_profil(Request $request)
    {
        $param = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param) {
            $filename = $param->param_value . '-' . date('Y-m-d') . '-' . '000038300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            return Redirect::to($redirect);
        }

        $param = array();
        $report_name = "profil";
        $header = $this->repository_lneraca->GetHeader($report_name);
        $data = $this->repository_profil->Datatable($param);
        switch ($request->type) {
            case 'xlsx':
                return Excel::download(new ProfilExport($data, $header), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new ProfilExport($data, $header), $filename . '.txt');
                break;
            case 'pdf':
                // return (new UserExport($data_list))->download($filename.'.pdf', \Maatwebsite\Excel\Excel::MPDF);
                require_once __DIR__ . '/vendor/autoload.php';

                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML('<h1>Hello world!</h1>');
                $mpdf->Output();
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_rincian_bank(Request $request)
    {
        // dd($request->all());
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '000038300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $request_param = explode('#', $request->param);
        if (count($request_param) != 2) {
            Session::flash('error_message', 'There is something wrong with parameter export Rincian Payment');
            return Redirect::to($redirect);
        }
        // dd($request_param);
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'search_report_year' => $request_param[0],
            'search_report_month' => $request_param[1],
        );
        $data = $this->repository_rbank->Datatable($param);
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianBankExport($data), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianBankExport($data), $filename . '.txt');
                break;
            case 'pdf':
                // return (new UserExport($data_list))->download($filename.'.pdf', \Maatwebsite\Excel\Excel::MPDF);
                require_once __DIR__ . '/vendor/autoload.php';

                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML('<h1>Hello world!</h1>');
                $mpdf->Output();
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_rincian_payment(Request $request)
    {
        // dd($request->all());
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '000038300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $request_param = explode('#', $request->param);
        if (count($request_param) != 2) {
            Session::flash('error_message', 'There is something wrong with parameter export Rincian Payment');
            return Redirect::to($redirect);
        }
        // dd($request_param);
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'search_report_year' => $request_param[0],
            'search_report_month' => $request_param[1],
        );
        $data = $this->repository_rpayment->Datatable($param);
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianPaymentExport($data), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianPaymentExport($data), $filename . '.txt');
                break;
            case 'pdf':
                // return (new UserExport($data_list))->download($filename.'.pdf', \Maatwebsite\Excel\Excel::MPDF);
                require_once __DIR__ . '/vendor/autoload.php';

                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML('<h1>Hello world!</h1>');
                $mpdf->Output();
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_rincian_ewallet(Request $request)
    {
        // dd($request->all());
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '000038300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $request_param = explode('#', $request->param);
        if (count($request_param) != 2) {
            Session::flash('error_message', 'There is something wrong with parameter export Rincian E-Wallet');
            return Redirect::to($redirect);
        }
        // dd($request_param);
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'search_report_year' => $request_param[0],
            'search_report_month' => $request_param[1],
        );
        $data = $this->repository_ewallet->Datatable($param);
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianEWalletExport($data), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianEWalletExport($data), $filename . '.txt');
                break;
            case 'pdf':
                // return (new UserExport($data_list))->download($filename.'.pdf', \Maatwebsite\Excel\Excel::MPDF);
                require_once __DIR__ . '/vendor/autoload.php';

                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML('<h1>Hello world!</h1>');
                $mpdf->Output();
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_lap_neraca(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '000038300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "lap_neraca";
        $data = $this->repository_lneraca->DatatableExport($param);
        $header = $this->repository_lneraca->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '000038300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapNeracaExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapNeracaExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_lap_laba(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '000038300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "lap_laba";
        $data = $this->repository_llaba->DatatableExport($param);
        $header = $this->repository_llaba->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '000038300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapLabaExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapLabaExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_lap_modal(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '000038300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "lap_modal";
        $data = $this->repository_lmodal->DatatableExport($param);
        $header = $this->repository_lmodal->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '000038300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapModalExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapModalExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_lap_arus(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '000038300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "lap_arus";
        $data = $this->repository_larus->DatatableExport($param);
        $header = $this->repository_larus->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '000038300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapArusExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapArusExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_inclusivity(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '000038300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "inclusivity";
        if ($request->tipe == 'tahunan') {
            $data = $this->repository_linclusivity->DatatableExportYearly($param);
        } else {
            $data = $this->repository_linclusivity->DatatableExport($param);
        }

        // dd($data);
        $header = $this->repository_linclusivity->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '690138300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapInclusivityExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapInclusivityExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_transaction_value(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '690238300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "transaction_value";
        if ($request->tipe == 'tahunan') {
            $data = $this->repository_linclusivity->DatatableExportYearly($param);
        } else {
            $data = $this->repository_linclusivity->DatatableExport($param);
        }
        // dd($data);
        $header = $this->repository_ltransaction_value->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '690238300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapTransactionValueExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapTransactionValueExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_loan_quality(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '690338300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "loan_quality";
        if ($request->tipe == 'tahunan') {
            $data = $this->repository_linclusivity->DatatableExportYearly($param);
        } else {
            $data = $this->repository_linclusivity->DatatableExport($param);
        }
        // dd($data);
        $header = $this->repository_lloan_quality->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '690338300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapLoanQualityExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapLoanQualityExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_detail_outstanding_penyelenggara(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '290738300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "detail_outstanding_penyelenggara";
        $data = $this->repository_ldetail_outstanding_penyelenggara->DatatableExport($param);
        // dd($data);
        $header = $this->repository_ldetail_outstanding_penyelenggara->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '290738300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapDetailOutstandingPenyelenggaraExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapDetailOutstandingPenyelenggaraExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_detail_kualitas_pinjaman(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '690838300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "detail_kualitas_pinjaman";
        $data = $this->repository_ldetail_kualitas_pinjaman->DatatableExport($param);
        // dd($data);
        $header = $this->repository_ldetail_kualitas_pinjaman->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '690838300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapDetailKualitasPinjamanExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapDetailKualitasPinjamanExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_pengaduan_pengguna(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');
        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '691138300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $report_name = "pengaduan_pengguna";
        $data = $this->repository_lpengaduan_pengguna->DatatableExport($param);
        // dd($data);
        $header = $this->repository_lpengaduan_pengguna->GetHeader($report_name);

        $tipe = $request->tipe;

        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '691138300';
        switch ($request->type) {
            case 'xls':
                return Excel::download(new RincianLapPengaduanPenggunaExport($data, $header, $tipe), $filename . '.xlsx');
                break;
            case 'csv':
                return Excel::download(new RincianLapPengaduanPenggunaExport($data, $header, $tipe), $filename . '.txt');
                break;
            default:
                # code...
                break;
        }
        return response()->json($data);
    }

    public function export_all_report(Request $request)
    {
        $param_sandi = $this->main_repository->FirstParam('APPLICATION_PARAM', 'SANDI_LJK');

        if ($param_sandi) {
            $filename = $param_sandi->param_value . '-' . date('Y-m-d') . '-' . '691138300';
        } else {
            Session::flash('error_message', 'Parameter APPLICATION_PARAM|SANDI_LJK Not Found');
            $redirect = '/' . $request->locate . $this->redirect;
            return Redirect::to($redirect);
        }

        $user = $request->session()->get('user');
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $tipe = '';

        $report_name = "lap_neraca";
        $data = $this->repository_lneraca->DatatableExport($param);
        $header = $this->repository_lneraca->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '110038300';
        Excel::store(new RincianLapNeracaExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $report_name = "lap_laba";
        $data = $this->repository_llaba->DatatableExport($param);
        $header = $this->repository_llaba->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '120038300';
        Excel::store(new RincianLapLabaExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $report_name = "lap_modal";
        $data = $this->repository_lmodal->DatatableExport($param);
        $header = $this->repository_lmodal->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '120138300';
        Excel::store(new RincianLapModalExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $report_name = "lap_arus";
        $data = $this->repository_larus->DatatableExport($param);
        $header = $this->repository_larus->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '130038300';
        Excel::store(new RincianLapArusExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $report_name = "loan_quality";
        $data = $this->repository_linclusivity->DatatableExport($param);
        $header = $this->repository_lloan_quality->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '690338300';
        Excel::store(new RincianLapLoanQualityExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $report_name = "transaction_value";
        $data = $this->repository_linclusivity->DatatableExport($param);
        $header = $this->repository_ltransaction_value->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '690238300';
        Excel::store(new RincianLapTransactionValueExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $report_name = "inclusivity";
        $data = $this->repository_linclusivity->DatatableExport($param);
        $header = $this->repository_linclusivity->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '690138300';
        Excel::store(new RincianLapInclusivityExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $report_name = "detail_outstanding_penyelenggara";
        $data = $this->repository_ldetail_outstanding_penyelenggara->DatatableExport($param);
        $header = $this->repository_ldetail_outstanding_penyelenggara->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '290738300';
        Excel::store(new RincianLapDetailOutstandingPenyelenggaraExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $report_name = "detail_kualitas_pinjaman";
        $data = $this->repository_ldetail_kualitas_pinjaman->DatatableExport($param);
        $header = $this->repository_ldetail_kualitas_pinjaman->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '690838300';
        Excel::store(new RincianLapDetailKualitasPinjamanExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $report_name = "pengaduan_pengguna";
        $data = $this->repository_lpengaduan_pengguna->DatatableExport($param);
        $header = $this->repository_lpengaduan_pengguna->GetHeader($report_name);
        $filename = $header->report_filename . '-' . date('Y-m-d') . '-' . '691138300';
        Excel::store(new RincianLapPengaduanPenggunaExport($data, $header, $tipe), $filename . '.txt', 'monthly');

        $zip = new ZipArchive;
        $fileName = 'Monthly-' . date('Y-m-d') . '.zip';

        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE) {
            $files = File::files(public_path('report/Monthly-' . date('Y-m-d')));

            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }

            $zip->close();
        }

        return response()->download(public_path($fileName));
    }

    //=============================================== POSTDATA ===============================================


}
