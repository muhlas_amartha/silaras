<?php

namespace App\Http\Controllers;

use Validator;
use App;
use DataTables;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Repository\MainRepository;
use App\Http\Repository\ProfilRepository;

class ProfilController extends Controller
{

    public function __construct(Request $request)
    {
        $this->repository = new ProfilRepository();
        $this->main_repository = new MainRepository();

        $data = $_SERVER['REDIRECT_URL'];   
        $url = explode('/', $data);
        $this->locate = $url[2];

        $this->url = '/'.basename($_SERVER['REQUEST_URI']);
        $this->redirect = '/profil';
        $this->event = 'Profil Perusahaan ';
    }

    public function profil_index(Request $request)
    {
        App::setLocale($this->locate);
        $user = $request->session()->get('user');
        if(empty($user)){
            Session::flash('error_message', 'Silahkan Login Terlebih Dahulu!');
            return Redirect::to('/');
        }
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level
        );

        //PUBLIC
        $data['menu'] = $this->main_repository->GetMenu($user->username);
        $data['m_notif'] = $this->main_repository->GetNotif($param);
        $data['user'] = $user;

        //KEPERLUAN BLADE
        $data['data']  = $this->repository->DataTable($param);
        $data['locate']  = $this->locate;
        $data['provinsi'] = $this->repository->GetProvinsi();
        // dd($data['data']);

        $getdata = $this->repository->FirstDataComponentCode('040000000000');//code_mobile_platfrom
        if($getdata){
            $data['code_mobile_platfrom'] = $getdata->general_information;
        }else{
            Session::flash('error_message', 'Component Code 040000000000 Not Found at Menu Profil Perusahaan');
            return Redirect::to('id/dashboard');
        }

        $getdata_data2 = $this->repository->FirstDataComponentCode('180000000000');//DATI II
        if($getdata_data2){
            $data['kabkota'] = $this->repository->GetKabKotaCityCode($getdata_data2->general_information);
            // $data['code_dati2'] = $getdata_data2;
        }else{
            Session::flash('error_message', 'Component Code 180000000000 Not Found at Menu Profil Perusahaan');
            return Redirect::to('id/dashboard');
        }

        return view('profil.profil', $data);
    }

    //=============================================== GETDATA ===============================================
    public function profil_getvalidator(Request $request){
        $data = $this->repository->FirstDataValidator($request->id);
        return response()->json($data);
    }

    public function profil_getkab(Request $request){
        $data = $this->repository->GetKabKota($request->provinsi_code);
        return response()->json($data);
    }
    //=============================================== POSTDATA ===============================================
    public function profil_edit(Request $request)
    {   
        // dd($request->all());
        $redirect = '/'.$request->locate.$this->redirect;
        $user = $request->session()->get('user');

        foreach ($request->general_information as $t_id => $value) {
            $getdata = $this->repository->FirstDataValidator($t_id);
            if($getdata->component_validator == 'M'){
                if($value == null || $value == ''){
                    Session::flash('error_message', 'Uraian '.$getdata->component_desc.' is Mandatory');
                    return Redirect::to($redirect);
                }
            }else if($getdata->component_validator == 'C'){
                if($getdata->component_code == '050000000000'){//nama_mobile_platfrom
                    $code_mobile_platfrom = '040000000000';
                    $getdata_component = $this->repository->FirstDataComponentCode($code_mobile_platfrom);
                    // dd($getdata_component, $value);
                    if($getdata_component){
                        if($value == null || $value == ''){//mobile platrfrom ada isinya tapi nama mobile platfromnya kosong
                            Session::flash('error_message', 'Jika Mobile Platform ada, maka Nama Mobile Platform Wajib Diisi');
                            return Redirect::to($redirect);
                        }
                    }else{
                        Session::flash('error_message', 'Component Code 040000000000 Not Found');
                        return Redirect::to($redirect);
                    }
                }
            }
        }

        $data = array(
            'id_user' => $user->first_name.' '.$user->last_name,
            'general_information' => $request->general_information
        );

        $before_log = $this->repository->GetData();
        foreach ($before_log as $key => $value) {
            $general_information_before[] = $value->general_information;
        }
        // dd($general_information_before);
        $data_log = array(
            'current_url' => $_SERVER['REQUEST_URI'],
            'event' => 'Edit '.$this->event,
            'description' => json_encode($data),
            'created_by' => $user->id_user,
            'created_name' => $user->first_name.' '.$user->last_name,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'before_log' => json_encode($general_information_before),
            'after_log' => json_encode($request->general_information),
        );
        // dd($data_log);
        $add = $this->repository->Edit($data);
        $add_log = $this->main_repository->AddLogActivity($data_log);
        
        if($add['status'] == 1){
            Session::flash('message', $add['message']);
            return Redirect::to($redirect);
        }else{
            Session::flash('error_message', $add['message']);
            return Redirect::to($redirect);
        }

    }

}
