<?php

namespace App\Http\Controllers;

use Validator;
use Excel;
use App;
use PDF;
use DataTables;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Repository\MainRepository;
use App\Http\Repository\UserRepository;
use App\Exports\UserExport;

class UserController extends Controller
{

    public function __construct(Request $request)
    {
        $this->repository = new UserRepository();
        $this->main_repository = new MainRepository();

        $data = $_SERVER['REDIRECT_URL'];   
        $url = explode('/', $data);
        $this->locate = $url[2];
        // dd($this->locate);

        $this->url = '/'.basename($_SERVER['REQUEST_URI']);
        $this->redirect = $this->locate.'/user';
        $this->event = 'User';
    }

    public function user_index(Request $request)
    {
        App::setLocale($this->locate);
        $user = $request->session()->get('user');
        if(empty($user)){
            Session::flash('error_message', 'Silahkan Login Terlebih Dahulu!');
            return Redirect::to('/');
        }
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level
        );

        //PUBLIC
        $data['menu'] = $this->main_repository->GetMenu($user->username);
        $data['m_notif'] = $this->main_repository->GetNotif($param);
        $data['user'] = $user;

        //KEPERLUAN BLADE
        $data['m_divisi']  = $this->main_repository->GetMDivisi();
        $data['sec_role']  = $this->main_repository->GetSecRole();
        $data['locate']  = $this->locate;

        return view('administration.user', $data);
    }

    public function user_list(Request $request)
    {
        $param = array(
            'id_user' => $request->id_user,
        );
        $data_list = $this->repository->DataTable($param);
        // dd($data_list);
        return Datatables::of($data_list)->make();
    }

    //=============================================== GETDATA ===============================================
    public function user_getdata(Request $request){
        $data = $this->repository->GetDataId($request->id);
        return response()->json($data);
    }

    public function user_export(Request $request){
        // dd($request->all());
        $filename = 'User-'.date('YmdHis');
        $param = array(
            'id_user' => $request->id_user,
        );
        $data_list = $this->repository->DataTable($param);
        switch ($request->type) {
            case 'xls':
                return Excel::download(new UserExport($data_list), $filename.'.xlsx');
            break;
            case 'csv':
                return Excel::download(new UserExport($data_list), $filename.'.csv');
            break;
            case 'pdf':
                // return (new UserExport($data_list))->download($filename.'.pdf', \Maatwebsite\Excel\Excel::MPDF);
                require_once __DIR__ . '/vendor/autoload.php';

                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML('<h1>Hello world!</h1>');
                $mpdf->Output();
            break;
            default:
                # code...
                break;
        }
    }

    //=============================================== POSTDATA ===============================================
    public function user_add(Request $request)
    {
    	// dd($request->all());
        $user = $request->session()->get('user');
        $rules = array(
            'first_name' => 'required',
            'username' => 'required',
            'email' => 'required',
            'id_divisi' => 'required',
            'id_role' => 'required',
        );

        $messages = array(
            'required' => ':attribute cannot be empty.',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('error_message', 'Data is Not Compeleted');
            return Redirect::to($this->redirect);
        } else {
            if(file_exists($_FILES['file_upload']['tmp_name'])) {
                $file = $request->file_upload;
                $ext = $file->getClientOriginalExtension();
                $get_ext = $this->main_repository->GetMParamExt($ext);
                if(!empty($get_ext)){
                    $destination_path = 'upload/User/';
                    $file_name_ori = $file->getClientOriginalName();
                    $file_name = date('ymdhis').'_'.$file->getClientOriginalName();
                    $file_path = $destination_path.$file_name;

                    if (!file_exists($destination_path)) {
                        mkdir($destination_path, 0777, true);
                    }
                    move_uploaded_file($_FILES['file_upload']['tmp_name'], $file_path);
                }else{
                    Session::flash('error_message', 'Format file tidak mendukung, silahkan upload menggunakan file lain');
                    return Redirect::to($this->redirect);
                }
            }else{
                $file_name = null;
                $file_name_ori = null;
                $file_path = null;
            }

            $chars_low = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 3);
            $chars_up = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 3);
            $chars_number = substr(str_shuffle("0123456789"), 0, 1);
            $chars_simbol = substr(str_shuffle("!@#$^&*()_-=+;:,.?"), 0, 1);
            $password = $chars_low.$chars_up.$chars_number.$chars_simbol;
            // dd($password);
            $data_email = array(
                'name' => $request->first_name.' '.$request->last_name,
                'username' => $request->username,
                'password' => $password,
            );
            // dd($data_email);
            $email = $this->main_repository->GetMParamFromEmail();
            $email_from = $email->param_nama;
            $to_email = $request->email;
            $to_name = $request->first_name.' '.$request->last_name;
            if($this->locate == 'id'){
                $mail_blade = 'mail.mail_userbaru';
            }else{
                $mail_blade = 'mail.mail_newuser';
            }
            if(!empty($to_email)){
                Mail::send($mail_blade, $data_email, function($message) use ($to_name, $to_email, $email_from) {
                    $message->to($to_email, $to_name)
                            ->subject('Permintaan Username dan Password Aplikasi ICOM');
                    $message->from($email_from,'Permintaan Username dan Password Aplikasi ICOM');
                });
            }

        	$data = array(
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'username' => $request->username,
                'password' => md5($password),
                // 'password' => $password,
                'email' => $request->email,
                'nik' => $request->nik,
                'phone_number' => $request->phone_number,
                'id_divisi' => $request->id_divisi,
                'id_role' => $request->id_role,
                'file_name' => $file_name,
                'file_name_ori' => $file_name_ori,
                'file_path' => $file_path,
                'status_user' => 1,
                'id_user' => $user->id_user,
                'nama_user' => $user->first_name.' '.$user->last_name,
            );

            $data_log = array(
                'current_url' => $_SERVER['REQUEST_URI'],
                'event' => 'Add '.$this->event,
                'description' => json_encode($data),
                'created_by' => $user->id_user,
                'created_name' => $user->first_name.' '.$user->last_name,
                'ip_address' => $_SERVER['REMOTE_ADDR'] 
            );
            $add = $this->repository->Add($data);
            $add_log = $this->main_repository->AddLogActivity($data_log);
            
            if($add['status'] == 1){
                Session::flash('message', $add['message']);
                return Redirect::to($this->redirect);
            }else{
                Session::flash('error_message', $add['message']);
                return Redirect::to($this->redirect);
            }
        }
    }

    public function user_action(Request $request)
    {
        // dd($this->redirect, $request->all());
        $action_flag = $request->action_flag;
        $user = $request->session()->get('user');

        if($action_flag == 'A'){
            $password = md5($request->password);
        }else if($action_flag == 'E'){
            $get_pass = $this->repository->GetPassword($request->id);
            $password = $get_pass->password;
        }

        $data = array(
            'id' => $request->id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'username' => $request->username,
            'password' => $password,
            'email' => $request->email,
            'nik' => $request->nik,
            'phone_number' => $request->phone_number,
            'id_divisi' => $request->id_divisi,
            'id_role' => $request->id_role,
            'status' => 1,
            'id_user' => $user->id_user,
            'nama_user' => $user->first_name.' '.$user->last_name,
        );

        if($action_flag == 'A'){
            $ket_log = 'Add';
            $action = $this->repository->Add($data);
        }else if($action_flag == 'E'){
            $ket_log = 'Edit';
            $action = $this->repository->Edit($data);
        }

        $data_log = array(
            'current_url' => $_SERVER['REQUEST_URI'],
            'event' =>  $ket_log.' '.$this->event,
            'description' => json_encode($data),
            'created_by' => $user->id_user,
            'created_name' => $user->first_name.' '.$user->last_name,
            'ip_address' => $_SERVER['REMOTE_ADDR'] 
        );
        $add_log = $this->main_repository->AddLogActivity($data_log);
        
        if($action['status'] == 1){
            $result[] = array("status" => $action['status'], "message" => 'Item has been '.$ket_log.'ed');  
            return response()->json($result[0]);
        }else{
            $result[] = array("status" => $action['status'], "message" => 'Item has been '.$ket_log.'ed');  
            return response()->json($result[0]);
        }
    }

    public function user_delete(Request $request)
    {
        // dd($request->all());
        $user = $request->session()->get('user');
        $data = array(
            'id' => $request->id,
            'status' => 0,
            'id_user' => $user->id_user,
            'nama_user' => $user->first_name.' '.$user->last_name,
        );
        $data_del = $this->repository->GetDataId($request->id);
        $data_log = array(
            'current_url' => $_SERVER['REQUEST_URI'],
            'event' => 'Delete '.$this->event,
            'description' => json_encode($data_del),
            'created_by' => $user->id_user,
            'created_name' => $user->first_name.' '.$user->last_name,
            'ip_address' => $_SERVER['REMOTE_ADDR'] 
        );
        $add = $this->repository->Deleted($data);
        $add_log = $this->main_repository->AddLogActivity($data_log);
        
        if($add['status'] == 1){
            Session::flash('message', $add['message']);
            return Redirect::to($this->redirect);
        }else{
            Session::flash('error_message', $add['message']);
            return Redirect::to($this->redirect);
        }

    }

    public function user_reset_password(Request $request)
    {
        dd($request->all());
        $user = $request->session()->get('user');
        $rules = array(
            'id' => 'required',
        );

        $messages = array(
            'required' => ':attribute cannot be empty.',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('error_message', 'Data is Not Compeleted');
            return Redirect::to($this->redirect);
        } else {
            $getdata = $this->repository->GetDataId($request->id);
            // dd($getdata);
            $chars_low = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 3);
            $chars_up = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 3);
            $chars_number = substr(str_shuffle("0123456789"), 0, 1);
            $chars_simbol = substr(str_shuffle("!@#$^&*()_-=+;:,.?"), 0, 1);
            $password = $chars_low.$chars_up.$chars_number.$chars_simbol;
            // dd($password);
            $data_email = array(
                'name' => $getdata->first_name.' '.$getdata->last_name,
                'username' => $getdata->username,
                'password' => $password,
            );
            // dd($data_email);
            $email = $this->main_repository->GetMParamFromEmail();
            $email_from = $email->param_nama;
            $to_email = $getdata->email;
            $to_name = $getdata->first_name.' '.$getdata->last_name;
            if($this->locate == 'id'){
                $mail_blade = 'mail.mail_aturpassword';
            }else{
                $mail_blade = 'mail.mail_resetpassword';
            }
            if(!empty($to_email)){
                Mail::send($mail_blade, $data_email, function($message) use ($to_name, $to_email, $email_from) {
                    $message->to($to_email, $to_name)
                            ->subject('Reset Password Aplikasi ICOM');
                    $message->from($email_from,'Reset Password Aplikasi ICOM');
                });
            }

            $data = array(
                'password' => md5($password),
                'id' => $request->id,
                'id_user' => $user->id_user,
                'nama_user' => $user->first_name.' '.$user->last_name,
            );

            $data_log = array(
                'current_url' => $_SERVER['REQUEST_URI'],
                'event' => 'Reset Password '.$this->event,
                'description' => json_encode($data),
                'created_by' => $user->id_user,
                'created_name' => $user->first_name.' '.$user->last_name,
                'ip_address' => $_SERVER['REMOTE_ADDR'] 
            );
            $add = $this->repository->ResetPassword($data);
            $add_log = $this->main_repository->AddLogActivity($data_log);
            
            if($add['status'] == 1){
                Session::flash('message', $add['message']);
                return Redirect::to($this->redirect);
            }else{
                Session::flash('error_message', $add['message']);
                return Redirect::to($this->redirect);
            }
        }
    }

}
