<?php

namespace App\Http\Controllers;

use Validator;
use App;
use DataTables;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Repository\MainRepository;
use App\Http\Repository\LapModalRepository;

class LapModalController extends Controller
{

    public function __construct(Request $request)
    {
        $this->repository = new LapModalRepository();
        $this->main_repository = new MainRepository();

        $data = $_SERVER['REDIRECT_URL'];
        $url = explode('/', $data);
        $this->locate = $url[2];

        $this->url = '/' . basename($_SERVER['REQUEST_URI']);
        $this->redirect = '/lap_modal';
        $this->event = 'Laporan Laba Rugi ';
    }

    public function lap_modal_index(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
        App::setLocale($this->locate);
        $user = $request->session()->get('user');
        if (empty($user)) {
            Session::flash('error_message', 'Silahkan Login Terlebih Dahulu!');
            return Redirect::to('/');
        }
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        //PUBLIC
        $data['menu'] = $this->main_repository->GetMenu($user->username);
        $data['m_notif'] = $this->main_repository->GetNotif($param);
        $data['user'] = $user;

        //KEPERLUAN BLADE
        $data['data']  = $this->repository->DataTable($param);
        // dd($data['data']);
        $data['bulan'] = $this->main_repository->GetParamTitleType('APPLICATION_PARAM', 'IS_MONTH');
        $data['locate']  = $this->locate;
        $data['max_year'] = ($request->report_year) ? $request->report_year : date('Y');
        $data['max_month'] = ($request->report_month) ? $request->report_month : date('n');

        if ($data['max_month'] == '1') {
            $month_now = 'Januari';
        } elseif ($data['max_month'] == '2') {
            $month_now = 'Februari';
        } elseif ($data['max_month'] == '3') {
            $month_now = 'Maret';
        } elseif ($data['max_month'] == '4') {
            $month_now = 'April';
        } elseif ($data['max_month'] == '5') {
            $month_now = 'Mei';
        } elseif ($data['max_month'] == '6') {
            $month_now = 'Juni';
        } elseif ($data['max_month'] == '7') {
            $month_now = 'Juli';
        } elseif ($data['max_month'] == '8') {
            $month_now = 'Agustus';
        } elseif ($data['max_month'] == '9') {
            $month_now = 'September';
        } elseif ($data['max_month'] == '10') {
            $month_now = 'Oktober';
        } elseif ($data['max_month'] == '11') {
            $month_now = 'November';
        } elseif ($data['max_month'] == '12') {
            $month_now = 'Desember';
        }
        $data['month_now'] = $month_now;

        return view('kinerja.lap_modal', $data);
    }

    public function lap_modal_list(Request $request)
    {
        // dd('asdf',$request->all());
        $request_param = explode('#', $request->param);
        $user = $request->session()->get('user');
        if (count($request_param) != 2) {
            Session::flash('error_message', 'There is something wrong with parameter list Rincian E-Wallet');
            return Redirect::to($redirect);
        }
        // dd($request_param);
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'search_report_year' => $request_param[0],
            'search_report_month' => $request_param[1],
        );
        $data_list = $this->repository->DataTable($param);
        // dd($data_list);
        // return Datatables::of($data_list)->make();
        echo json_encode($data_list);
    }

    //=============================================== GETDATA ===============================================
    public function lap_modal_getdata(Request $request)
    {
        $data = $this->repository->FirstDataId($request->id);
        return response()->json($data);
    }
    //=============================================== POSTDATA ===============================================

    public function lap_modal_edit(Request $request)
    {
        // dd($request->all());
        $redirect = '/' . $request->locate . $this->redirect;
        // dd($redirect);
        $user = $request->session()->get('user');

        foreach ($request->trx_amount as $t_id => $value) {
            if ($value == null || $value == '') {
                Session::flash('error_message', 'Uraian ' . $value . ' is Mandatory');
                return Redirect::to($redirect);
            }
        }

        $data = array(
            'id_user' => $user->first_name . ' ' . $user->last_name,
            'trx_amount' => $request->trx_amount
        );
        $add = $this->repository->Edit($data);
        if ($add['status'] == 1) {
            Session::flash('message', $add['message']);
            return Redirect::to($redirect . '?report_year=' . $request->tmp_year . '&report_month=' . $request->tmp_month);
        } else {
            Session::flash('error_message', $add['message']);
            return Redirect::to($redirect . '?report_year=' . $request->tmp_year . '&report_month=' . $request->tmp_month);
        }
    }

    public function lap_modal_action(Request $request)
    {
        // dd($this->redirect, $request->all());
        $action_flag = $request->action_flag;
        $user = $request->session()->get('user');

        $data = array(
            'id' => $request->id,
            'report_year' => $request->report_year,
            'report_month' => $request->report_month,
            'component_code' => $request->component_code,
            'component_desc' => $request->component_desc,
            'general_information' => $request->general_information,
            'id_user' => $user->id_user,
            'nama_user' => $user->first_name . ' ' . $user->last_name,
        );

        if ($action_flag == 'A') {
            $ket_log = 'Add ';
            $action = $this->repository->Add($data);
        } else {
            $ket_log = 'Edit ';
            $action = $this->repository->Edit($data);
        }

        $before_log = $this->repository->GetData();
        foreach ($before_log as $key => $value) {
            $general_information_before[] = $value->t_id . ':' . $value->general_information;
        }
        // dd($general_information_before);
        $data_log = array(
            'current_url' => $_SERVER['REQUEST_URI'],
            'event' =>  $ket_log . $this->event,
            'description' => json_encode($data),
            'created_by' => $user->id_user,
            'created_name' => $user->first_name . ' ' . $user->last_name,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'before_log' => json_encode($general_information_before),
            'after_log' => json_encode($data),
        );
        $add_log = $this->main_repository->AddLogActivity($data_log);

        if ($action['status'] == 1) {
            $result[] = array("status" => $action['status'], "message" => 'Item has been ' . $ket_log . 'ed');
            return response()->json($result[0]);
        } else {
            $result[] = array("status" => $action['status'], "message" => 'Item has been ' . $ket_log . 'ed');
            return response()->json($result[0]);
        }
    }

    public function lap_modal_delete(Request $request)
    {
        // dd($request->all());
        $redirect = env('APP_URLAPP') . '/' . $this->locate . '/' . $this->redirect;
        $user = $request->session()->get('user');
        $data = array(
            'id' => $request->id,
            'status' => 'f',
            'id_user' => $user->id_user,
            'nama_user' => $user->first_name . ' ' . $user->last_name,
        );
        // $data_del = $this->repository->GetDataId($request->id);
        $data_log = array(
            'current_url' => $_SERVER['REQUEST_URI'],
            'event' => 'Delete ' . $this->event,
            'description' => json_encode($data),
            'created_by' => $user->id_user,
            'created_name' => $user->first_name . ' ' . $user->last_name,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'before_log' => '',
            'after_log' => '',
        );
        $add = $this->repository->Deleted($data);
        $add_log = $this->main_repository->AddLogActivity($data_log);
        return $add;
        // if($add['status'] == 1){
        //     Session::flash('message', $add['message']);
        //     return Redirect::back();
        // }else{
        //     Session::flash('error_message', $add['message']);
        //     return Redirect::back();
        // }

    }
}
