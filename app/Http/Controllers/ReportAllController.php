<?php

namespace App\Http\Controllers;

use Validator;
use App;
use DataTables;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Repository\MainRepository;
use App\Http\Repository\ReportListRepository;

class ReportAllController extends Controller
{

    public function __construct(Request $request)
    {
        $this->repository = new ReportListRepository();
        $this->main_repository = new MainRepository();

        $data = $_SERVER['REDIRECT_URL'];
        $url = explode('/', $data);
        $this->locate = $url[2];

        $this->url = '/' . basename($_SERVER['REQUEST_URI']);
        $this->redirect = '/report_list';
        $this->event = 'Report List ';
    }

    public function all_report_index(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
        App::setLocale($this->locate);
        $user = $request->session()->get('user');
        if (empty($user)) {
            Session::flash('error_message', 'Silahkan Login Terlebih Dahulu!');
            return Redirect::to('/');
        }
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        $data['m_notif'] = $this->main_repository->GetNotif($param);
        $data['user'] = $user;

        //PUBLIC
        $data['menu'] = $this->main_repository->GetMenu($user->username);

        //KEPERLUAN BLADE
        // $data['data']  = $this->repository->DataTable();

        $data['bulan'] = $this->main_repository->GetParamTitleType('APPLICATION_PARAM', 'IS_MONTH');
        $data['locate']  = $this->locate;
        $data['max_year'] = ($request->report_year) ? $request->report_year : date('Y');
        $data['max_month'] = ($request->report_month) ? $request->report_month : date('n');

        if ($data['max_month'] == '1') {
            $month_now = 'Januari';
        } elseif ($data['max_month'] == '2') {
            $month_now = 'Februari';
        } elseif ($data['max_month'] == '3') {
            $month_now = 'Maret';
        } elseif ($data['max_month'] == '4') {
            $month_now = 'April';
        } elseif ($data['max_month'] == '5') {
            $month_now = 'Mei';
        } elseif ($data['max_month'] == '6') {
            $month_now = 'Juni';
        } elseif ($data['max_month'] == '7') {
            $month_now = 'Juli';
        } elseif ($data['max_month'] == '8') {
            $month_now = 'Agustus';
        } elseif ($data['max_month'] == '9') {
            $month_now = 'September';
        } elseif ($data['max_month'] == '10') {
            $month_now = 'Oktober';
        } elseif ($data['max_month'] == '11') {
            $month_now = 'November';
        } elseif ($data['max_month'] == '12') {
            $month_now = 'Desember';
        }
        $data['month_now'] = $month_now;

        return view('report.all_report', $data);
    }

    public function all_report_zip(Request $request)
    {
        // $files = array();
        // foreach ($request->data as $key => $value) {
        //     $dataparam = array(
        //         "custid" => $value['custid'],
        //         "date" => $value['date'],
        //         "type" => $value['type'],
        //         "isDownload" => "1",
        //         "username" => $user->user_username,
        //         "user_lastname" => $user->user_last_name,
        //         "cust" => $value['custid'],
        //         "table" => "goaml_v2.apj_goaml_ltkt"
        //     );
        //     $query = $this->go_aml_repository->ReportedXMLSave($dataparam);

        //     $dom = $this->thisVarAPJ($user, $value['custid'], $dataparam);

        //     $datetime = date('ymdHis');
        //     // $name = strftime('Goaml-'.$datetime.'.xml');

        //     $get_name = $this->main_repository->GetNameXML($dataparam);

        //     if ($request->type == 'DEBIT') {
        //         $type = 'D';
        //     } else {
        //         $type = 'C';
        //     }
        //     $txt_name = 'LTKT-' . $type . '-' . str_replace('-', '', $get_name->apj_t_date_transaction) . '-' . $get_name->apj_t_entity_reference;
        //     $name = strftime($txt_name . '.xml');
        //     // $name = strftime('GoamlCTR-'.$value['custid'].'-'.$datetime.'.xml');

        //     $dir = 'GoamlCTRXML/';
        //     if (!file_exists($dir)) {
        //         mkdir($dir, 0777, true);
        //     }
        //     $dom->save($dir . $name, LIBXML_NOEMPTYTAG);
        //     array_push($files, $name);
        // }

        // //ANOTHER TRY
        // $dir = 'GoamlCTRXML/'; //folder path
        // if (!file_exists($dir)) {
        //     mkdir($dir, 0777, true);
        // }

        // $archive = 'GoamlCTRXML' . time() . '.zip';

        // $zip = new ZipArchive;
        // $zip->open($archive, ZipArchive::CREATE);
        // $files = scandir($dir);
        // unset($files[0], $files[1]);
        // foreach ($files as $file) {
        //     $zip->addFile($dir . $file);
        // }
        // $zip->close();

        // header('Content-Type: application/zip');
        // header('Content-disposition: attachment; filename='.$archive);
        // header('Content-Length: '.filesize($archive));

        // header('Content-Description: File Transfer');
        // header('Content-Type: application/octet-stream');
        // header('Content-Disposition: attachment; filename=' . basename($archive));
        // header('Content-Transfer-Encoding: binary');
        // header('Expires: 0');
        // header('Cache-Control: must-revalidate');
        // header('Pragma: public');
        // header('Content-Length: ' . filesize($archive));
        // readfile($archive);
        // unlink($archive);
        // foreach ($files as $file) {
        //     unlink($dir . '/' . $file);
        // }

        // $result[] = array("status" => "1", "message" => 'Your File Ready To Download', "Filename" => basename($archive));
        // return response()->json($result[0]);
    }
}
