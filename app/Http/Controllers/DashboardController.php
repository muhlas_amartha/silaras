<?php

namespace App\Http\Controllers;

use Validator;
use Excel;
use App;
use Lang;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Http\Repository\DashboardRepository;
use App\Http\Repository\MainRepository;

class DashboardController extends Controller
{   

    public function __construct(Request $request)
    {
        $this->repository = new DashboardRepository();
        $this->main_repository = new MainRepository();

        $data = $_SERVER['REDIRECT_URL'];   
        $url = explode('/', $data);
        $this->locate = $url[2];

        $this->url = 'dashboard';
    }

    public function dashboard_index(Request $request)
    {
        $user = $request->session()->get('user');
        if($user == null){
            Session::flash('error_message', 'Silahkan Login Terlebih Dahulu');
            return Redirect::to('/');
        }
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level
        );

        $data['menu'] = $this->main_repository->GetMenu($user->username);
        $data['m_notif'] = $this->main_repository->GetNotif($param);
        $data['user'] = $user;
        $data['locate']  = $this->locate;

        // dd($data);
        return view('dashboard.dashboard', $data);
    }
}
