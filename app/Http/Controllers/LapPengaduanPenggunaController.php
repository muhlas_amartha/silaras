<?php

namespace App\Http\Controllers;

use Validator;
use App;
use DataTables;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Repository\MainRepository;
use App\Http\Repository\LapPengaduanPenggunaRepository;

class LapPengaduanPenggunaController extends Controller
{

    public function __construct(Request $request)
    {
        $this->repository = new LapPengaduanPenggunaRepository();
        $this->main_repository = new MainRepository();

        $data = $_SERVER['REDIRECT_URL'];
        $url = explode('/', $data);
        $this->locate = $url[2];

        $this->url = '/' . basename($_SERVER['REQUEST_URI']);
        $this->redirect = '/pengaduan_pengguna';
        $this->event = 'Laporan Pengaduan Pengguna';
    }

    public function pengaduan_pengguna_index(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
        App::setLocale($this->locate);
        $user = $request->session()->get('user');
        if (empty($user)) {
            Session::flash('error_message', 'Silahkan Login Terlebih Dahulu!');
            return Redirect::to('/');
        }
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'year' => ($request->report_year) ? $request->report_year : date('Y'),
            'month' => ($request->report_month) ? $request->report_month : date('n'),
        );

        //PUBLIC
        $data['menu'] = $this->main_repository->GetMenu($user->username);
        $data['m_notif'] = $this->main_repository->GetNotif($param);
        $data['user'] = $user;

        //KEPERLUAN BLADE
        $data['data']  = $this->repository->DataTable($param);
        // dd($data['data']);
        $data['bulan'] = $this->main_repository->GetParamTitleType('APPLICATION_PARAM', 'IS_MONTH');
        $data['locate']  = $this->locate;
        // $data['provinsi'] = $this->repository->GetProvinsi();
        $data['max_year'] = ($request->report_year) ? $request->report_year : date('Y');
        $data['max_month'] = ($request->report_month) ? $request->report_month : date('n');

        if ($data['max_month'] == '1') {
            $month_now = 'Januari';
        } elseif ($data['max_month'] == '2') {
            $month_now = 'Februari';
        } elseif ($data['max_month'] == '3') {
            $month_now = 'Maret';
        } elseif ($data['max_month'] == '4') {
            $month_now = 'April';
        } elseif ($data['max_month'] == '5') {
            $month_now = 'Mei';
        } elseif ($data['max_month'] == '6') {
            $month_now = 'Juni';
        } elseif ($data['max_month'] == '7') {
            $month_now = 'Juli';
        } elseif ($data['max_month'] == '8') {
            $month_now = 'Agustus';
        } elseif ($data['max_month'] == '9') {
            $month_now = 'September';
        } elseif ($data['max_month'] == '10') {
            $month_now = 'Oktober';
        } elseif ($data['max_month'] == '11') {
            $month_now = 'November';
        } elseif ($data['max_month'] == '12') {
            $month_now = 'Desember';
        }
        $data['month_now'] = $month_now;

        return view('kinerja.lap_pengaduan_pengguna', $data);
    }

    //=============================================== GETDATA ===============================================
    public function pengaduan_pengguna_getvalidator(Request $request)
    {
        $data = $this->repository->FirstDataValidator($request->id);
        return response()->json($data);
    }

    public function pengaduan_pengguna_getkab(Request $request)
    {
        $data = $this->repository->GetKabKota($request->provinsi_code);
        return response()->json($data);
    }

    public function pengaduan_pengguna_list(Request $request)
    {
        // dd('asdf',$request->all());
        $request_param = explode('#', $request->param);
        $user = $request->session()->get('user');
        if (count($request_param) != 2) {
            Session::flash('error_message', 'There is something wrong with parameter list Rincian E-Wallet');
            return Redirect::to($redirect);
        }
        // dd($request_param);
        $param = array(
            'id_divisi' => $user->id_divisi,
            'user_level' => $user->user_level,
            'search_report_year' => $request_param[0],
            'search_report_month' => $request_param[1],
        );
        $data_list = $this->repository->DataTable($param);
        // dd($data_list);
        // return Datatables::of($data_list)->make();
        echo json_encode($data_list);
    }

    //=============================================== POSTDATA ===============================================
    public function pengaduan_pengguna_edit(Request $request)
    {
        // dd($request->all());
        $redirect = '/' . $request->locate . $this->redirect;
        $user = $request->session()->get('user');

        foreach ($request->trx_amount as $t_id => $value) {
            if ($value == null || $value == '') {
                Session::flash('error_message', 'Uraian ' . $value . ' is Mandatory');
                return Redirect::to($redirect);
            }
        }

        $data = array(
            'id_user' => $user->first_name . ' ' . $user->last_name,
            'trx_amount' => $request->trx_amount,
            'done_amount' => $request->done_amount,
            'cancel_amount' => $request->cancel_amount,
            'on_progress_amount' => $request->on_progress_amount
        );
        // dd($data);
        $add = $this->repository->Edit($data);
        if ($add['status'] == 1) {
            Session::flash('message', $add['message']);
            return Redirect::to($redirect);
        } else {
            Session::flash('error_message', $add['message']);
            return Redirect::to($redirect);
        }
    }
}
