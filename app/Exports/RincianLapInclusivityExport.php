<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;

class RincianLapInclusivityExport implements FromCollection, WithHeadings
{
    use Exportable;
    protected $data;
    protected $header;

    function __construct($data, $header, $tipe)
    {
        $this->data = $data;
        $this->header = $header;
        $this->tipe = $tipe;
    }

    public function collection()
    {
        // dd($this->data);
        $collection = new Collection();
        foreach ($this->data as $item) {
            if ($this->tipe == 'tahunan') {
                $collection->push((object)[
                    'test' => $item->flag_detail . '|' . $item->component_code . '|' . $item->province_code
                        . '|' . $item->trx_amount_before_january . '|' . $item->trx_amount_january
                        . '|' . $item->trx_amount_before_february . '|' . $item->trx_amount_february
                        . '|' . $item->trx_amount_before_march . '|' . $item->trx_amount_march
                        . '|' . $item->trx_amount_before_april . '|' . $item->trx_amount_april
                        . '|' . $item->trx_amount_before_mei . '|' . $item->trx_amount_mei
                        . '|' . $item->trx_amount_before_june . '|' . $item->trx_amount_june
                        . '|' . $item->trx_amount_before_july . '|' . $item->trx_amount_july
                        . '|' . $item->trx_amount_before_august . '|' . $item->trx_amount_august
                        . '|' . $item->trx_amount_before_september . '|' . $item->trx_amount_september
                        . '|' . $item->trx_amount_before_october . '|' . $item->trx_amount_october
                        . '|' . $item->trx_amount_before_november . '|' . $item->trx_amount_november
                        . '|' . $item->trx_amount_before_december . '|' . $item->trx_amount_december,
                ]);
            } else {
                $collection->push((object)[
                    'test' => $item->flag_detail . '|' . $item->component_code . '|' . $item->province_code . '|' . $item->trx_amount_all_before . '|' . $item->trx_amount,
                ]);
            }
        }
        return $collection;
    }

    public function headings(): array
    {
        return [
            $this->header->flag_header . '|' . $this->header->sector_code . '|' . $this->header->ljk_code . '|' . $this->header->date_period . '|' . $this->header->report_period . '|' . $this->header->report_scope . '|' . $this->header->report_id
        ];
    }
}
