<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;

class ProfilExport implements FromCollection, WithHeadings
{
    use Exportable;
    protected $data;

    function __construct($data, $header)
    {
        $this->data = $data;
        $this->header = $header;
    }

    public function collection()
    {
        $collection = new Collection();
        foreach ($this->data as $item) {
            $collection->push((object)[
                'test' => $item->flag_detail . '|' . $item->component_code . '|' . $item->general_information,
            ]);
        }
        return $collection;
    }

    public function headings(): array
    {
        return [
            $this->header->flag_header . '|' . $this->header->sector_code . '|' . $this->header->ljk_code . '|' . $this->header->date_period . '|' . $this->header->report_period . '|' . $this->header->report_scope
        ];
    }
}
