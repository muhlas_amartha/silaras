<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;

class RincianPaymentExport implements FromCollection, WithHeadings
{
    use Exportable;
    protected $data;

    function __construct($data) {
        $this->data = $data;
    }

    public function collection()
    {
        $collection = new Collection();
        foreach($this->data as $item){
            $collection->push((object)[
                'test' => $item->flag_detail.'|'.$item->component_code.'|'.$item->general_information,
            ]);
        }
        return $collection;
    }

    public function headings(): array
    {
        return [
            '',
        ];
    }

}