<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;

class RincianLapPengaduanPenggunaExport implements FromCollection, WithHeadings
{
    use Exportable;
    protected $data;
    protected $header;

    function __construct($data, $header, $tipe)
    {
        $this->data = $data;
        $this->header = $header;
        $this->tipe = $tipe;
    }

    public function collection()
    {
        $collection = new Collection();
        foreach ($this->data as $item) {
            // if ($this->tipe == 'tahunan') {
            //     $collection->push((object)[
            //         'test' => $item->flag_detail . '|' . $item->component_code . '|' . $item->trx_amount . '|' . $item->trx_amount_before,
            //     ]);
            // } else {
            $collection->push((object)[
                'test' => $item->flag_detail . '|' . $item->component_code . '|' . trim(preg_replace('/\s\s+/', '', $item->timeframe_code)) . '|' . $item->trx_amount . '|' . $item->done_amount . '|' . $item->cancel_amount . '|' . $item->on_progress_amount,
            ]);
            // }
        }
        return $collection;
    }

    public function headings(): array
    {
        return [
            $this->header->flag_header . '|' . $this->header->sector_code . '|' . $this->header->ljk_code . '|' . $this->header->date_period . '|' . $this->header->report_period . '|' . $this->header->report_scope . '|' . $this->header->report_id
        ];
    }
}
